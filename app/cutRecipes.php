<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class cutRecipes extends Model
{
    public $timestamps = true;
    protected $table = 'cutsRecipes';
    protected $getRows = 10;

    public function getCutsRecipes()
    {
        return DB::table($this->table)
            ->get();
    }
    public function addCutsRecipes($name)
    {
        return DB::table($this->table)->insertGetId(
        array('name' => $name)
    );
    }
    public function removeCutsRecipes($id){
        DB::table($this->table)->where('id', '=', $id)->delete();

    }
}
