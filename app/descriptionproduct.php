<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class descriptionproduct extends Model
{
    public $timestamps = false;
    public $table = 'descriptionproduct';

    public function getProducts($skip){
        return DB::table('descriptionproduct')->select('product')->skip($skip)->take(100)->get();

    }


    public function deletedescription($id){
        DB::table($this->table)->where('receiptId', '=', $id)->delete();

    }
}
