<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class imagesrecipes extends Model
{
    public $timestamps = false;
    public $table = 'imagesrecipes';
    //


    public function insertNewReceipt($title, $cooking, $portions){
       return  DB::table('receipts')->insertGetId(
            array('title' => $title,
                'cooking_time' => $cooking,
                'portions' => $portions,
                )
        );
    }
}
