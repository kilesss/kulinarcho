<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


class links extends Model
{
    public $timestamps = false;


    public function getById($id){
        $user = DB::table('links')->where('id', $id)->get();
        return $user[0];
    }
    //
}
