<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class foodCategories extends Model
{
    public $timestamps = true;
    protected $table = 'foodCategories';
    protected $getRows = 10;

    public function getFoodCategories()
    {
        return DB::table($this->table)
            ->get();
    }
    public function addFoodCategories($name)
    {
        return DB::table($this->table)->insertGetId(
        array('name' => $name)
    );
    }
    public function removeFoodCategories($id){
        DB::table($this->table)->where('id', '=', $id)->delete();

    }
    public function foodCategoryOnMenu($id, $checked){
        $isChecked = 0;

        if ($checked == 'true'){ $isChecked = 1;}

        return DB::table($this->table)
            ->where('id', '=', $id)
            ->update(['showOnMenu' => $isChecked]);

    }
}
