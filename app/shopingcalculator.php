<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class shopingcalculator extends Model
{
    public function getShopingLIst(){
        return DB::table('shopingcalculators')->get();

    }
    public function deleteAllrows(){
        DB::table('shopingcalculators')->delete();

    }
}
