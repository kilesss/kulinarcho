<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class receiptMaping extends Model
{
    public $timestamps = false;
    protected $table = 'receiptMaping';

    public function fillData(){


        $r = DB::table($this->table)->get();

            foreach ($r as $item) {
                $products = explode(',',$item->products);

                $steps = explode('.',$item->steps);
                $string = "<div class='row'>".$item->title."</div><br><hr><br>";
                foreach ($products as $prod){
                    $string.="<div class='row'>".$prod."</div><br>";
                }
                $string.="<br><hr><br>";
                foreach ($steps as $prod){
                    $string.="<div class='row'>".$prod."</div><br>";
                }
                $string.="<br><hr><hr>";
                $receipt = new receipts();
                $receipt->title = $item->title;
                $receipt->active = 0;
                $receipt->plaintext=$string;
                $receipt->save();
            }
        }

    }

