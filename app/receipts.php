<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class receipts extends Model
{
    public $timestamps = false;
    protected $getRows = 20;
    protected $table = 'receipts';
    protected $tableproductreceiptmap = 'productreceiptmap';
    protected $tabledescriptionproduct = 'descriptionproduct';
    //


    public function insertNewReceipt($title, $cooking, $portions, $category, $type){
       return  DB::table('receipts')->insertGetId(
            array('title' => $title,
                'cooking_time' => $cooking,
                'portions' => $portions,
                'category'=>$category,
                'type'=>$type,
                )
        );
    }
    public function getReceipts($page, $title = false, $category=false, $type=false){
        $count = DB::table($this->table)->count();
        if ($page > 1) {
            $skip = $page * $this->getRows;
        } else {
            $skip = 0;
        }
        $records = DB::table($this->table)
            ->skip($skip)
            ->take($this->getRows);

        if ($title!= false){
         $records->where('name', '%', $title, '%');
        }

        if ($category!= false){
            $records->where('category',$category);
        }
        if ($type!= false){
            $records->where('type',$type);
        }
            $recordsArray = $records->get();

        $pages = $count / $this->getRows;
        $pages2 = $count % $this->getRows;
        $countPage = (int)$pages;
        if ($pages2 > 0) {
            $countPage++;
        }

        return array('count' => $countPage, 'records' => $recordsArray);
    }
    public function getRecipesByID($id){
        $recipeBasic = DB::table($this->table)
            ->where('id', $id)
            ->first();
        $recipeProducts = DB::table($this->tableproductreceiptmap)
                ->where('receiptId', $id)
            ->get();
        $recipeDesc = DB::table($this->tabledescriptionproduct)
            ->where('receiptId', $id)
            ->get();
        return ['basic' => $recipeBasic,
                'products'=>$recipeProducts,
                'description'=> $recipeDesc
        ];
//        dd($recipeBasic, $recipeProducts, $recipeDesc);
    }
    public function updateBasicRecipes($basic, $id){
        $r = DB::table($this->table)
            ->where('id', $id)
            ->update([
                'title' => $basic['title'],
                'cooking_time' => $basic['cooking_time'],
                'portions' => $basic['portions'],
                'category' => $basic['category'],
                'type' => $basic['type'],
                'active' => $basic['active'],
            ]);
    }
}
