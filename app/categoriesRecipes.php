<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class categoriesRecipes extends Model
{
    public $timestamps = true;
    protected $table = 'categoriesRecipes';
    protected $getRows = 10;

    public function getCategoriesRecipes()
    {
        return DB::table($this->table)
            ->get();
    }
    public function addCategoriesRecipes($name)
    {
        return DB::table($this->table)->insertGetId(
        array('name' => $name)
    );
    }
    public function removeCategoriesRecipes($id){
        DB::table($this->table)->where('id', '=', $id)->delete();

    }


}
