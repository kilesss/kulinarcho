<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class products extends Model
{
    public $timestamps = false;
    protected $table = 'products';
protected $categoriesTable ='foodcategories';
    public function getProducts($skip){
        return DB::table($this->table)->select('product')->skip($skip)->take(100)->get();

    }
    public function getProductByID($id){
        return DB::table($this->table)->select('name')->where('id', $id)->first();

    }
    public function getProductsShowOnMenu(){
        return[
            'products'=>DB::table($this->table)
            ->join($this->categoriesTable, $this->categoriesTable.'.id', '=','foodCategoryId')
            ->select($this->table.'.name', $this->table.'.id',$this->table.'.foodCategoryId' )
            ->where($this->categoriesTable.'.showOnMenu', '1')
            ->where($this->table.'.showOnMenu', '1')
            ->get(),
            'categories'=>DB::table($this->categoriesTable)
                            ->select('name', 'id')
                            ->where('showOnMenu', '1')
                            ->get()
            ];
    }
}
