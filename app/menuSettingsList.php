<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class menuSettingsList extends Model
{
    public $timestamps = false;
    public $table = 'menu_settings_list';

    public function insertRecords($name, $dateStart, $dateEnd){
        return DB::table($this->table)->insertGetId([
            'user_id'=>Auth::user()->id,
            'name'=>$name,
            'date_start'=>Carbon::create((int)$dateStart[0], (int)$dateStart[1],(int) $dateStart[2]),
            'date_end'=>Carbon::create($dateEnd[0], $dateEnd[1], $dateEnd[2])
        ]);
    }

}

