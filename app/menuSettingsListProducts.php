<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class menuSettingsListProducts extends Model
{
    public $timestamps = false;
    public $table = 'menu_settings_list_products';

    public function insertRecords($records, $listID){
        $rows = [];
        foreach ($records as $r){
            $date = explode('/',$r['day']);
            $rows[] = [
                'user_id'=>Auth::user()->id,
                'day'=>Carbon::create($date[2], $date[1], $date[0]),
                'type'=>$r['type'],
                'mealid'=>$r['meal'],
                'prodId'=>$r['catId'],
                'menu_settings_id'=>$listID
            ];
        }
        DB::table($this->table)->insert($rows);
    }
}
