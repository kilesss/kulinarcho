<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;
    protected $getRows = 20;
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getPaginator($page)
    {
        $count = DB::table($this->table)->count();
        if ($page > 1) {
            $skip = $page * $this->getRows;
        } else {
            $skip = 0;
        }
        $records = DB::table($this->table)
            ->skip($skip)
            ->take($this->getRows)
            ->get();

        $pages = $count / $this->getRows;
        $pages2 = $count % $this->getRows;
        $countPage = (int)$pages;
        if ($pages2 > 0) {
            $countPage++;
        }

        return array('count' => $countPage, 'records' => $records);
    }
}
