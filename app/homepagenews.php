<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class homepagenews extends Model
{
    public $timestamps = true;
    protected $table = 'homepagenews';
    protected $getRows = 10;

    public function getActiveNews()
    {
        return DB::table($this->table)
            ->where('active', 1)
            ->get();

    }

    public function getPaginator($page)
    {
        $count = DB::table($this->table)->count();
        if ($page > 1) {
            $skip = $page * $this->getRows;
        } else {
            $skip = 0;
        }
        $records = DB::table($this->table)
            ->skip($skip)
            ->take($this->getRows)
            ->get();

        $pages = $count / $this->getRows;
        $pages2 = $count % $this->getRows;
        $countPage = (int)$pages;
        if ($pages2 > 0) {
            $countPage++;
        }

        return array('count' => $countPage, 'records' => $records);
    }

    public function deactivateNews($id)
    {
        return DB::table($this->table)
            ->where('id', $id)
            ->update(['active' => 0]);
    }

    public function activateNews($id)
    {
        return DB::table($this->table)
            ->where('id', $id)
            ->update(['active' => 1]);
    }
}
