<?php

namespace App\Http\Controllers\adminControllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $masterUser;
    public $adminUser;
    public $regularUser;
    public $vipUser;
    public $errors = [];
    public function __construct()
    {
        $value = config('userRoles');
        $this->masterUser = $value['masterUser'];
        $this->adminUser = $value['adminUser'];
        $this->regularUser = $value['regularUser'];
        $this->vipUser = $value['vipUser'];
    }

    public function adminGlobalVariables(){
        $routes = config('adminRouting.routing');

        return ['routes'=>$routes,
            'errors'=>$this->errors];
    }

    public function setErrors($key, $value){
        $this->errors[$key] = $value;
    }

}
