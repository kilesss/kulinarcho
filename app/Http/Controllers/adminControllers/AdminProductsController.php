<?php

namespace App\Http\Controllers\adminControllers;

use App\cutRecipes;
use App\foodCategories;
use App\homepagenews;
use App\productsRecipes;
use App\typeRecipes;
use Illuminate\Http\Request;
use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Faker\Provider\Image;
use App\categoriesRecipes;
use Illuminate\Support\Facades\Auth;

class AdminProductsController extends Controller
{

    public function ajaxAction($ajax)
    {
        if (Auth::check()) {
            switch ($ajax['type']) {
                case 'addcategory':
                    $this->addcategory($ajax['id']);
                    break;
                case 'removeCategory':
                    if(Auth::user()->role == $this->masterUser) {
                        $this->removeCategory($ajax['id']);
                        break;
                    }else{
                        echo json_encode("noPermision");
                        die;
                    }
                case 'addType':
                    $this->addType($ajax['id']);
                    break;
                case 'removeType':
                    if(Auth::user()->role == $this->masterUser) {
                        $this->removeType($ajax['id']);
                        break;
                    }else{
                        echo json_encode("noPermision");
                        die;
                    }
                case 'addCuts':
                    $this->addCuts($ajax['id']);
                    break;
                case 'removeCuts':
                    if(Auth::user()->role == $this->masterUser) {
                        $this->removeCuts($ajax['id']);
                        break;
                    }else{
                        echo json_encode("noPermision");
                        die;
                    }
                case 'addProduct':
                    $this->addProduct($ajax['id']);
                    break;
                case 'productInput':
                    $this->productInput($ajax['id']);
                    break;
                case 'removeProduct':
                    if(Auth::user()->role == $this->masterUser) {
                        $this->removeProduct($ajax['id']);
                        break;
                    }else{
                        echo json_encode("noPermision");
                        die;
                    }
                case 'editProductShow':
                    $this->editProduct($ajax, 'show');
                    break;
                case 'editProductSubmit':
                    $this->editProduct($ajax, 'submit');
                    break;
                case 'foodCategoryOnMenu':
                    $this->foodCategoryOnMenu($ajax);
                    break;
                    break;
                case 'foodCategory':
                    $this->foodCategory($ajax);
                    break;
                case 'removeFoodCategory':
                    if(Auth::user()->role == $this->masterUser) {
                        $this->removeFoodCategory($ajax);
                    }else{
                        echo json_encode("noPermision");
                        die;
                    }
//
            }
        }else{
            echo json_encode('requireLogin');
            die;
        }
    }


    public function parametersAction(Request $request)
    {
        $productPage = 0;
        $catModel = new categoriesRecipes();
        $foodCatModel = new foodCategories();
        $typeModel = new typeRecipes();
        $cutsModel = new cutRecipes();
        $productModel = new productsRecipes();
        $catVal = $catModel->getCategoriesRecipes();
        $typeVal = $typeModel->getTypeRecipes();
        $cutsVal = $cutsModel->getCutsRecipes();
        $foodCat = $foodCatModel->getFoodCategories();
        if ($request->route('productPage') != null) {
            $paginator = $productModel->getPaginator($request->route('productPage'));
            $productPage = $request->route('productPage');
        }else{
            $paginator = $productModel->getPaginator(1);

        }

        if ($request->post()) {

            $postRequest = $request->post();

            if (isset($postRequest['ajax'])) {
                $this->ajaxAction($postRequest);
            }
            if (isset($postRequest['paginator-pageProduct']) && $postRequest['paginator-pageProduct'] != "") {
                $paginator = $productModel->getPaginator($postRequest['paginator-pageProduct']);

            }
        }
        return array(
            'global' => $this->adminGlobalVariables(),
            'categoriesList' => $catVal,
            'foodCategoriesList' => $foodCat,
            'typesList' => $typeVal,
            'cutsList' => $cutsVal,
            'countProduct' => $paginator['count'],
            'recordsProduct' => $paginator['records'],
            'productPage' => $productPage
        );

    }
    private function foodCategoryOnMenu($ajax){
        $foodCatModel = new foodCategories();

        echo json_encode($foodCatModel->foodCategoryOnMenu($ajax['id'],$ajax['checked']));
        die;
    }
    private function foodCategory($ajax){
        $foodCatModel = new foodCategories();

        echo json_encode($foodCatModel->addFoodCategories($ajax['id']));
        die;
    }

    private function removeFoodCategory($ajax){
        $foodCatModel = new foodCategories();
        $foodCatModel->removeFoodCategories($ajax['id']);
        echo json_encode(1);
        die;

    }
    private function addcategory($id)
    {
        $catModel = new categoriesRecipes();
        echo json_encode($catModel->addCategoriesRecipes($id));
        die;
    }

    private function removeCategory($id)
    {
        $catModel = new categoriesRecipes();
        $catModel->removeCategoriesRecipes($id);
        echo json_encode(1);
        die;
    }

    private function addType($id)
    {
        $catModel = new typeRecipes();
        echo json_encode($catModel->addTypeRecipes($id));
        die;
    }

    private function removeType($id)
    {
        $catModel = new typeRecipes();
        $catModel->removeTypeRecipes($id);
        echo json_encode(1);
        die;
    }

    private function addCuts($id)
    {
        $catModel = new cutRecipes();
        echo json_encode($catModel->addCutsRecipes($id));
        die;
    }

    private function removeCuts($id)
    {
        $catModel = new cutRecipes();
        $catModel->removeCutsRecipes($id);
        echo json_encode(1);
        die;
    }

    private function addProduct($id)
    {
        $catModel = new productsRecipes();
        echo json_encode($catModel->addProductsRecipes($id));
        die;
    }

    private  function productInput($id){
        $catModel = new productsRecipes();
        echo json_encode($catModel->getProductByNameAutoFill($id));
        die;
    }
    private function removeProduct($id)
    {
        $catModel = new productsRecipes();
        $catModel->removeProductsRecipes($id);
        echo json_encode(1);
        die;
    }

    private function editProduct($ajax, $type){
        $productModel = new productsRecipes();
        if ($type == "show"){
            $product = $productModel->getProductByID($ajax['id']);
            echo json_encode(['id'=> $product->id,
                'name'=> $product->name,
                'hint'=> $product->hint,
                'proteins'=> $product->proteins,
                'carbohydrates'=> $product->carbohydrates,
                'calories'=> $product->calories,
                'foodCategoryId'=> $product->foodCategoryId,
                'showOnMenu'=> $product->showOnMenu,
                'fat'=> $product->fat]);
            die;
        }elseif ($type == 'submit'){
           $res =  $productModel->updateProduct($ajax);
            echo $res;
            die;
        }

    }
}
