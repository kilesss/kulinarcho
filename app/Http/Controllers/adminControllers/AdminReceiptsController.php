<?php

namespace App\Http\Controllers\adminControllers;

use App\categoriesRecipes;
use App\cutRecipes;
use App\descriptionproduct;
use App\imagesrecipes;
use App\productreceiptmap;
use App\products;
use App\productsRecipes;
use App\receipts;
use App\typeRecipes;
use Illuminate\Http\Request;


class AdminReceiptsController extends Controller
{


    public function indexAction($request)
    {
        $postRequest = $request->post();

        if (isset($postRequest['ajax'])) {
            $this->ajaxAction($postRequest['ajax']);
        }

        $categoryModel = new categoriesRecipes();
        $category = $categoryModel->getCategoriesRecipes();
        $typeModel = new typeRecipes();

        $cutModel = new cutRecipes();
        $cut = $cutModel->getCutsRecipes();
        $type = $typeModel->getTypeRecipes();
        return array(
            'category' => $category,
            'cut' => $cut,
            'type' => $type
        );
    }

    public function ajaxAction($ajax)
    {
        switch ($ajax['type']) {
            case 'recipesListSort':
                $this->listReceiptsAction($ajax);
                break;
        }
    }

    public function addRecipesAction($request)
    {
        $post = $request->post();
        $productsModel = new productsRecipes();
        $title = $request['receiptTitle'];
        $category = $request['receiptCategory'];
        $type = $request['receiptType'];
        $cookingTime = $request['ProductsCookingTime'];
        $portion = $request['ProductsPortions'];
        $products = [];
        $steps = [];
        $errors = [];
        foreach ($post as $key => $val) {
            if (strpos($key, 'receiptProduct') !== false) {
                $keyproduct = explode('_', $key);
                $productID = $productsModel->getProductByName($post['receiptProduct_' . $keyproduct[1]]);
                if ($productID == null) {
                    $errors['product'][] = $post['receiptProduct_' . $keyproduct[1]];
                    $errors['type'] = 'wrongProduct';
                } else {
                    $products[$keyproduct[1]]['product'] = $productID->id;
                    $products[$keyproduct[1]]['cuts'] = $post['receiptProductCategory_' . $keyproduct[1]];

                    $products[$keyproduct[1]]['vol'] = $post['productVol_' . $keyproduct[1]];
                    $products[$keyproduct[1]]['hint'] = $post['hintProduct_' . $keyproduct[1]];
                }
            }
            if (strpos($key, 'stepCooking') !== false) {
                $steps[] = $val;
            }

        }
        if ($title == '') {
            $errors['type']['title'] = 'wrongTitle';
        }
        if ($category == '') {
//TODO check in database if exist
            $errors['type']['category'] = 'wrongCategory';
        }
        if ($cookingTime == '') {
            $errors['type']['cookingTime'] = 'wrongCookingTime';
        }
        if ($type == '') {
//TODO check in database if exist
            $errors['type']['type'] = 'wrongType';
        }
        if ($portion == '') {
            $errors['type']['portion'] = 'wrongPortion';
        }
        if (count($errors) >= 1) {
            $this->setErrors('products', $errors['type']);
            return array(
                'title' => $title,
                'category' => $category,
                'cookingTime' => $cookingTime,
                'type' => $type,
                'portion' => $portion,
                'products' => $products,
                'steps' => $steps,
            );
        }

        //TODO add validation
        $receiptsModel = new receipts();
        $receiptId = $receiptsModel->insertNewReceipt($title, $cookingTime, $portion, $category, $type);
        if ($receiptId != null && $receiptId > 0) {
            if ($request->hasfile('imagePortions')) {
                $this->validate($request, [
                    'imagePortions' => 'required',
                    'imagePortions.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                ]);

                $extension = $request->file('imagePortions')->getClientOriginalExtension();
                $imageTitle = 'fpni_' . $receiptId . '_' . rand(1, 9999) . '.' . $extension;

                $request->file('imagePortions')->move(public_path() . '/img/imageReceips/', $imageTitle);
                $imageRecipe = new imagesrecipes();
                $imageRecipe->receiptId = $receiptId;
                $imageRecipe->image = '/img/imageReceips/' . $imageTitle;
                $imageRecipe->save();
            }
            foreach ($products as $prod) {

                $productreceiptmapModel = new productreceiptmap();

                $productreceiptmapModel->receiptId = $receiptId;
                $productreceiptmapModel->productId = $prod['product'];
                $productreceiptmapModel->cutsId = $prod['cuts'];
                $productreceiptmapModel->hint = $prod['hint'];
                $productreceiptmapModel->valueId = $prod['vol'];
                $productreceiptmapModel->save();
            }
            foreach ($steps as $k => $step) {
                if ($step != "") {
                    $stepsModel = new descriptionproduct();
                    $stepsModel->receiptId = $receiptId;
                    $stepsModel->step = $k + 1;
                    $stepsModel->text = $step;
                    $stepsModel->save();
                }
            }
            return array('success' => 1);
        } else {
            $this->setErrors('productsTitle', 'recipeNotSave');

            return array(
                'title' => $title,
                'category' => $category,
                'cookingTime' => $cookingTime,
                'type' => $type,
                'portion' => $portion,
                'products' => $products,
                'steps' => $steps,
            );
        }
    }

    public function updateRecipesAction($request, $receiptId)
    {
        $receiptsModel = new receipts();
        $post = $request->post();
        $productsModel = new productsRecipes();
        $productreceiptmapModel = new productreceiptmap();
        $stepsModel = new descriptionproduct();

        $active = 0;

        $products = [];
        $steps = [];
        $errors = [];
        foreach ($post as $key => $val) {
            if (strpos($key, 'receiptProduct') !== false) {
                $keyproduct = explode('_', $key);
                if ($post['receiptProduct_' . $keyproduct[1]] != null && $post['receiptProduct_' . $keyproduct[1]] != '') {

                    $productID = $productsModel->getProductByName($post['receiptProduct_' . $keyproduct[1]]);
                    if ($productID == null) {
                        $errors['product'][] = $post['receiptProduct_' . $keyproduct[1]];
                        $errors['type'] = 'wrongProduct';
                    } else {
                        $products[$keyproduct[1]]['product'] = $productID->id;
                        $products[$keyproduct[1]]['cuts'] = $post['receiptProductCategory_' . $keyproduct[1]];

                        $products[$keyproduct[1]]['vol'] = $post['productVol_' . $keyproduct[1]];
                        $products[$keyproduct[1]]['hint'] = $post['hintProduct_' . $keyproduct[1]];
                    }
                }
            }
            if (strpos($key, 'stepCooking') !== false) {
                $steps[] = $val;
            }

        }

//        if ($post['receiptTitle'] == '') {
//            $errors['type']['title'] = 'wrongTitle';
//        }
//        if ($post['receiptCategory'] == '') {
////TODO check in database if exist
//            $errors['type']['category'] = 'wrongCategory';
//        }
//        if ($post['ProductsCookingTime'] == '') {
//            $errors['type']['cookingTime'] = 'wrongCookingTime';
//        }
//        if ($post['receiptType'] == '') {
////TODO check in database if exist
//            $errors['type']['type'] = 'wrongType';
//        }
//        if ($post['ProductsPortions'] == '') {
//            $errors['type']['portion'] = 'wrongPortion';
//        }
        if (count($errors) >= 1) {

            $this->setErrors('products', $errors['type']);
            return array(
                'title' => $post['receiptTitle'],
                'cooking_time' => $post['ProductsCookingTime'],
                'portions' => $post['ProductsPortions'],
                'category' => $post['receiptCategory'],
                'type' => $post['receiptType'],
                'active' => $active,
                'products' => $products,
                'steps' => $steps,
            );
        }

        if (isset($post['activeReceipt']) && $post['activeReceipt'] == 'on') {
            $active = 1;
        }
        $receiptsModel->updateBasicRecipes([
            'title' => $post['receiptTitle'],
            'cooking_time' => $post['ProductsCookingTime'],
            'portions' => $post['ProductsPortions'],
            'category' => $post['receiptCategory'],
            'type' => $post['receiptType'],
            'active' => $active,
        ], $receiptId);
        $productreceiptmapModel->deleteProducts($receiptId);

        foreach ($products as $prod) {
            $productreceiptmapModel = new productreceiptmap();


            $productreceiptmapModel->receiptId = $receiptId;
            $productreceiptmapModel->productId = $prod['product'];
            $productreceiptmapModel->cutsId = $prod['cuts'];
            $productreceiptmapModel->hint = $prod['hint'];
            $productreceiptmapModel->valueId = $prod['vol'];
            $productreceiptmapModel->save();
        }
        $stepsModel->deletedescription($receiptId);

        foreach ($steps as $k => $step) {
            $stepsModel = new descriptionproduct();

            if (isset($step) && $step != "") {
                $stepsModel->receiptId = $receiptId;
                $stepsModel->step = $k + 1;
                $stepsModel->text = $step;
                $stepsModel->save();
            }
        }
        return array('success' => 1);
    }

    public function listReceiptsAction($request, $ajax = false)
    {
        $receiptsModel = new receipts();

        if ($ajax == false) {
            if ($request->route('receiptPage') != null) {
                $list = $receiptsModel->getReceipts($request->route('receiptPage'));
            } else {
                $list = $receiptsModel->getReceipts(1);

            }
        } else {
            dd($ajax);
        }
        return ['list' => $list];

    }

    public function getRecipesByID($id)
    {
        $receiptsModel = new receipts();
        $productsModel = new products();
        $receipt = $receiptsModel->getRecipesByID($id);
        if ($receipt['basic'] == null) {
            return 0;
        }
//        if ($receipt['basic']->title == null || $receipt['basic']->cooking_time == null || $receipt['basic']->portions == null ||
//            $receipt['basic']->category == null || $receipt['basic']->type == null ){
//            return 0;
//
//        }else {
//        dd($receipt);
        $prodParced = [];
        foreach ($receipt['products'] as $prod) {
            $name = $productsModel->getProductByID($prod->id);
            $prodParced[] = ['product' => $prod->id, 'name' => $name->name, 'cuts' => $prod->cutsId, 'vol' => $prod->valueId, 'hint' => $prod->hint];
        }
        $stepsParced = [];
        foreach ($receipt['description'] as $step) {
            $stepsParced[$step->step] = $step->text;
        }
        return array(
            'title' => $receipt['basic']->title,
            'category' => $receipt['basic']->category,
            'cookingTime' => $receipt['basic']->cooking_time,
            'type' => $receipt['basic']->type,
            'portion' => $receipt['basic']->portions,
            'active' => $receipt['basic']->active,
            'plaintext' => $receipt['basic']->plaintext,
            'products' => $prodParced,
            'steps' => $stepsParced,
            'updateReceipt' => '1'
        );
//        }
    }

}
