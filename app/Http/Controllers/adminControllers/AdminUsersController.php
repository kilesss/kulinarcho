<?php

namespace App\Http\Controllers\adminControllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class AdminUsersController extends Controller
{
    public function indexAction()
    {
        $users = new User();
        $usersData = $users->getPaginator(1);
//    var_dump("<pre>", $usersData); die;
        $errors = [];
        if (!Auth::check()) {
            $errors[] = 'requireLogin';
        }
        if (Auth::user()->role != $this->masterUser) {
            $errors[] = 'needMaster';
        }
        return [ 'errors' => $errors,
                'count'=>$usersData['count'],
                'records'=>$usersData['records']
            ];
        //TODO da se napravi paginatora i search-a na potrebiteli
    }

    public function userShowAction($id){

    }
}
