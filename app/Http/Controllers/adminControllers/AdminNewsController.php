<?php

namespace App\Http\Controllers\adminControllers;

use App\cutRecipes;
use App\homepagenews;
use App\productsRecipes;
use App\typeRecipes;
use Illuminate\Http\Request;
use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Faker\Provider\Image;
use App\categoriesRecipes;
use Illuminate\Support\Facades\Auth;

class AdminNewsController extends Controller
{


    public function newsAction(Request $request)
    {
        $homeNews = new homepagenews();
        $paginator = $homeNews->getPaginator(1);

        if ($request->post()) {

            $postRequest = $request->post();

            if (isset($postRequest['ajax'])) {
                $this->ajaxAction($postRequest);
            }
            if (isset($postRequest['paginator-page']) && $postRequest['paginator-page'] != "") {
                $paginator = $homeNews->getPaginator($postRequest['paginator-page']);

            } else {
                if ($request->hasfile('newsImages')) {
                    $this->validate($request, [
                        'newsImages' => 'required',
                        'newsImages.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                    ]);

                    $extension = $request->file('newsImages')->getClientOriginalExtension();
                    $imageTitle = 'fpni_' . rand(1, 9999) . '.' . $extension;

                    $request->file('newsImages')->move(public_path() . '/img/frontPageNewsImages/', $imageTitle);
                    $homeNews->title = $postRequest['titleNews'];
                    $homeNews->content = $postRequest['contentNews'];
                    $homeNews->image = $imageTitle;
                    $homeNews->body = $postRequest['textNews'];
                    $homeNews->active = $postRequest['activateNews'];
                    $homeNews->save();
                }
            }
        }
        return array(
            'global' => $this->adminGlobalVariables(),
            'count' => $paginator['count'],
            'records' => $paginator['records']
        );
    }


    public function ajaxAction($ajax)
    {
        if (Auth::check()) {
            switch ($ajax['type']) {
                case 'deactivateNews':
                    $this->deactivateNews($ajax['id']);
                    break;
                case 'activateNews':
                    $this->activateNews($ajax['id']);
                    break;
            }
        }else{
            echo json_encode('requireLogin');
            die;
        }
    }


    private function deactivateNews($id)
    {
        $homeNews = new homepagenews();
        $responce = $homeNews->deactivateNews($id);
        if ($responce == 1) {
            echo json_encode(1);
            die;
        } else {
            echo json_encode(0);
            die;
        }
    }

    private function activateNews($id)
    {
        $homeNews = new homepagenews();
        $responce = $homeNews->activateNews($id);
        if ($responce == 1) {
            echo json_encode(1);
            die;
        } else {
            echo json_encode(0);
            die;
        }
    }

}
