<?php

namespace App\Http\Controllers\frontControllers;

use App\menuSettingsListCategories;
use App\menuSettingsListProducts;
use App\menuSettingsList;
use Illuminate\Http\Request;
use App\products;
use App\Http\Controllers\Controller;
class ProfileController extends Controller
{

    public function indexAction()
    {

    }

    public function myRecipesAction()
    {

    }

    public function favoriteRecipesAction()
    {

    }

    public function editRecipesAction()
    {

    }

    public function addRecipes()
    {

    }

    public function settingsMenuAction()
    {
        $productsModel = new products();
        return [
            'products' => $productsModel->getProductsShowOnMenu()
        ];
    }

    public function fillMenuSelectedProducts($request)
    {
        if (isset($request['params'])) {
            $products = [];
            $categories = [];
            $count = 0;
            $beginDate = '';
            foreach ($request['params'] as $days => $param) {
                if ($count == 0) {
                    $beginDate = $days;
                }

                if (isset($param['b'])) {
                    if (isset($param['b']['typePositive']['categories']) && count($param['b']['typePositive']['categories']) > 0) {
                        foreach ($param['b']['typePositive']['categories'] as $positiveCategories) {
                            $categories[] = [
                                'day' => $days,
                                'type' => 1,
                                'meal' => 1,
                                'catId' => $positiveCategories
                            ];
                        }
                    }
                    if (isset($param['b']['typePositive']['products']) && count($param['b']['typePositive']['products']) > 0) {
                        foreach ($param['b']['typePositive']['products'] as $positiveProducts) {
                            $products[] = [
                                'day' => $days,
                                'type' => 1,
                                'meal' => 1,
                                'catId' => $positiveProducts
                            ];
                        }
                    }
                }
                if (isset($param['b'])) {
                    if (isset($param['b']['typeNegative']['categories']) && count($param['b']['typeNegative']['categories']) > 0) {
                        foreach ($param['b']['typeNegative']['categories'] as $positiveCategories) {
                            $categories[] = [
                                'day' => $days,
                                'type' => 0,
                                'meal' => 1,
                                'catId' => $positiveCategories
                            ];
                        }
                    }
                    if (isset($param['b']['typeNegative']['products']) && count($param['b']['typeNegative']['products']) > 0) {
                        foreach ($param['b']['typeNegative']['products'] as $positiveProducts) {
                            $products[] = [
                                'day' => $days,
                                'type' => 0,
                                'meal' => 1,
                                'catId' => $positiveProducts
                            ];
                        }
                    }
                }

                if (isset($param['l'])) {
                    if (isset($param['l']['typePositive']['categories']) && count($param['l']['typePositive']['categories']) > 0) {
                        foreach ($param['l']['typePositive']['categories'] as $positiveCategories) {
                            $categories[] = [
                                'day' => $days,
                                'type' => 1,
                                'meal' => 2,
                                'catId' => $positiveCategories
                            ];
                        }
                    }
                    if (isset($param['l']['typePositive']['products']) && count($param['l']['typePositive']['products']) > 0) {
                        foreach ($param['l']['typePositive']['products'] as $positiveProducts) {
                            $products[] = [
                                'day' => $days,
                                'type' => 1,
                                'meal' => 2,
                                'catId' => $positiveProducts
                            ];
                        }
                    }
                }
                if (isset($param['l'])) {
                    if (isset($param['l']['typeNegative']['categories']) && count($param['l']['typeNegative']['categories']) > 0) {
                        foreach ($param['l']['typeNegative']['categories'] as $positiveCategories) {
                            $categories[] = [
                                'day' => $days,
                                'type' => 0,
                                'meal' => 2,
                                'catId' => $positiveCategories
                            ];
                        }
                    }
                    if (isset($param['l']['typeNegative']['products']) && count($param['l']['typeNegative']['products']) > 0) {
                        foreach ($param['l']['typeNegative']['products'] as $positiveProducts) {
                            $products[] = [
                                'day' => $days,
                                'type' => 0,
                                'meal' => 2,
                                'catId' => $positiveProducts
                            ];
                        }
                    }
                }


                if (isset($param['d'])) {
                    if (isset($param['d']['typePositive']['categories']) && count($param['d']['typePositive']['categories']) > 0) {
                        foreach ($param['d']['typePositive']['categories'] as $positiveCategories) {
                            $categories[] = [
                                'day' => $days,
                                'type' => 1,
                                'meal' => 3,
                                'catId' => $positiveCategories
                            ];
                        }
                    }
                    if (isset($param['d']['typePositive']['products']) && count($param['d']['typePositive']['products']) > 0) {
                        foreach ($param['d']['typePositive']['products'] as $positiveProducts) {
                            $products[] = [
                                'day' => $days,
                                'type' => 1,
                                'meal' => 3,
                                'catId' => $positiveProducts
                            ];
                        }
                    }
                }
                if (isset($param['d'])) {
                    if (isset($param['d']['typeNegative']['categories']) && count($param['d']['typeNegative']['categories']) > 0) {
                        foreach ($param['d']['typeNegative']['categories'] as $positiveCategories) {
                            $categories[] = [
                                'day' => $days,
                                'type' => 0,
                                'meal' => 3,
                                'catId' => $positiveCategories
                            ];
                        }
                    }
                    if (isset($param['d']['typeNegative']['products']) && count($param['d']['typeNegative']['products']) > 0) {
                        foreach ($param['d']['typeNegative']['products'] as $positiveProducts) {
                            $products[] = [
                                'day' => $days,
                                'type' => 0,
                                'meal' => 3,
                                'catId' => $positiveProducts
                            ];
                        }
                    }
                }
            }
            $beginDateParsed = explode('/', $beginDate);
            $bDayParsed = '';
            $bMonthParsed = '';
            $bYearParsed = '';

            if (!isset($beginDateParsed[0])) {
                $bDayParsed = date('d');
            } else {
                $bDayParsed = $beginDateParsed[0];
            }
            if (!isset($beginDateParsed[1])) {
                $bMonthParsed = date('m');

            } else {
                $bMonthParsed = $beginDateParsed[1];
            }
            if (!isset($beginDateParsed[2])) {
                $bYearParsed = date('y');

            } else {
                $bYearParsed = $beginDateParsed[2];
            }
            $daysMEnu = ($request['weeks'] * 7) - 1;
            $endDate = date('Y/m/d', strtotime($bYearParsed . '-' . $bMonthParsed . '-' . $bDayParsed . ' + ' . $daysMEnu . 'days'));

            $menuSettingsListModel = new menuSettingsList();
            $listId = $menuSettingsListModel->insertRecords($request['title'], [$bYearParsed, $bMonthParsed, $bDayParsed], explode('/', $endDate));
            if (count($products) > 0) {
                $productsModel = new menuSettingsListProducts();
                $productsModel->insertRecords($products, $listId);
            }
            if (count($categories) > 0) {
                $productsModel = new menuSettingsListCategories();
                $productsModel->insertRecords($categories, $listId);
            }
        }
        //TODO set validation

    }
}
