<?php
/**
 * Created by PhpStorm.
 * User: lyubomir.penchev
 * Date: 11.6.2018
 * Time: 15:29
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\frontControllers\ProfileController;

class FrontSwitchController extends Controller
{
    protected $isLogged;
    protected $username;
    protected $globalVariable;

    public function __construct()
    {
        parent::__construct();
    }

    public function testAction(Request $request)
    {
        $keyword = $request->input('keyword');
        $skills = DB::table('products')->where('name', 'like', '%' . $keyword . '%')
            ->select('name', 'id')
            ->get();
        return json_encode($skills);
    }

    public function homeAction()
    {
        return View::make("front.home.index");

    }

    public function profileAction(Request $request)
    {
        $profileController = new ProfileController();

        $request = $request->post();
        if (isset($request['ajax']) &&$request['type'] == 'productsMenuSElect' ){
            $profileController->fillMenuSelectedProducts($request);
        }
        $parameters = [
            'settingsMenu' => $profileController->settingsMenuAction()
        ];
        return View::make("front.profile.index", $parameters);

    }

    public function newRecipesAction(Request $request)
    {
        return View::make("front.recipes.newRecipes");
    }

    public function menuAction(Request $request)
    {
        return View::make("front.menu.index");
    }
    public function shoppingListAction(Request $request)
    {
        return View::make("front.shoppingList.index");
    }
    public function membersAction(Request $request)
    {
        return View::make("front.members.index");
    }
    public function newsAction(Request $request)
    {
        return View::make("front.news.index");
    }
    public function searchRecipesAction(Request $request)
    {
        return View::make("front.searchRecipes.index");
    }
    public function recipeAction(Request $request)
    {
        return View::make("front.recipe.index");
    }
}