<?php

namespace App\Http\Controllers;

use App\cutRecipes;
use App\homepagenews;
use App\productsRecipes;
use App\receiptMaping;
use App\typeRecipes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Faker\Provider\Image;
use App\categoriesRecipes;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\adminControllers\AdminProductsController;
use App\Http\Controllers\adminControllers\AdminNewsController;
use App\Http\Controllers\adminControllers\AdminReceiptsController;

class AdminSwitchController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function homepageAction()
    {
//        $desc = new receiptMaping();
//        $desc->fillData();
        return View::make("admin.index.index", array('global' => $this->adminGlobalVariables()));


    }

    public function newsAction(Request $request)
    {
        $news = new AdminNewsController();
        $newsProperties = $news->newsAction($request);
        //TODO validate values
        $this->setErrors('notUpdated', 'receiptNotUpdated');
//var_dump("<pre>",$this->adminGlobalVariables());die;
        return View::make("admin.news.index", array(
            'global' => $this->adminGlobalVariables(),
            'count' => $newsProperties['count'],
            'records' => $newsProperties['records']
        ));

    }

    private function ajaxAction($ajax)
    {
        switch ($ajax['type']) {
            case 'addcategory':
            case 'removeCategory':
            case 'addType':
            case 'removeType':
            case 'addCuts':
            case 'removeCuts':
            case 'addProduct':
            case 'productInput':
            case 'removeProduct':
            case 'foodCategory':
            case 'removeFoodCategory':
                $products = new AdminProductsController();
                $products->ajaxAction($ajax);
                break;
            case 'deactivateNews':
            case 'activateNews':
                $news = new AdminNewsController();
                $news->ajaxAction($ajax);
                break;

        }
    }

    public function parametersAction(Request $request)
    {

        $products = new AdminProductsController();
        $parametersProperties = $products->parametersAction($request);

        return View::make("admin.parameters.index", array(
            'global' => $this->adminGlobalVariables(),
            'categoriesList' => $parametersProperties['categoriesList'],
            'foodCategoriesList' => $parametersProperties['foodCategoriesList'],
            'typesList' => $parametersProperties['typesList'],
            'cutsList' => $parametersProperties['cutsList'],
            'countProduct' => $parametersProperties['countProduct'],
            'recordsProduct' => $parametersProperties['recordsProduct'],
            'productPage'=>$parametersProperties['productPage']
        ));

    }

    public function receiptAction(Request $request)
    {
//dd($this->adminGlobalVariables());
        $receiptController = new AdminReceiptsController();
        $receipt = $receiptController->indexAction($request);
        $variablesView = [];
        $variablesView['categories'] = $receipt['category'];
        $variablesView['cuts'] = $receipt['cut'];
        $variablesView['types'] = $receipt['type'];
        if ($request->post()) {
            if (isset($request->post()['updateReceipt']) && $request->post()['updateReceipt'] == 1){
                if ($request->route('receiptId') != null){
                    $response = $receiptController->updateRecipesAction($request, $request->route('receiptId'));
                }else{
                    $this->setErrors('notUpdated', 'receiptNotUpdated');
                }

            }else {
                $response = $receiptController->addRecipesAction($request);
            }
            $variablesView=  array_merge($variablesView, $response);
        }
        if ($request->route('receiptId') != null){
            $receiptData = $receiptController->getRecipesByID($request->route('receiptId'));
            if ($receiptData == 0){
                $this->setErrors('productstitle', 'recipeNotExist');
            }else {
                $variablesView = array_merge($variablesView, $receiptData);
            }
        }
//        dd($variablesView);
        $variablesView['global'] = $this->adminGlobalVariables();

        return View::make("admin.receipts.index", $variablesView);
    }
    public function receiptListAction(Request $request)
    {
        $receiptController = new AdminReceiptsController();
        $receipt = $receiptController->indexAction($request);
        $receiptList = $receiptController->listReceiptsAction($request);
        $variablesView = [];

        $variablesView['categories'] = $receipt['category'];
        $variablesView['cuts'] = $receipt['cut'];
        $variablesView['types'] = $receipt['type'];
        $variablesView['list'] = $receiptList['list'];
//        var_dump("<pre>",$receiptList['list']['records'][0], $receipt['category']);die;
//        if ($request->post()) {
//            $response = $receiptController->addRecipesAction($request);
//
//            $variablesView=  array_merge($variablesView, $response);
//            dd($request, $response, $variablesView);
//
//        }
        $variablesView['global'] = $this->adminGlobalVariables();

        return View::make("admin.receiptsList.index", $variablesView);
    }

    public function usersAction(Request $request)
    {
        $userController = new AdminUsersController();
        $users = $userController->indexAction($request);
        $errors = [];
        if (isset($users['errors'])) {
            $this->setErrors('user', $users['errors']);
//            $errors = $users['errors'];
        }
        return View::make("admin.users.index",
            array(
                'global' => $this->adminGlobalVariables(),
//                'errors' => $errors,
                'records' => $users['records'],
                'count' => $users['count'],
                'master' => $this->masterUser,
                'admin' => $this->adminUser,
                'regular' => $this->regularUser,
                'vip' => $this->vipUser,

            )
        );
    }

    public function usersShowAction($id)
    {
        $userController = new AdminUsersController();
        $users = $userController->userShowAction($id);
        $errors = [];
        if (isset($users['errors'])) {
            $this->setErrors('user', $users['errors']);

//            $errors = $users['errors'];
        }
        return View::make("admin.usersShow.index",
            array(
                'global' => $this->adminGlobalVariables(),
//                'errors' => $errors,


            )
        );
    }
}
