<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $masterUser;
    public $adminUser;
    public $regularUser;
    public $vipUser;
    public $errors = [];
    protected $globalVariable;

    public function __construct()
    {
        $this->shareGlobalVatiables();
        $value = config('userRoles');
        $this->masterUser = $value['masterUser'];
        $this->adminUser = $value['adminUser'];
        $this->regularUser = $value['regularUser'];
        $this->vipUser = $value['vipUser'];


        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $loginArray['isLogged'] = false;
            $this->isLogged = false;
            if (Auth::check()) {
                $this->isLogged = true;
                $this->username = Auth::user()->name;

            }


            view()->share('isLogged', $this->isLogged);
            view()->share('username', $this->username);
//            view()->share('user', $this->user);

            return $next($request);
        });

    }
    public function shareGlobalVatiables(){
        $routes = $this->adminGlobalVariables();
        $this->globalVariable = $routes;
        view()->share('globalVariable', $this->globalVariable);


    }
    public function adminGlobalVariables(){
        $routes = config('adminRouting.routing');
        $frontRoutes = config('frontRouting.routing');
        return ['routes'=>$routes,
            'frontRoutes'=>$frontRoutes,
            'errors'=>$this->errors
        ];
    }

    public function setErrors($key, $value){
        $this->errors[$key] = $value;
    }

}
