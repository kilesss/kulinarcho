<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class productsRecipes extends Model
{
    public $timestamps = true;
    protected $table = 'products';
    protected $getRows = 20;


//
//    public function formatRows(){
//        for($i =1 ; $i<=3726; $i++) {
//            $row = DB::table($this->table)
//                ->where('id', $i)
//                ->first();
//            if (isset($row->name)) {
//                DB::table($this->table)
//                    ->where('id', $i)
//                    ->update(['name' => trim($row->name)]);
//            }else{
//                echo $i,"<hr>";
//            }
//        }
//        die;
//    }
    public function getCutsRecipes()
    {
        return DB::table($this->table)
            ->get();
    }
    public function addProductsRecipes($name)
    {
        $result = $this->getProductByName($name);
        if (isset($result->name) ){
            return 0;
        }else {
            return DB::table($this->table)->insertGetId(
                array('name' => $name)
            );
        }
    }

    public function getPaginator($page)
    {
        $count = DB::table($this->table)->count();
        if ($page > 1) {
            $skip = $page * $this->getRows;
        } else {
            $skip = 0;
        }
        $records = DB::table($this->table)
            ->skip($skip)
            ->take($this->getRows)
            ->get();

        $pages = $count / $this->getRows;
        $pages2 = $count % $this->getRows;
        $countPage = (int)$pages;
        if ($pages2 > 0) {
            $countPage++;
        }

        return array('count' => $countPage, 'records' => $records);
    }
    public function removeProductsRecipes($id){
        DB::table($this->table)->where('id', '=', $id)->delete();
    }
    public function getProductByName($name){
        return DB::table($this->table)
            ->where('name', $name)
            ->first();
    }
    public function getProductByNameAutoFill($name){
        return DB::table($this->table)
            ->where('name','like', "%".$name."%")
            ->take(10)
            ->get();
    }
    public function getProductByID($id){
        return DB::table($this->table)
            ->where('id', $id)
            ->first();
    }
    public function updateProduct($params){
        $isShowOnMenu = 0;
        if($params['showOnMenu'] == 'true'){$isShowOnMenu = 1;}
        return DB::table($this->table)
            ->where('id', $params['id'])
                    ->update([
                        'name' => $params['name'],
                        'hint' => $params['hint'],
                        'calories' => $params['calories'],
                        'proteins' => $params['proteins'],
                        'fat' => $params['fats'],
                        'foodCategoryId' => $params['foodcategoryid'],
                        'showOnMenu' => $isShowOnMenu,
                        'carbohydrates' => $params['carbo'],
                        ]);
    }

}
