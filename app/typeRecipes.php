<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class typeRecipes extends Model
{
    public $timestamps = true;
    protected $table = 'typeRecipes';
    protected $getRows = 10;

    public function getTypeRecipes()
    {
        return DB::table($this->table)
            ->get();
    }
    public function addTypeRecipes($name)
    {
        return DB::table($this->table)->insertGetId(
        array('name' => $name)
    );
    }
    public function removeTypeRecipes($id){
        DB::table($this->table)->where('id', '=', $id)->delete();

    }
}
