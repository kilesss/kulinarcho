<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/fillData', 'FillDataController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('admin')->group(function () {

        Route::prefix('/home')->group(function () {
            Route::get('/', 'AdminSwitchController@homepageAction');
            Route::get('/news', 'AdminSwitchController@newsAction');
            Route::post('/news', 'AdminSwitchController@newsAction');


            Route::prefix('/parameters')->group(function(){
                Route::get('/', 'AdminSwitchController@parametersAction');
                Route::post('/', 'AdminSwitchController@parametersAction');

                Route::get('/products/{productPage?}', 'AdminSwitchController@parametersAction');
            });
            Route::get('/receipt/{receiptId?}', 'AdminSwitchController@receiptAction');
            Route::post('/receipt/{receiptId?}', 'AdminSwitchController@receiptAction');

            Route::get('/receiptList/{receiptPage?}', 'AdminSwitchController@receiptListAction');
            Route::post('/receiptList/{receiptPage?}', 'AdminSwitchController@receiptListAction');

        });
    });
    Route::prefix('/')->group(function () {
        Route::get('/', 'FrontSwitchController@homeAction');
        Route::get('/newRecipes', 'FrontSwitchController@newRecipesAction');
        Route::get('/profile', 'FrontSwitchController@profileAction');
        Route::post('/profile', 'FrontSwitchController@profileAction');

        Route::get('/shoppingList', 'FrontSwitchController@shoppingListAction');
        Route::get('/menu', 'FrontSwitchController@menuAction');
        Route::get('/members', 'FrontSwitchController@membersAction');
    });
});


Route::prefix('/')->group(function () {
    Route::get('/news', 'FrontSwitchController@newsAction');
    Route::get('/searchRecipes', 'FrontSwitchController@searchRecipesAction');
    Route::get('/recipe', 'FrontSwitchController@recipeAction');

});
// tyrsene recepti public
//  novini  public
//  recepta public
// spisyk pazar login
// menu login
// potrebitel i login
//
//
//

    Route::get('find', 'FrontSwitchController@testAction');
Auth::routes();

