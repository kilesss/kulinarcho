-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26 авг 2018 в 21:45
-- Версия на сървъра: 10.1.31-MariaDB
-- PHP Version: 7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homeproject`
--

-- --------------------------------------------------------

--
-- Структура на таблица `menuselectedcategories`
--

CREATE TABLE `menuselectedcategories` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` int(11) NOT NULL,
  `catId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `menuselectedcategories`
--

INSERT INTO `menuselectedcategories` (`id`, `user_id`, `day`, `type`, `catId`) VALUES
(1, 1, '2018-09-01 22:57:41', 0, 9),
(2, 1, '2018-09-01 22:57:41', 0, 11),
(3, 1, '2018-09-01 22:57:41', 0, 13),
(4, 1, '2018-09-01 22:57:52', 1, 9),
(5, 1, '2018-09-01 22:57:52', 1, 11),
(6, 1, '2018-09-01 22:57:52', 1, 13),
(7, 1, '2018-08-27 22:58:48', 1, 11);

-- --------------------------------------------------------

--
-- Структура на таблица `menuselectedproducts`
--

CREATE TABLE `menuselectedproducts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` int(11) NOT NULL,
  `prodId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `menuselectedproducts`
--

INSERT INTO `menuselectedproducts` (`id`, `user_id`, `day`, `type`, `prodId`) VALUES
(1, 1, '2018-08-31 22:54:22', 1, 62),
(2, 1, '2018-08-31 22:54:22', 1, 63),
(3, 1, '2018-08-31 22:54:22', 1, 64),
(4, 1, '2018-08-31 22:56:23', 0, 62),
(5, 1, '2018-08-31 22:56:23', 0, 63),
(6, 1, '2018-08-31 22:56:23', 0, 64),
(7, 1, '2018-08-31 22:56:23', 0, 13),
(8, 1, '2018-08-27 22:58:48', 1, 33);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menuselectedcategories`
--
ALTER TABLE `menuselectedcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menuselectedproducts`
--
ALTER TABLE `menuselectedproducts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menuselectedcategories`
--
ALTER TABLE `menuselectedcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `menuselectedproducts`
--
ALTER TABLE `menuselectedproducts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
