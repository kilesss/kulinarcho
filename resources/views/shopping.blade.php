@include('front.header')

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="form-group  col-md-12">
                <label for="exampleFormControlSelect1">Кой магазин</label>
                <select class="form-control col-md-12" id="typeShopping">
                    <option value="0">Избери</option>
                    <option value="20">Метро</option>
                    <option value="0">Друг</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-12">
                <label for="exampleFormControlInput1">Цена</label>
                <input type="number" class="form-control  col-md-12" id="price">
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-12">
                <label for="exampleFormControlInput1">Количество</label>
                <input type="number" class="form-control  col-md-12" id="volume">
            </div>
        </div>
        <div CLASS="row">
            <div class="col-md-12">
                <div class="col-md-6" style="width: 50%;"><i>Временна цена: </i></div>
            <div id="tempPrice"  class="col-md-6" style="width: 49%;"> <i>145</i></div>
            </div>
        </div>
        <div CLASS="row">
            <div class="form-group  col-md-12">
            <div class="col-md-12 btn btn-success form-control" id="submitShopping">Добави</div>
                </div>
        </div>
        <div class="row">
            <div class="col-md-5" style="width: 49%">
                <div class="row">Изхарчена сума:</div>
                <div class="row" style="color: red" id="totalSum">{{$total}}</div>
            </div>
        </div>
        <div class="row" id="allredyBuyed">
            @foreach ($list as $l)
                <div class="row" style="width: 100%;background:  white;color:  black;border-radius: 10px;margin-top: 3%;">'+
                        <div class="col-md-3" style="width: 15%">{{$l['price']}}
                        </div>
                    <div class="col-md-3" style="width: 15%">
                        {{$l['volume']}}
                               </div>
                           <div class="col-md-3" style="width: 22%">
                                       {{$l['dds']}}
                           </div>
                    <div class="col-md-3" style="width: 22%;  color: red">
                        {{$l['tempPrice']}}
                    </div>
                </div>
            @endforeach
    </div>
        <div class="row">
            <div class="col-md-12 btn btn-danger" id="finnishShopping">Завърши покупката</div>
        </div>
</div>

<script>
    var dds = 0;
    var tempPirce = 0;
    var totalSum = 0;
    var price = 0;
    var volume = 0;
    var lasttempPirce = 0;
    $("#typeShopping").change(function(){
        dds = $( "#typeShopping" ).val();
    });
    $("#price").on("change paste keyup", function() {
        if ($(this).val()> 0 && $("#volume").val()> 0) {
         var ddscalculate = (dds/100)+1;
            tempPirce = $(this).val() * $("#volume").val()*ddscalculate;
            tempPirce = tempPirce.toFixed(2);
            price = $("#price").val();
            volume = $("#volume").val();
            lasttempPirce = tempPirce;
            $("#tempPrice").html(tempPirce)
        }
    });
    $("#volume").on("change paste keyup", function() {
        if ($(this).val()> 0 && $("#price").val()> 0)
        {
            var ddscalculate = (dds/100)+1;
            tempPirce = $(this).val() * $("#price").val()*ddscalculate;
            tempPirce = tempPirce.toFixed(2);
            price = $("#price").val();
            volume = $("#volume").val();
            lasttempPirce = tempPirce;
            $("#tempPrice").html(tempPirce)
        }
    });
    $("#submitShopping").click(function(){
       totalSum = parseFloat(totalSum)+parseFloat(tempPirce);
        price = $("#price").val();
        volume = $("#volume").val();
        lasttempPirce = tempPirce;
        $("#tempPrice").html(0);
        $("#price").val('');
        $("#volume").val('');
        tempPirce = 0;
        $("#totalSum").html(totalSum);
        if (lasttempPirce> 0 && price> 0 && volume> 0) {
            var formData = {
                price: price,
                volume: volume,
                dds: dds,
                type: 'newShoping',
            };
           var responce = submitBuyng(formData);
            $("#allredyBuyed").append(renderRowAlredyBuyed())
        }
    });
    function renderRowAlredyBuyed(){
        return '<div class="row" style="width: 100%;background:  white;color:  black;border-radius: 10px;margin-top: 3%;">'+
        '    <div class="col-md-3" style="width: 24%">'+
        '        '+price+''+
        '</div>'+
        '<div class="col-md-3" style="width: 24%">'+
volume+
        '        </div>'+
        '        <div class="col-md-3" style="width: 24%">'+
        '        <div class="row">'+dds+'</div>'+
        '        </div>'+
        '        <div class="col-md-3" style="width: 24%">'+
        '<div class="row" style="color: red" id="totalSum">'+lasttempPirce+'</div>'+
        '        </div>'+
        '        </div>'
    }


    function submitBuyng(data){
        var responce = 0;
        $.ajax({
            type:'POST',
            url:'shopping',
            data:data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            finish:function(data){
//                return data;
            }
        }).done(function(data){
            responce = data;

        });
    }
    $("#finnishShopping").click(function(){
        $.ajax({
            type:'POST',
            url:'shopping',
            data:{
                type: 'finnsihShoping',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            finish:function(data){
                location.reload();
            }
        }).done(function(data){
            location.reload();

        });
    })
</script>

@include('front.footer')
