@include('front.header')

<style>
    .slide-item{
        height: 450px!important;
    }
    .slider-caption {
        bottom: 0!important;
        background: #fff0;
    }
</style>
<section class="main-slider" data-auto="5000">
    <div class="main-slider-item">
        <img width="1349" height="597" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920.jpg"
             class="slide-item img-responsive wp-post-image" alt=""
             srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920.jpg 1349w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-300x133.jpg 300w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-1024x453.jpg 1024w" sizes="(max-width: 1349px) 100vw, 1349px" />				<div class="slider-caption">
            <div class="slider-caption-overlay"></div>
            <div class="main-caption-content">
                <a href="http://demo.djmimi.net/themes/recipe/recipe/feta-cheese-turkey-burgers/">
                    <h1 class="animation">Feta Cheese Turkey Burgers</h1>
                </a>
                <p class="slider-excerpt">
                    Ground turkey makes a great burger. It makes an even better one with feta cheese and kalamata olives.						</p>
                <ul class="list-unstyled list-inline recipe-meta clearfix">
                    <li>
                        <div class="avatar">
                            <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_4-40x40.png" alt="author" width="40" height="40"/>


                            By 									<a href="http://demo.djmimi.net/themes/recipe/author/sally/">
                                Sally Marley
                            </a>
                        </div>
                    </li>
                    <li>
									<span class="tip level advanced" data-title="Difficulty: Advanced">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                    </li>
                    <li class="tip" data-title="Yield">
                        <i class="fa fa-table"></i>
                        5 Burgers								</li>

                    <li class="tip" data-title="Servings">
                        <i class="fa fa-users"></i>
                        5 People								</li>

                    <li class="tip" data-title="Cook Time">
                        <i class="fa fa-clock-o"></i>
                        15min								</li>
                </ul>
            </div>
        </div>
    </div>
    {{--<div class="main-slider-item">--}}
        {{--<img width="1349" height="597" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pizza-346985_1920.jpg" class="slide-item img-responsive wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pizza-346985_1920.jpg 1349w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pizza-346985_1920-300x133.jpg 300w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pizza-346985_1920-1024x453.jpg 1024w" sizes="(max-width: 1349px) 100vw, 1349px" />				<div class="slider-caption">--}}
            {{--<canvas id="canvas_2"></canvas>--}}
            {{--<div class="slider-caption-overlay"></div>--}}
            {{--<div class="main-caption-content">--}}
                {{--<a href="http://demo.djmimi.net/themes/recipe/recipe/cos-and-tomato-salad/">--}}
                    {{--<h1 class="animation">Cos and tomato salad</h1>--}}
                {{--</a>--}}
                {{--<p class="slider-excerpt">--}}
                    {{--Juicy, sweet tomatoes brighten any menu. Toss them into one of these creative, popular salads the next time you want to freshen a meal.						</p>--}}
                {{--<ul class="list-unstyled list-inline recipe-meta clearfix">--}}
                    {{--<li>--}}
                        {{--<div class="avatar">--}}
                            {{--<img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_8-40x40.png" alt="author" width="40" height="40"/>--}}


                            {{--By 									<a href="http://demo.djmimi.net/themes/recipe/author/mark/">--}}
                                {{--Mark McAlly--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li>--}}
									{{--<span class="tip level advanced" data-title="Difficulty: Advanced">--}}
		{{--<span class="level-bar-1"></span>--}}
		{{--<span class="level-bar-2"></span>--}}
		{{--<span class="level-bar-3"></span>--}}
	{{--</span>--}}
                    {{--</li>--}}
                    {{--<li class="tip" data-title="Yield">--}}
                        {{--<i class="fa fa-table"></i>--}}
                        {{--2 Bowles								</li>--}}

                    {{--<li class="tip" data-title="Servings">--}}
                        {{--<i class="fa fa-users"></i>--}}
                        {{--2 People								</li>--}}

                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="main-slider-item">--}}
        {{--<img width="1349" height="597" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920.jpg" class="slide-item img-responsive wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920.jpg 1349w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920-300x133.jpg 300w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920-1024x453.jpg 1024w" sizes="(max-width: 1349px) 100vw, 1349px" />				<div class="slider-caption">--}}
            {{--<canvas id="canvas_3"></canvas>--}}
            {{--<div class="slider-caption-overlay"></div>--}}
            {{--<div class="main-caption-content">--}}
                {{--<a href="http://demo.djmimi.net/themes/recipe/recipe/brazilian-black-bean-stew/">--}}
                    {{--<h1 class="animation">Brazilian Black Bean Stew</h1>--}}
                {{--</a>--}}
                {{--<p class="slider-excerpt">--}}
                    {{--This easy yet wonderful stew can be made any time of year. The meat can be omitted without compromising the dish.						</p>--}}
                {{--<ul class="list-unstyled list-inline recipe-meta clearfix">--}}
                    {{--<li>--}}
                        {{--<div class="avatar">--}}
                            {{--<img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_6-40x40.png" alt="author" width="40" height="40"/>--}}


                            {{--By 									<a href="http://demo.djmimi.net/themes/recipe/author/nella/">--}}
                                {{--Nella Heart--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li>--}}
									{{--<span class="tip level easy" data-title="Difficulty: Easy">--}}
		{{--<span class="level-bar-1"></span>--}}
		{{--<span class="level-bar-2"></span>--}}
		{{--<span class="level-bar-3"></span>--}}
	{{--</span>--}}
                    {{--</li>--}}
                    {{--<li class="tip" data-title="Yield">--}}
                        {{--<i class="fa fa-table"></i>--}}
                        {{--2 Bowls								</li>--}}

                    {{--<li class="tip" data-title="Servings">--}}
                        {{--<i class="fa fa-users"></i>--}}
                        {{--2 People								</li>--}}

                    {{--<li class="tip" data-title="Cook Time">--}}
                        {{--<i class="fa fa-clock-o"></i>--}}
                        {{--15min								</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
</section>
<p>	<section>

@include('front.menu')

        <div class="container">
            <div class="section-title clearfix">
                <h3 class="pull-left">
                    <i class="fa fa-filter"></i>
                    Quick Filter				</h3>
                <a href="http://demo.djmimi.net/themes/recipe/recipes-search/" class="btn pull-right">All Categories</a>
            </div>
            <div class="row category-list">
                <div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=beverages"><span class="icon icon_198"></span> Beverages</a></div><div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=breads"><span class="icon icon_173"></span> Breads</a></div><div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=breakfast"><span class="icon icon_56"></span> Breakfast</a></div><div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=burgers"><span class="icon icon_71"></span> Burgers</a></div><div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=desserts"><span class="icon icon_146"></span> Desserts</a></div><div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=lunch"><span class="icon icon_176"></span> Lunch</a></div><div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=salads"><span class="icon icon_49"></span> Salads</a></div><div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=soups"><span class="icon icon_102"></span> Soups</a></div><div class="col-sm-4"><a href="http://demo.djmimi.net/themes/recipe/recipes-search/?recipe-category=vegetarian"><span class="icon icon_45"></span> Vegetarian</a></div>			</div>
        </div>
    </section>
    <section class="">
        <div class="container">
            <div class="section-title clearfix">
                <h3 class="pull-left">
                    <i class="fa fa-star"></i>
                    Top Rated Recipes					</h3>
                <a href="http://demo.djmimi.net/themes/recipe/recipes-search/?sort=ratings-desc" class="btn pull-right">All Top Rated Recipes</a>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="recipe-box white-block">
                        <div class="blog-media">
                            <div class="embed-responsive embed-responsive-16by9">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/"><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-360x203.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></a></div>		<div class="ratings">
                                <span class="bottom-ratings tip" data-title="Average Rate: 4.5 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 90%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>		</div>
                        </div>
                        <div class="content-inner">
                            <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/" class="blog-title">
                                <h4>Homemade Granola</h4>
                            </a>

<p>This is a great recipe for homemade granola. I make it weekly! Any dried fruit can be substituted for the raisins. I use dried cranberries.</p>

<div class="avatar">
    <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_11-25x25.png" alt="author" width="25" height="25"/>


    By 			<a href="http://demo.djmimi.net/themes/recipe/author/john/">
        John Doe
    </a>
</div>
</div>
<div class="content-footer">
    <div class="content-inner">
        <ul class="list-unstyled list-inline recipe-meta clearfix">
            <li>
						<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
            </li>
            <li class="tip" data-title="Yield">
                <i class="fa fa-table"></i>
                Few Dozen					</li>

            <li class="tip" data-title="Servings">
                <i class="fa fa-users"></i>
                3 People					</li>

        </ul>
    </div>
</div>
</div>						</div>
<div class="col-sm-4">
    <div class="recipe-box white-block">
        <div class="blog-media">
            <div class="embed-responsive embed-responsive-16by9">
                <a href="http://demo.djmimi.net/themes/recipe/recipe/chilled-mocha/"><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-360x203.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></a></div>		<div class="ratings">
                <span class="bottom-ratings tip" data-title="Average Rate: 4.33 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 86.6%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>		</div>
        </div>
        <div class="content-inner">
            <a href="http://demo.djmimi.net/themes/recipe/recipe/chilled-mocha/" class="blog-title">
                <h4>Chilled mocha</h4>
            </a>

            <p>First, make some coffee ice cubes, then blend them together with milk and chocolate syrup for a frosty treat!</p>

            <div class="avatar">
                <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_4-25x25.png" alt="author" width="25" height="25"/>


                By 			<a href="http://demo.djmimi.net/themes/recipe/author/sally/">
                    Sally Marley
                </a>
            </div>
        </div>
        <div class="content-footer">
            <div class="content-inner">
                <ul class="list-unstyled list-inline recipe-meta clearfix">
                    <li>
						<span class="tip level advanced" data-title="Difficulty: Advanced">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                    </li>
                    <li class="tip" data-title="Yield">
                        <i class="fa fa-table"></i>
                        1 Liter					</li>

                    <li class="tip" data-title="Servings">
                        <i class="fa fa-users"></i>
                        4 People					</li>

                    <li class="tip" data-title="Cook Time">
                        <i class="fa fa-clock-o"></i>
                        3h 15min					</li>
                </ul>
            </div>
        </div>
    </div>						</div>
<div class="col-sm-4">
    <div class="recipe-box white-block">
        <div class="blog-media">
            <div class="embed-responsive embed-responsive-16by9">
                <a href="http://demo.djmimi.net/themes/recipe/recipe/feta-cheese-turkey-burgers/"><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-360x203.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></a></div>		<div class="ratings">
                <span class="bottom-ratings tip" data-title="Average Rate: 4.14 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 82.8%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>		</div>
        </div>
        <div class="content-inner">
            <a href="http://demo.djmimi.net/themes/recipe/recipe/feta-cheese-turkey-burgers/" class="blog-title">
                <h4>Feta Cheese Turkey Burgers</h4>
            </a>

            <p>Ground turkey makes a great burger. It makes an even better one with feta cheese and kalamata olives.</p>

            <div class="avatar">
                <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_4-25x25.png" alt="author" width="25" height="25"/>


                By 			<a href="http://demo.djmimi.net/themes/recipe/author/sally/">
                    Sally Marley
                </a>
            </div>
        </div>
        <div class="content-footer">
            <div class="content-inner">
                <ul class="list-unstyled list-inline recipe-meta clearfix">
                    <li>
						<span class="tip level advanced" data-title="Difficulty: Advanced">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                    </li>
                    <li class="tip" data-title="Yield">
                        <i class="fa fa-table"></i>
                        5 Burgers					</li>

                    <li class="tip" data-title="Servings">
                        <i class="fa fa-users"></i>
                        5 People					</li>

                    <li class="tip" data-title="Cook Time">
                        <i class="fa fa-clock-o"></i>
                        15min					</li>
                </ul>
            </div>
        </div>
    </div>						</div>
</div>
</div>
</section>
<section class="">
    <div class="container">
        <div class="section-title clearfix">
            <h3 class="pull-left">
                <i class="fa fa-clock-o"></i>
                Latest Recipes					</h3>
            <a href="http://demo.djmimi.net/themes/recipe/recipes-search/?sort=date-desc" class="btn pull-right">All Latest Recipes</a>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="recipe-box white-block">
                    <div class="blog-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <a href="http://demo.djmimi.net/themes/recipe/recipe/french-onion-soup-gratinee/"><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-360x203.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></a></div>		<div class="ratings">
                            <span class="bottom-ratings tip" data-title="Average Rate: 4 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 80%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>		</div>
                    </div>
                    <div class="content-inner">
                        <a href="http://demo.djmimi.net/themes/recipe/recipe/french-onion-soup-gratinee/" class="blog-title">
                            <h4>French Onion Soup Gratinee</h4>
                        </a>

                        <p>About as good as it gets! This is the version of French Onion Soup that people seek when they go to restaurants. Healthy and easy.</p>

                        <div class="avatar">
                            <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_7-25x25.png" alt="author" width="25" height="25"/>


                            By 			<a href="http://demo.djmimi.net/themes/recipe/author/nick/">
                                Nick Slick
                            </a>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="content-inner">
                            <ul class="list-unstyled list-inline recipe-meta clearfix">
                                <li>
						<span class="tip level advanced" data-title="Difficulty: Advanced">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                </li>
                                <li class="tip" data-title="Yield">
                                    <i class="fa fa-table"></i>
                                    4 Bowls					</li>

                                <li class="tip" data-title="Servings">
                                    <i class="fa fa-users"></i>
                                    4 People					</li>

                                <li class="tip" data-title="Cook Time">
                                    <i class="fa fa-clock-o"></i>
                                    5min					</li>
                            </ul>
                        </div>
                    </div>
                </div>						</div>
            <div class="col-sm-4">
                <div class="recipe-box white-block">
                    <div class="blog-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <a href="http://demo.djmimi.net/themes/recipe/recipe/avocado-panzanella-salad/"><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-360x203.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></a></div>		<div class="ratings">
                            <span class="bottom-ratings tip" data-title="Average Rate: 3.88 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 77.6%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>		</div>
                    </div>
                    <div class="content-inner">
                        <a href="http://demo.djmimi.net/themes/recipe/recipe/avocado-panzanella-salad/" class="blog-title">
                            <h4>Avocado panzanella salad</h4>
                        </a>

                        <p>This high fibre salad is a vibrant mix of tomatoes, avocado and crunchy pieces of ciabatta. Thissalad is full of the authentic flavours of Italy.</p>

                        <div class="avatar">
                            <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_1-25x25.png" alt="author" width="25" height="25"/>


                            By 			<a href="http://demo.djmimi.net/themes/recipe/author/admin/">
                                Melissa Hearth
                            </a>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="content-inner">
                            <ul class="list-unstyled list-inline recipe-meta clearfix">
                                <li>
						<span class="tip level medium" data-title="Difficulty: Medium">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                </li>
                                <li class="tip" data-title="Yield">
                                    <i class="fa fa-table"></i>
                                    3 Bowles					</li>

                                <li class="tip" data-title="Servings">
                                    <i class="fa fa-users"></i>
                                    4 People					</li>

                                <li class="tip" data-title="Cook Time">
                                    <i class="fa fa-clock-o"></i>
                                    15min					</li>
                            </ul>
                        </div>
                    </div>
                </div>						</div>
            <div class="col-sm-4">
                <div class="recipe-box white-block">
                    <div class="blog-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <a href="http://demo.djmimi.net/themes/recipe/recipe/kale-and-feta-salad/"><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-374173_1920-360x203.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-374173_1920-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-374173_1920-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-374173_1920-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></a></div>		<div class="ratings">
                            <span class="bottom-ratings tip" data-title="Average Rate: 4 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 80%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>		</div>
                    </div>
                    <div class="content-inner">
                        <a href="http://demo.djmimi.net/themes/recipe/recipe/kale-and-feta-salad/" class="blog-title">
                            <h4>Kale and Feta Salad</h4>
                        </a>

                        <p>Salads can be healthy, satisfying meals on their own or perfect accompaniments to main dishes. We&#8217;ve got a great selection of salads.</p>

                        <div class="avatar">
                            <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_6-25x25.png" alt="author" width="25" height="25"/>


                            By 			<a href="http://demo.djmimi.net/themes/recipe/author/nella/">
                                Nella Heart
                            </a>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="content-inner">
                            <ul class="list-unstyled list-inline recipe-meta clearfix">
                                <li>
						<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                </li>
                                <li class="tip" data-title="Yield">
                                    <i class="fa fa-table"></i>
                                    3 Bowles					</li>

                                <li class="tip" data-title="Servings">
                                    <i class="fa fa-users"></i>
                                    4 People					</li>

                                <li class="tip" data-title="Cook Time">
                                    <i class="fa fa-clock-o"></i>
                                    15min					</li>
                            </ul>
                        </div>
                    </div>
                </div>						</div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-title clearfix">
            <h3 class="pull-left">
                <i class="fa fa-users"></i>
                Top Rated Authors				</h3>
            <a href="http://demo.djmimi.net/themes/recipe/members/" class="btn pull-right">All Authors</a>
        </div>
        <div class="row">
            <div class="col-sm-2 col-xs-4">
                <div class="user-block">
                    <a href="http://demo.djmimi.net/themes/recipe/author/sally/" data-title="Sally Marley" class="tip">
                        <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_4.png" class="img-responsive" alt="author" width="150" height="150"/>
									<span class="user-block-overlay animation">
										<i class="fa fa-plus-circle animation"></i>
									</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-2 col-xs-4">
                <div class="user-block">
                    <a href="http://demo.djmimi.net/themes/recipe/author/sara/" data-title="Sara Parker" class="tip">
                        <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_2.png" class="img-responsive" alt="author" width="150" height="150"/>
									<span class="user-block-overlay animation">
										<i class="fa fa-plus-circle animation"></i>
									</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-2 col-xs-4">
                <div class="user-block">
                    <a href="http://demo.djmimi.net/themes/recipe/author/nella/" data-title="Nella Heart" class="tip">
                        <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_6.png" class="img-responsive" alt="author" width="150" height="150"/>
									<span class="user-block-overlay animation">
										<i class="fa fa-plus-circle animation"></i>
									</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-2 col-xs-4">
                <div class="user-block">
                    <a href="http://demo.djmimi.net/themes/recipe/author/admin/" data-title="Melissa Hearth" class="tip">
                        <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_1.png" class="img-responsive" alt="author" width="150" height="150"/>
									<span class="user-block-overlay animation">
										<i class="fa fa-plus-circle animation"></i>
									</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-2 col-xs-4">
                <div class="user-block">
                    <a href="http://demo.djmimi.net/themes/recipe/author/nick/" data-title="Nick Slick" class="tip">
                        <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_7.png" class="img-responsive" alt="author" width="150" height="150"/>
									<span class="user-block-overlay animation">
										<i class="fa fa-plus-circle animation"></i>
									</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-2 col-xs-4">
                <div class="user-block">
                    <a href="http://demo.djmimi.net/themes/recipe/author/marina/" data-title="Marina Doe" class="tip">
                        <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_9.png" class="img-responsive" alt="author" width="150" height="150"/>
									<span class="user-block-overlay animation">
										<i class="fa fa-plus-circle animation"></i>
									</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="">
    <div class="container">
        <div class="section-title clearfix">
            <h3 class="pull-left">
                <i class="fa fa-heart"></i>
                Most Favourited Recipes					</h3>
            <a href="http://demo.djmimi.net/themes/recipe/recipes-search/?sort=favourited-desc" class="btn pull-right">All Most Favourited Recipes</a>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="recipe-box recipe-box-alt white-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="blog-media">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <a href="http://demo.djmimi.net/themes/recipe/recipe/creamy-strawberry-crepes/"><img width="263" height="148" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-263x148.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-263x148.jpg 263w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-360x203.jpg 360w" sizes="(max-width: 263px) 100vw, 263px" /></a></div>				<div class="ratings">
                                    <span class="bottom-ratings tip" data-title="Average Rate: 3.83 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 76.6%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>				</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="content-inner content-inner-alt">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/creamy-strawberry-crepes/" class="blog-title">
                                    <h5>Creamy Strawberry Crepes</h5>
                                </a>

                                <p>This recipe has been a family favorite for over 30 years! Thes&#8230;</p>
                            </div>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="content-inner">
                            <ul class="list-unstyled list-inline recipe-meta clearfix">
                                <li>
						<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                </li>
                                <li>
                                    <div class="avatar">
                                        <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_9-25x25.png" alt="author" width="25" height="25"/>


                                        By 						<a href="http://demo.djmimi.net/themes/recipe/author/marina/">
                                            Marina Doe
                                        </a>
                                    </div>
                                </li>
                                <li class="tip" data-title="Yield">
                                    <i class="fa fa-table"></i>
                                    Few Dozen					</li>

                                <li class="tip" data-title="Servings">
                                    <i class="fa fa-users"></i>
                                    3 People					</li>

                            </ul>
                        </div>
                    </div>
                </div>						</div>
            <div class="col-sm-6">
                <div class="recipe-box recipe-box-alt white-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="blog-media">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <a href="http://demo.djmimi.net/themes/recipe/recipe/french-onion-soup-gratinee/"><img width="263" height="148" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-263x148.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-263x148.jpg 263w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-360x203.jpg 360w" sizes="(max-width: 263px) 100vw, 263px" /></a></div>				<div class="ratings">
                                    <span class="bottom-ratings tip" data-title="Average Rate: 4 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 80%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>				</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="content-inner content-inner-alt">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/french-onion-soup-gratinee/" class="blog-title">
                                    <h5>French Onion Soup Gratinee</h5>
                                </a>

                                <p>About as good as it gets! This is the version of French Onion &#8230;</p>
                            </div>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="content-inner">
                            <ul class="list-unstyled list-inline recipe-meta clearfix">
                                <li>
						<span class="tip level advanced" data-title="Difficulty: Advanced">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                </li>
                                <li>
                                    <div class="avatar">
                                        <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/av_7-25x25.png" alt="author" width="25" height="25"/>


                                        By 						<a href="http://demo.djmimi.net/themes/recipe/author/nick/">
                                            Nick Slick
                                        </a>
                                    </div>
                                </li>
                                <li class="tip" data-title="Yield">
                                    <i class="fa fa-table"></i>
                                    4 Bowls					</li>

                                <li class="tip" data-title="Servings">
                                    <i class="fa fa-users"></i>
                                    4 People					</li>

                                <li class="tip" data-title="Cook Time">
                                    <i class="fa fa-clock-o"></i>
                                    5min					</li>
                            </ul>
                        </div>
                    </div>
                </div>						</div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-title clearfix">
            <h3 class="pull-left">
                <i class="fa fa-pencil-square-o"></i>
                Lates Articles				</h3>
            <a href="http://demo.djmimi.net/themes/recipe/blog/" class="btn pull-right">All Articles</a>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="blog-item white-block post-61 post type-post status-publish format-image has-post-thumbnail hentry category-image tag-blog tag-image-2 tag-recipe tag-test post_format-post-format-image">
                    <div class="blog-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <a href="http://demo.djmimi.net/themes/recipe/image-post-format/">
                                <img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/hamburger-515389_1280-360x203.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/hamburger-515389_1280-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/hamburger-515389_1280-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/hamburger-515389_1280-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" />	</a>
                        </div>									</div>
                    <div class="content-inner">

                        <div class="blog-title-wrap">
                            <a href="http://demo.djmimi.net/themes/recipe/image-post-format/" class="blog-title">
                                <h4>Image Post Format</h4>
                            </a>
                            <p class="post-meta clearfix">
                                By 											<a href="http://demo.djmimi.net/themes/recipe/author/john/">
                                    John Doe											</a>
                                in 											<a href="http://demo.djmimi.net/themes/recipe/category/image/">Image</a> 											<span class="pull-right">
											July 11, 2014											</span>
                            </p>
                        </div>

                        <p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>

                        <div class="clearfix">
                            <a href="http://demo.djmimi.net/themes/recipe/image-post-format/" class="btn pull-left">
                                Continue reading										</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="blog-item white-block post-44 post type-post status-publish format-standard has-post-thumbnail hentry category-standard tag-blog tag-recipe tag-standard-2 tag-test">
                    <div class="blog-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <a href="http://demo.djmimi.net/themes/recipe/standard-post-format/"><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/cabinet-334128_1280-360x203.jpg" class="embed-responsive-item wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/cabinet-334128_1280-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/cabinet-334128_1280-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/cabinet-334128_1280-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></a></div>									</div>
                    <div class="content-inner">

                        <div class="blog-title-wrap">
                            <a href="http://demo.djmimi.net/themes/recipe/standard-post-format/" class="blog-title">
                                <h4>Standard Post Format</h4>
                            </a>
                            <p class="post-meta clearfix">
                                By 											<a href="http://demo.djmimi.net/themes/recipe/author/rick/">
                                    Rick Newton											</a>
                                in 											<a href="http://demo.djmimi.net/themes/recipe/category/standard/">Standard</a> 											<span class="pull-right">
											February 11, 2015											</span>
                            </p>
                        </div>

                        <p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>

                        <div class="clearfix">
                            <a href="http://demo.djmimi.net/themes/recipe/standard-post-format/" class="btn pull-left">
                                Continue reading										</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="blog-item white-block post-18 post type-post status-publish format-gallery has-post-thumbnail hentry category-galleries tag-blog tag-gallery tag-recipe tag-slider tag-test post_format-post-format-gallery">
                    <div class="blog-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <ul class="list-unstyled post-slider embed-responsive-item">
                                <li><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/spice-370114_1280-360x203.jpg" class="embed-responsive-item" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/spice-370114_1280-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/spice-370114_1280-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/spice-370114_1280-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></li><li><img width="360" height="203" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/chocolate-599516_1280-360x203.jpg" class="embed-responsive-item" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/chocolate-599516_1280-360x203.jpg 360w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/chocolate-599516_1280-848x477.jpg 848w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2014/07/chocolate-599516_1280-263x148.jpg 263w" sizes="(max-width: 360px) 100vw, 360px" /></li>			</ul>
                        </div>									</div>
                    <div class="content-inner">

                        <div class="blog-title-wrap">
                            <a href="http://demo.djmimi.net/themes/recipe/slider-gallery/" class="blog-title">
                                <h4>Gallery Post Format Slider</h4>
                            </a>
                            <p class="post-meta clearfix">
                                By 											<a href="http://demo.djmimi.net/themes/recipe/author/nick/">
                                    Nick Slick											</a>
                                in 											<a href="http://demo.djmimi.net/themes/recipe/category/galleries/">Gallery</a> 											<span class="pull-right">
											January 11, 2015											</span>
                            </p>
                        </div>

                        <p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>

                        <div class="clearfix">
                            <a href="http://demo.djmimi.net/themes/recipe/slider-gallery/" class="btn pull-left">
                                Continue reading										</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</p>

<a href="javascript:;" class="to_top btn">
    <span class="fa fa-angle-up"></span>
</a>

<section class="footer_widget_section">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="widget white-block clearfix widget_widget_recipe" ><div class="widget-title-wrap"><h5 class="widget-title">Most Liked Recipes</h5></div><ul class="list-unstyled no-top-padding"><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/creamy-strawberry-crepes/">
                                    Creamy Strawberry Crepes
                                </a>
                                <p class="grey"><i class="fa fa-thumbs-o-up tip" data-title="Likes"></i>264</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/french-onion-soup-gratinee/">
                                    French Onion Soup Gratinee
                                </a>
                                <p class="grey"><i class="fa fa-thumbs-o-up tip" data-title="Likes"></i>174</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/">
                                    Homemade Granola
                                </a>
                                <p class="grey"><i class="fa fa-thumbs-o-up tip" data-title="Likes"></i>116</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/chilled-mocha/">
                                    Chilled mocha
                                </a>
                                <p class="grey"><i class="fa fa-thumbs-o-up tip" data-title="Likes"></i>103</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/avocado-panzanella-salad/">
                                    Avocado panzanella salad
                                </a>
                                <p class="grey"><i class="fa fa-thumbs-o-up tip" data-title="Likes"></i>53</p>
                            </div>
                            <div class="clearfix"></div>
                        </li></ul></div>				</div>
            <div class="col-md-3">
                <div class="widget white-block clearfix widget_widget_recipe" ><div class="widget-title-wrap"><h5 class="widget-title">Most Viewed Recipes</h5></div><ul class="list-unstyled no-top-padding"><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/creamy-strawberry-crepes/">
                                    Creamy Strawberry Crepes
                                </a>
                                <p class="grey"><i class="fa fa-eye tip" data-title="Views"></i>7940</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/french-onion-soup-gratinee/">
                                    French Onion Soup Gratinee
                                </a>
                                <p class="grey"><i class="fa fa-eye tip" data-title="Views"></i>6111</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/">
                                    Homemade Granola
                                </a>
                                <p class="grey"><i class="fa fa-eye tip" data-title="Views"></i>5632</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/chilled-mocha/">
                                    Chilled mocha
                                </a>
                                <p class="grey"><i class="fa fa-eye tip" data-title="Views"></i>3673</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/delicious-chocolate-cake/">
                                    Delicious Chocolate Cake
                                </a>
                                <p class="grey"><i class="fa fa-eye tip" data-title="Views"></i>2271</p>
                            </div>
                            <div class="clearfix"></div>
                        </li></ul></div>				</div>
            <div class="col-md-3">
                <div class="widget white-block clearfix widget_widget_recipe" ><div class="widget-title-wrap"><h5 class="widget-title">Top Rated Recipes</h5></div><ul class="list-unstyled no-top-padding"><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/">
                                    Homemade Granola
                                </a>
                                <p class="grey"><span class="bottom-ratings tip" data-title="Average Rate: 4.5 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 90%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span></p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/chilled-mocha/">
                                    Chilled mocha
                                </a>
                                <p class="grey"><span class="bottom-ratings tip" data-title="Average Rate: 4.33 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 86.6%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span></p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/feta-cheese-turkey-burgers/">
                                    Feta Cheese Turkey Burgers
                                </a>
                                <p class="grey"><span class="bottom-ratings tip" data-title="Average Rate: 4.14 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 82.8%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span></p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/brownie-548591-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/delicious-chocolate-cake/">
                                    Delicious Chocolate Cake
                                </a>
                                <p class="grey"><span class="bottom-ratings tip" data-title="Average Rate: 4.14 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 82.8%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span></p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/french-onion-soup-gratinee/">
                                    French Onion Soup Gratinee
                                </a>
                                <p class="grey"><span class="bottom-ratings tip" data-title="Average Rate: 4 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 80%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span></p>
                            </div>
                            <div class="clearfix"></div>
                        </li></ul></div>				</div>
            <div class="col-md-3">
                <div class="widget white-block clearfix widget_widget_recipe" ><div class="widget-title-wrap"><h5 class="widget-title">Most Favourited Recipes</h5></div><ul class="list-unstyled no-top-padding"><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/pancakes-282239_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/creamy-strawberry-crepes/">
                                    Creamy Strawberry Crepes
                                </a>
                                <p class="grey"><i class="fa fa-heart-o tip" data-title="Favourites"></i>34</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/france-confectionery-83373-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/french-onion-soup-gratinee/">
                                    French Onion Soup Gratinee
                                </a>
                                <p class="grey"><i class="fa fa-heart-o tip" data-title="Favourites"></i>22</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_2-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/chilled-mocha/">
                                    Chilled mocha
                                </a>
                                <p class="grey"><i class="fa fa-heart-o tip" data-title="Favourites"></i>21</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/">
                                    Homemade Granola
                                </a>
                                <p class="grey"><i class="fa fa-heart-o tip" data-title="Favourites"></i>20</p>
                            </div>
                            <div class="clearfix"></div>
                        </li><li class="top-authors">
                            <div class="widget-image-thumb">
                                <img width="50" height="50" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-50x50.jpg" class="img-responsive" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-40x40.jpg 40w" sizes="(max-width: 50px) 100vw, 50px" />
                            </div>

                            <div class="widget-text">
                                <a href="http://demo.djmimi.net/themes/recipe/recipe/feta-cheese-turkey-burgers/">
                                    Feta Cheese Turkey Burgers
                                </a>
                                <p class="grey"><i class="fa fa-heart-o tip" data-title="Favourites"></i>14</p>
                            </div>
                            <div class="clearfix"></div>
                        </li></ul></div>				</div>
        </div>
    </div>
</section>


@include('front.footer')
