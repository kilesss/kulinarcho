<section class="navigation-bar white-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="navigation">
                    <button class="navbar-toggle button-white menu" data-toggle="collapse"
                            data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-3x"></i>
                    </button>
                    <button class="navbar-toggle button-white menu small-search-toggle" data-toggle="collapse"
                            data-target=".search-collapse">
                        <i class="fa fa-search fa-3x"></i>
                    </button>


                </div>
            </div>
        </div>
    </div>
</section>
