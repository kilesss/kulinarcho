<section class="copyrights">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p>All rights reserved © Designed by KileSoft</p>
            </div>
            <div class="col-md-4">
                <p class="text-right">
                    <a href="#" class="copyrights-share" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="#" class="copyrights-share" target="_blank">
                        <i class="fa fa-twitter"></i>
                    </a>

                    <a href="#" class="copyrights-share" target="_blank">
                        <i class="fa fa-google-plus"></i>
                    </a>

                    <a href="#" class="copyrights-share" target="_blank">
                        <i class="fa fa-linkedin"></i>
                    </a>

                    <a href="#" class="copyrights-share" target="_blank">
                        <i class="fa fa-tumblr"></i>
                    </a>


                    <a href="#" class="copyrights-share" target="_blank">
                        <i class="fa fa-pinterest"></i>
                    </a>


                    <a href="#" class="copyrights-share" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>

                </p>
            </div>
        </div>
    </div>
</section>
</body>
</html>
