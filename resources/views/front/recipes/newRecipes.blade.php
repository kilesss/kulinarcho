@include('front.header')

@include('front.menu')
<section>
    <div class="container">
        <div class="row">
            @include('front.profile.addNew')
        </div>
    </div>
</section>
@include('front.footer')

@include('front.recipes.js.javascripts')

