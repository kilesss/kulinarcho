<div class="col-sm-9 sectionProfile" style="display: none;" id="sectionDiet">
    <div class="white-block">
        <div class="content-inner">
            <h4 class="no-top-margin">@lang('front.editProfile.title')</h4>
            <p>@lang('front.editProfile.info')</p>
            <hr>

        </div>
    </div>
</div>