<div class="col-sm-9 sectionProfile" style="display: none;" id="sectionDashboard">
    <div class="white-block">
        <div class="content-inner">
            <h4 class="no-top-margin">@lang('front.profileDashboardmenu.myrecipes')</h4>
            <p>@lang('front.profileDashboard.hi') {{$username}}. @lang('front.profileDashboard.greeting').</p>
            <hr>
            <div class="row">
                <div class="col-sm-6">
                    <div class="dashboard-item clearfix">
                        <div class="pull-left">
                            <i class="fa fa-book"></i>
                            @lang('front.profileDashboard.publishedRecipes'):
                        </div>

                        <div class="pull-right badge">
                            0
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="dashboard-item clearfix">
                        <div class="pull-left">
                            <i class="fa fa-repeat"></i>
                            @lang('front.profileDashboard.waitingUpdateRecipes') :
                        </div>

                        <div class="pull-right badge">
                            2
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="dashboard-item clearfix">
                        <div class="pull-left">
                            <i class="fa fa-reply"></i>
                            @lang('front.profileDashboard.waithingApprovalRecipes'):
                        </div>

                        <div class="pull-right badge">
                            2
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="dashboard-item clearfix">
                        <div class="pull-left">
                            <i class="fa fa-heart"></i>
                            @lang('front.profileDashboard.favoriteRecipes'):
                        </div>

                        <div class="pull-right badge">
                            6
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="dashboard-item clearfix">
                        <div class="pull-left">
                            <i class="fa fa-star"></i>
                            @lang('front.profileDashboard.rating')        </div>

                        <div class="pull-right">
                            <span class="bottom-ratings tip" data-title="Average Rate: 0 / 5">
                                <span class="icon icon_rate"></span>
                                <span class="icon icon_rate"></span>
                                <span class="icon icon_rate"></span>
                                <span class="icon icon_rate"></span>
                                <span class="icon icon_rate"></span>
                                <span class="top-ratings" style="width: 0%">
                                    <span class="icon icon_rate"></span>
                                    <span class="icon icon_rate"></span>
                                    <span class="icon icon_rate"></span>
                                    <span class="icon icon_rate"></span>
                                    <span class="icon icon_rate"></span>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="dashboard-item clearfix">
                        <div class="pull-left">
                            <i class="fa fa-cutlery"></i>
                            @lang('front.profileDashboard.cookingLevel')        </div>

                        <div class="pull-right badge">
                            @lang('front.profileDashboard.beginerLevel')            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>