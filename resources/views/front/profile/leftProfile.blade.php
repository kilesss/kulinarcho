<div class="col-sm-3">
    <div class="white-block">
        <div class="my-sidebar">
            <div class="my-avatar " style="background: url( '' );">
                <img src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2017/12/maxresdefault-1-150x150.jpg"
                     class="img-responsive" alt="author">
                <h4>{{$username}}</h4>

                <ul class="list-unstyled list-inline post-share">
                </ul>
            </div>
            <ul class="list-unstyled my-menu">
                <li class="active">
                    <a href="#" id="dashboardBtn">
                        <i class="fa fa-dashboard"></i> @lang('front.profileDashboardmenu.dashboard')</a>
                </li>
                <li class="">
                    <a href="#" id="myRecipesBtn">
                        <i class="fa fa-book"></i> @lang('front.profileDashboardmenu.myrecipes')
                    </a>
                </li>
                <li class="">
                    <a href="#" id="FavoriteBtn">
                        <i class="fa fa-heart"></i> @lang('front.profileDashboardmenu.favoriteRecipes')
                    </a>
                </li>
                <li class="">
                    <a href="#" id="EditBtn">
                        <i class="fa fa-cog"></i>@lang('front.profileDashboardmenu.editProfile')
                    </a>
                </li>
                <li class="">
                    <a href="#" id="AddNewBtn">
                        <i class="fa fa-plus-circle"></i> @lang('front.profileDashboardmenu.addnewRecipes')
                    </a>
                </li>
                <li class="">
                    <a href="#" id="menuSettingsBtn">
                        <i class="fa fa-plus-circle"></i> @lang('front.profileDashboardmenu.menuSettings')
                    </a>
                </li>
                <li class="">
                    <a href="#" id="dietBtn">
                        <i class="fa fa-plus-circle"></i> @lang('front.profileDashboardmenu.diet')
                    </a>
                </li>
                <li class="">
                    <a href="#" id="vipBtn" style="background-image: url('http://www.clker.com/cliparts/B/F/c/d/g/j/gold-button-hi.png'); background-size: 100%">
                        @lang('front.profileDashboardmenu.vip')
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>