<style>
    .roundCub{
        border: 1px solid silver;
        border-radius: 15px;
        text-align: center;
        padding-top: 3%;
        padding-bottom: 3%;
        margin: 3%;
        margin-bottom: 5%;
    }
    .shadowFree{
        -webkit-box-shadow: 0px 0px 76px -3px rgba(128,128,128,1);
        -moz-box-shadow: 0px 0px 76px -3px rgba(128,128,128,1);
        box-shadow: 0px 0px 76px -3px rgba(128,128,128,1);
    }
    .shadowMonthly{
        -webkit-box-shadow: 0px 0px 76px -3px rgba(64,255,109,1);
        -moz-box-shadow: 0px 0px 76px -3px rgba(64,255,109,1);
        box-shadow: 0px 0px 76px -3px rgba(64,255,109,1);
    }
    .shadowYearly{
        -webkit-box-shadow: 0px 0px 76px -3px rgba(0,247,255,1);
        -moz-box-shadow: 0px 0px 76px -3px rgba(0,247,255,1);
        box-shadow: 0px 0px 76px -3px rgba(0,247,255,1);
    }
    .shadowForever{
        -webkit-box-shadow: 0px 0px 100px 5px rgba(241,255,51,1);
        -moz-box-shadow: 0px 0px 100px 5px rgba(241,255,51,1);
        box-shadow: 0px 0px 100px 5px rgba(241,255,51,1);
    }
</style>
<div class="col-sm-9 sectionProfile" style="display: none;" id="sectionVip">
    <div class="white-block">
        <div class="content-inner">
            <h4 class="no-top-margin">@lang('front.vip.title')</h4>
            <p>@lang('front.vip.info')</p>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3  roundCub shadowForever">
                        <div class="row"><h3>@lang('front.vip.unlimiteTitle')</h3></div>
                        <div class="row">@lang('front.vip.foreverLimit')</div>
                        <div class="row">
                            <img src="https://media.istockphoto.com/vectors/fish-and-chips-chef-vector-id940417680">
                        </div>
                        <div class="row">@lang('front.vip.foreverAdds')</div><hr>
                        <div class="row">@lang('front.vip.foreverMenuDiet')</div><hr>
                        <div class="row">@lang('front.vip.foreverMenuMenu')</div><hr>
                        <div class="row">@lang('front.vip.foreverMenuShopping')</div><hr>
                        <div class="row">@lang('front.vip.foreverDevelopment')</div><hr>
                        <div class="row">
                            <div class="row">@lang('front.vip.price') 15 @lang('front.vip.priceVal')</div>
                            <div class="row"><button class="btn btn-success">@lang('front.vip.pay')</button></div>

                        </div>
                    </div>

                    <div class="col-md-3  roundCub shadowMonthly">
                        <div class="row"><h3>@lang('front.vip.monthlyTitle')</h3></div>
                        <div class="row">@lang('front.vip.monthlyLimit')</div>
                        <div class="row">
                            <img src="https://media.istockphoto.com/vectors/woman-chef-cartoon-character-mascot-vector-id839841374">
                        </div>
                        <div class="row">@lang('front.vip.monthlyAdds')</div><hr>
                        <div class="row">@lang('front.vip.monthlyMenuDiet')</div><hr>
                        <div class="row">@lang('front.vip.monthlyMenuMenu')</div><hr>
                        <div class="row">@lang('front.vip.monthlyMenuShopping')</div><hr>
                        <div class="row">@lang('front.vip.monthlyDevelopment')</div><hr>
                        <div class="row">
                            <div class="row">@lang('front.vip.price'): 50 @lang('front.vip.priceValST')</div>
                            <div class="row"><button class="btn btn-success">@lang('front.vip.pay')</button></div>

                        </div>
                    </div>
                    <div class="col-md-3  roundCub shadowYearly">
                        <div class="row"><h3>@lang('front.vip.yearlyTitle')</h3></div>
                        <div class="row">@lang('front.vip.yearlyLimit')</div>
                        <div class="row">
                            <img src="https://media.istockphoto.com/vectors/pizza-chef-woman-cartoon-character-vector-id824642632">
                        </div>
                        <div class="row">@lang('front.vip.yearlyAdds')</div><hr>
                        <div class="row">@lang('front.vip.yearlyMenuDiet')</div><hr>
                        <div class="row">@lang('front.vip.yearlyMenuMenu')</div><hr>
                        <div class="row">@lang('front.vip.yearlyMenuShopping')</div><hr>
                        <div class="row">@lang('front.vip.yearlyDevelopment')</div><hr>
                        <div class="row">
                            <div class="row">@lang('front.vip.price'): 5 @lang('front.vip.priceVal')</div>
                            <div class="row"><button class="btn btn-success">@lang('front.vip.pay')</button></div>

                        </div>
                    </div>
                    <div class="col-md-3  roundCub shadowFree">
                        <div class="row"><h3>@lang('front.vip.freeTitle')</h3></div>
                        <div class="row">@lang('front.vip.freeLimit')</div>
                        <div class="row">
                            <img src="https://media.istockphoto.com/vectors/chef-woman-cartoon-cook-vector-id868395958">
                        </div>
                        <div class="row">@lang('front.vip.freeAdds')</div><hr>
                        <div class="row">@lang('front.vip.freeMenuDiet')</div><hr>
                        <div class="row">@lang('front.vip.freeMenuMenu')</div><hr>
                        <div class="row">@lang('front.vip.freeMenuShopping')</div><hr>
                        <div class="row">
                            <div class="row">@lang('front.vip.price'): @lang('front.vip.priceValFree')</div>
                            <div class="row"><button class="btn btn-success">@lang('front.vip.payfree')</button></div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>