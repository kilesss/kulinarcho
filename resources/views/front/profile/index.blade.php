@include('front.header')

@include('front.menu')
<section>
    <div class="container">
        <div class="row">
            @include('front.profile.leftProfile')
            @include('front.profile.dashboard')
            @include('front.profile.myrecipes')
            @include('front.profile.favorite')
            @include('front.profile.editProfile')
            @include('front.profile.addNew')
            @include('front.profile.menuSettings')
            @include('front.profile.diet')
            @include('front.profile.becomeVip')
        </div>
    </div>
</section>
@include('front.footer')

@include('front.profile.js.javascripts')

