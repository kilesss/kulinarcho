<div class="col-sm-9 sectionProfile" style="display: none;" id="sectionMyRecipes">
    <div class="white-block">
        <div class="content-inner">
            <h4 class="no-top-margin">@lang('front.myrecipes.title')</h4>
            <p>@lang('front.myrecipes.info')</p>
            <hr>
            {{--<p class="pretable-loading" style="display: none;">Loading...</p>--}}
            <div class="bt-table" style="display: block;">
                <div class="bootstrap-table">
                    <div class="fixed-table-toolbar">
                        <div class="pull-left search"><input class="form-control" type="text"
                                                             placeholder="Search for recipes..."></div>
                    </div>
                    <div class="fixed-table-container">
                        <div class="fixed-table-header">
                            <table></table>
                        </div>
                        <div class="fixed-table-body">
                            <table data-toggle="table" data-search-align="left" data-search="true"
                                   data-classes="table table-striped" class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="">
                                        <div class="th-inner ">
                                            @lang('front.myrecipes.image')
                                        </div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner sortable">
                                            @lang('front.myrecipes.name')
                                        </div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner sortable">
                                            @lang('front.myrecipes.status')
                                        </div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner ">
                                            @lang('front.myrecipes.rating')
                                        </div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner sortable">
                                            @lang('front.myrecipes.view')
                                        </div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner sortable">
                                            @lang('front.myrecipes.action')
                                        </div>
                                        <div class="fht-cell"></div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-index="0">
                                    <td style="">
                                        <img width="150" height="150"
                                             src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2018/05/utensilios-de-cozinha-845x330-150x150.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2018/05/utensilios-de-cozinha-845x330-150x150.jpg 150w,
                                              http://demo.djmimi.net/themes/recipe/wp-content/uploads/2018/05/utensilios-de-cozinha-845x330-50x50.jpg 50w,
                                              http://demo.djmimi.net/themes/recipe/wp-content/uploads/2018/05/utensilios-de-cozinha-845x330-25x25.jpg 25w,
                                               http://demo.djmimi.net/themes/recipe/wp-content/uploads/2018/05/utensilios-de-cozinha-845x330-40x40.jpg 40w"
                                             sizes="(max-width: 150px) 100vw, 150px"></td>
                                    <td style="">
                                        <a href="http://demo.djmimi.net/themes/recipe/?post_type=recipe&amp;p=1434"
                                           target="_blank">
                                            Teste </a>
                                    </td>
                                    <td style="">
                                        @lang('front.myrecipes.statusactive')
                                    </td>
                                    <td style="">
                                      ****</td>
                                    <td style="">
                                        3
                                    </td>
                                    <td class="action" style="">
                                        <a href="http://demo.djmimi.net/themes/recipe/my-account/?page=edit_recipe&amp;recipe_id=1434">
                                            @lang('front.myrecipes.editBtn')
                                        </a>
                                        <a href="javascript:;" class="remove-recipe" data-recipe_id="1434">
                                            @lang('front.myrecipes.deleteBtn')
                                        </a>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="fixed-table-pagination" style="display: none;"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>