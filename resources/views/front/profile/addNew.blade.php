<div class="col-sm-9 sectionProfile" style="display: none;" id="sectionAddNew">
    <div class="white-block">
        <div class="content-inner">
            <h4 class="no-top-margin">Submit New Recipe</h4>
            <p>Populate fields below and after review you will receive response</p>
            <hr>
            <div class="form-group">
                <label for="title">Title *</label>
                <input type="text" name="title" id="title" value="" class="form-control">
            </div>


            <div class="form-group">
                <label for="recipe_cuisine">produkts *</label>
                <textarea  rows="10" class="col-md-12" name="description"
                          id="recipe_cuisine" aria-hidden="true" ></textarea>
            </div>

            <div class="container">
                <div class="row">
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker3'>
                                <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker3').datetimepicker({
                                format: 'LT'
                            });
                        });
                    </script>
                </div>
            </div>
           <div class="form-group">
                   <label for="coocking_time">cooking time*</label>
                   <input type="text" class="form-control" id="coocking_time" value="0">
           </div>

            <div class="form-group">
                <label for="recipe_cuisine">portions *</label>
                <select name="recipe_cuisine" id="recipe_cuisine" class="form-control">
                    <option value="">- Select -</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </div>
            <div class="form-group">
                <label for="recipe_cuisine">gotvene *</label>
                <textarea  rows="10" class="col-md-12" name="description"
                           id="recipe_cuisine" aria-hidden="true" ></textarea>
            </div>
            <div class="form-group">
                <label for="submitBTN"></label>

                    <button id="submitBTN" class="btn btn-success">Zapazi</button>
            </div>
        </div>
    </div>
</div>
