
<script>
    $( document ).ready(function() {
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        // $("#sectionDashboard").show();
        $("#sectionAddNew").show();

//        $('#coocking_time').durationPicker({
//            onChanged: function (newVal) {
//                $('#duration-label').text(newVal);
//            }
//        });
    });
    $("#dashboardBtn").click(function(){
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        $("#sectionDashboard").show();
    });
    $("#myRecipesBtn").click(function(){
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        $("#sectionMyRecipes").show();
    });
    $("#FavoriteBtn").click(function(){
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        $("#sectionFavorite").show();
    });
    $("#EditBtn").click(function(){
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        $("#sectionEditProfile").show();
    });
    $("#AddNewBtn").click(function(){
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        $("#sectionAddNew").show();
    });



    $("#menuSettingsBtn").click(function(){
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        $("#sectionMenuSettings").show();
    });
    $("#dietBtn").click(function(){
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        $("#sectionDiet").show();
    });
    $("#vipBtn").click(function(){
        $(".list-unstyled>li.active").removeClass("active");
        $(this).closest("li").addClass('active');

        $(".sectionProfile").hide();
        $("#sectionVip").show();
    });


</script>
{{--TIME PICKER--}}
