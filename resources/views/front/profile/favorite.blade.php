<div class="col-sm-9 sectionProfile" style="display: none;" id="sectionFavorite">
    <div class="white-block">
        <div class="content-inner">
            <h4 class="no-top-margin">My favourite recipes</h4>
            <p>Here you will find all your favourite recipes</p>
            <hr>
            <p class="pretable-loading" style="display: none;">Loading...</p>
            <div class="bt-table" style="display: block;">
                <div class="bootstrap-table"><div class="fixed-table-toolbar"><div class="pull-left search"><input class="form-control" type="text" placeholder="Search for recipes..."></div></div><div class="fixed-table-container"><div class="fixed-table-header"><table></table></div><div class="fixed-table-body"><div class="fixed-table-loading" style="top: 37px;">Loading, please wait...</div><table data-toggle="table" data-search-align="left" data-search="true" data-classes="table table-striped" class="table table-striped">
                                <thead>
                                <tr><th style=""><div class="th-inner ">
                                            Image		        </div><div class="fht-cell"></div></th><th style=""><div class="th-inner sortable">
                                            Name		        </div><div class="fht-cell"></div></th><th style=""><div class="th-inner sortable">
                                            Status		        </div><div class="fht-cell"></div></th><th style=""><div class="th-inner ">
                                            Ratings		        </div><div class="fht-cell"></div></th><th style=""><div class="th-inner sortable">
                                            Views		        </div><div class="fht-cell"></div></th><th style=""><div class="th-inner sortable">
                                            Action		        </div><div class="fht-cell"></div></th></tr>
                                </thead>
                                <tbody><tr data-index="0"><td style="">
                                        <img width="150" height="150" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/salad-468902_1920-40x40.jpg 40w" sizes="(max-width: 150px) 100vw, 150px">					</td><td style="">
                                        <a href="http://demo.djmimi.net/themes/recipe/recipe/avocado-panzanella-salad/" target="_blank">
                                            Avocado panzanella salad						</a>
                                    </td><td style="">
                                        Published					</td><td style="">
                                        <div class="td-ratings">
                                            <span class="bottom-ratings tip" data-title="Average Rate: 3.88 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 77.6%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>						</div>
                                    </td><td style="">
                                        2255					</td><td class="action" style="">
                                        <a href="javascript:;" class="recipe-favourite" data-recipe_id="621">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td></tr><tr data-index="1"><td style="">
                                        <img width="150" height="150" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/cake-219595_1920-40x40.jpg 40w" sizes="(max-width: 150px) 100vw, 150px">					</td><td style="">
                                        <a href="http://demo.djmimi.net/themes/recipe/recipe/brazilian-black-bean-stew/" target="_blank">
                                            Brazilian Black Bean Stew						</a>
                                    </td><td style="">
                                        Published					</td><td style="">
                                        <div class="td-ratings">
                                            <span class="bottom-ratings tip" data-title="Average Rate: 3.67 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 73.4%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>						</div>
                                    </td><td style="">
                                        1421					</td><td class="action" style="">
                                        <a href="javascript:;" class="recipe-favourite" data-recipe_id="631">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td></tr><tr data-index="2"><td style="">
                                        <img width="150" height="150" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/hamburger-494706_1920-40x40.jpg 40w" sizes="(max-width: 150px) 100vw, 150px">					</td><td style="">
                                        <a href="http://demo.djmimi.net/themes/recipe/recipe/feta-cheese-turkey-burgers/" target="_blank">
                                            Feta Cheese Turkey Burgers						</a>
                                    </td><td style="">
                                        Published					</td><td style="">
                                        <div class="td-ratings">
                                            <span class="bottom-ratings tip" data-title="Average Rate: 4.14 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 82.8%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>						</div>
                                    </td><td style="">
                                        1835					</td><td class="action" style="">
                                        <a href="javascript:;" class="recipe-favourite" data-recipe_id="627">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td></tr><tr data-index="3"><td style="">
                                        <img width="150" height="150" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/background-2561_1920-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/background-2561_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/background-2561_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/background-2561_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/background-2561_1920-40x40.jpg 40w" sizes="(max-width: 150px) 100vw, 150px">					</td><td style="">
                                        <a href="http://demo.djmimi.net/themes/recipe/recipe/decadent-challah-bread/" target="_blank">
                                            Decadent Challah Bread						</a>
                                    </td><td style="">
                                        Published					</td><td style="">
                                        <div class="td-ratings">
                                            <span class="bottom-ratings tip" data-title="Average Rate: 4 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 80%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>						</div>
                                    </td><td style="">
                                        562					</td><td class="action" style="">
                                        <a href="javascript:;" class="recipe-favourite" data-recipe_id="617">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td></tr><tr data-index="4"><td style="">
                                        <img width="150" height="150" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-40x40.jpg 40w" sizes="(max-width: 150px) 100vw, 150px">					</td><td style="">
                                        <a href="http://demo.djmimi.net/themes/recipe/recipe/sparkling-mint-juleps/" target="_blank">
                                            Sparkling mint juleps						</a>
                                    </td><td style="">
                                        Published					</td><td style="">
                                        <div class="td-ratings">
                                            <span class="bottom-ratings tip" data-title="Average Rate: 3 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 60%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>						</div>
                                    </td><td style="">
                                        192					</td><td class="action" style="">
                                        <a href="javascript:;" class="recipe-favourite" data-recipe_id="604">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td></tr><tr data-index="5"><td style="">
                                        <img width="150" height="150" src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/celebration-315079-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/celebration-315079-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/celebration-315079-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/celebration-315079-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/celebration-315079-40x40.jpg 40w" sizes="(max-width: 150px) 100vw, 150px">					</td><td style="">
                                        <a href="http://demo.djmimi.net/themes/recipe/recipe/maple-glazed-turkey/" target="_blank">
                                            Maple-glazed turkey						</a>
                                    </td><td style="">
                                        Published					</td><td style="">
                                        <div class="td-ratings">
                                            <span class="bottom-ratings tip" data-title="Average Rate: 4 / 5"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="top-ratings" style="width: 80%"><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span><span class="icon icon_rate"></span></span></span>						</div>
                                    </td><td style="">
                                        189					</td><td class="action" style="">
                                        <a href="javascript:;" class="recipe-favourite" data-recipe_id="601">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td></tr></tbody>
                            </table></div><div class="fixed-table-pagination" style="display: none;"></div></div></div><div class="clearfix"></div>
            </div>					</div>
    </div>
</div>