<div class="col-sm-9 sectionProfile" style="display: none;" id="sectionEditProfile">
    <div class="white-block">
        <div class="content-inner">
            <h4 class="no-top-margin">@lang('front.editProfile.title')</h4>
            <p>@lang('front.editProfile.info')</p>
            <hr>
            <form method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="avatar">@lang('front.editProfile.avatar') </label>
                            <input type="hidden" name="wp-user-avatar" id="avatar" value="">
                            <a href="javascript:;" class="image-upload user-avatar">@lang('front.editProfile.changeImage')</a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="email">@lang('front.editProfile.email') *</label>
                            <input type="text" name="email" id="email" value="demo@dmail.com" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="first_name">@lang('front.editProfile.firstname')</label>
                            <input type="text" name="first_name" id="first_name" value="Demo" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="last_name">@lang('front.editProfile.lastname')</label>
                            <input type="text" name="last_name" id="last_name" value="Demo" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="password">@lang('front.editProfile.password')</label>
                            <input type="password" name="password" id="password" value="" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="repeat_password">@lang('front.editProfile.repeatpassword')</label>
                            <input type="password" name="repeat_password" id="repeat_password" value=""
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="facebook">@lang('front.editProfile.facebookLink')</label>
                            <input type="text" name="facebook" id="facebook" value="" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="twitter">@lang('front.editProfile.twiterLink')</label>
                            <input type="text" name="twitter" id="twitter" value="" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="google">@lang('front.editProfile.goolglelink')</label>
                            <input type="text" name="google" id="google" value="" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="instagram">@lang('front.editProfile.instagramLink')</label>
                            <input type="text" name="instagram" id="instagram" value="" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">@lang('front.editProfile.aboutYou')</label>



                                    <textarea class="wp-editor-area" rows="10" autocomplete="off" cols="100"
                                              name="description" id="description"
                                              aria-hidden="true"></textarea>

                        </div>

                        <input type="hidden" value="update_profile" name="action">
                        <input type="hidden" value="367" name="user_id">
                        <input type="hidden" id="profile_field" name="profile_field" value="7e6445c062"><input
                                type="hidden" name="_wp_http_referer"
                                value="/themes/recipe/my-account/?page=edit_profile">
                        <div class="send_result"></div>

                        <a href="javascript:;" class="btn submit-form">
                            @lang('front.editProfile.updateBtn')</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>