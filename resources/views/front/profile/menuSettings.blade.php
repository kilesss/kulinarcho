<style>
    #demo-content {
        padding: 0px 0px 100px 0px;
    }

    #demo-content table.tutorial-table {
        table-layout: auto;
    }

    .tutorial-back a {
        background: #09f;
        padding: 10px 10px 10px 20px;
        margin: 20px 40px 20px 0;
        color: #fff;
        display: inline-block;
        width: 155px;
        text-align: center;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
        position: relative;
    }

    .tutorial-back a:before {
        background: url(https://phppot.com/wp-content/themes/solandra/images/sprite.png) no-repeat 0px 0px;
        height: 20px;
        width: 20px;
        display: inline-flex;
        content: '';
        position: absolute;
        top: 10px;
        left: 15px;
    }

    .tutorial-back a:hover {
        box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.50);
    }

    .fieldAddProducts {
        margin-right: 2%;
        border: 2px solid #aff1af;
        border-radius: 20px;
    }

    .fieldRemoveProducts {
        margin-right: 2%;
        border: 2px solid #ffa6a6;
        border-radius: 20px;
    }

</style>
<!-- Initialize the plugin: -->

<div class="col-sm-9 sectionProfile" style="display: none;" id="sectionMenuSettings">
    <div class="white-block">
        <div class="content-inner">
            <h4 class="no-top-margin">@lang('front.menuSettings.title')</h4>

            <p>@lang('front.menuSettings.subTitle')</p>
            <hr>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="recipe_cuisine">@lang('front.menuSettings.period')</label>
                    <select name="recipe_cuisine" id="periodgeneratedRecipes" class="form-control">
                        <option value="0">- @lang('front.menuSettings.select') -</option>
                        <option value="1">@lang('front.menuSettings.week')</option>
                        <option value="2">@lang('front.menuSettings.2weeks')</option>
                        <option value="3">@lang('front.menuSettings.3weeks')</option>
                        <option value="4">@lang('front.menuSettings.month')</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="breakfastCheckbox">
                        <label class="form-check-label" for="breakfastCheckbox">@lang('front.menuSettings.breakfast')</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="lunchCheckbox">
                        <label class="form-check-label" for="lunchCheckbox">@lang('front.menuSettings.lunch')</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="dinnerCheckbox">
                        <label class="form-check-label" for="dinnerCheckbox">@lang('front.menuSettings.dinner')</label>
                    </div>
                </div>

                <div class="col-md-12">
                    <button class="btn btn-success" id="generateMenuRecipes">@lang('front.menuSettings.generate')
                    </button>
                </div>
            </div>

        </div>
    </div>
    <div class="row col-md-12" id="fieldMenuSettings">
    </div>
</div>
<div id="selectProductsModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modalSelectProductTitle"></h4>
                <i id="modalSelectProductTitleDate"></i>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        @foreach ($settingsMenu['products']['categories'] as $cat)

                            <div class="col-md-6">
                                <div class="col-md-12 categories_products_row"
                                     id="categories_products_row_{{$cat->id}}">
                                    <button class="btn btn-default col-md-6 categorySelectButton"
                                            id="category_{{$cat->id}}"
                                            onclick="selectedCategory({{$cat->id}})"> {{ $cat->name }}</button>
                                    <button class="btn btn-default col-md-6"
                                            onclick="showProductsCategory({{$cat->id}})">@lang('front.menuSettings.productsInCategory')
                                    </button>
                                    <div class="col-md-12">
                                        <div class="form-group col-md-12 categories_products_section"
                                             style="display: none"
                                             id="categories_products__section_{{$cat->id}}">
                                            @foreach ($settingsMenu['products']['products'] as $prod)
                                                @if ($prod->foodCategoryId == $cat->id )
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input storeproductCheckbox"
                                                               type="checkbox"
                                                               id="categories_products_{{$prod->id}}"
                                                               value="{{$prod->id}}"
                                                               onclick="storeProducts({{$prod->id}}, {{$cat->id}})"
                                                        >
                                                        <label class="form-check-label"
                                                               id="categories_products_label_{{$prod->id}}"
                                                               for="categories_products_{{$prod->id}}">{{$prod->name}}</label>
                                                    </div>

                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="finnishSelectedProducts" data-dismiss="modal">@lang('front.menuSettings.save')
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('front.menuSettings.cancel')</button>
            </div>
        </div>

    </div>
</div>


<script>


    $('#generateMenuRecipes').click(function () {
        var weeks = $("#periodgeneratedRecipes option:selected").val();
        if (weeks > 0 && weeks < 5) {
            var breakfast = $('#breakfastCheckbox:checkbox:checked').length > 0;
            var lunch = $('#lunchCheckbox:checkbox:checked').length > 0;
            var dinner = $('#dinnerCheckbox:checkbox:checked').length > 0;
            if (breakfast != false || lunch != false || dinner != false) {
                generateMenu(weeks, breakfast, lunch, dinner);
            } else {
                alert('lipsvashti menuta');

            }

        } else {
            alert('greshna sedmica');
        }
    });
var weeksG = '';
    function generateMenu(weeks, breakfast, lunch, dinner) {
        var days = weeks * 7;
        weeksG = weeks;
        var generatedHtml = '<div class="col-md-12 white-block"  style="margin-top: 1%;" >\n' +
                '            <div class="content-inner">\n' +
                '<div class="form-group">' +
                '<label for="recipe_servings">{{Lang::get("front.menuSettings.titleMenuSettings")}}</label>' +
                '<input type="text" name="recipe_servings" id="title_recipe_settings" value="" class="form-control">' +
                '</div>' +
                '</div>' +
                '</div>';

        $('#fieldMenuSettings').empty();
        var firstDate = '';
        var lastDate = '';
        var titleTypeMeals = '';
        if (breakfast === true) {
            titleTypeMeals = titleTypeMeals + "{{Lang::get("front.menuSettings.breakfast")}} ";
        }
        if (lunch === true) {
            titleTypeMeals = titleTypeMeals + "{{Lang::get("front.menuSettings.lunch")}} ";
        }
        if (dinner == true) {
            titleTypeMeals = titleTypeMeals + "{{Lang::get("front.menuSettings.dinner")}}";
        }
        for (i = 0; i < days; i++) {
            var r = i + 1;
            var hours = r * 24;
            var currentDate = new Date(new Date().getTime() + hours * 60 * 60 * 1000);
            var day = currentDate.getDate();
            var month = currentDate.getMonth() + 1;
            var year = currentDate.getFullYear();
            var breakfastHtml = '';
            var lunchHtml = '';
            var dinerHtml = '';
            var dateMenu = day + '/' + month + '/' + year;
            if (i == 0) {
                firstDate = dateMenu;
            }
            if (i == days-1) {
                lastDate = dateMenu;
            }

            if (breakfast === true) {
                breakfastHtml = '<div class="row">\n' +
                        '                    <p class="row col-md-12">{{Lang::get("front.menuSettings.breakfast")}}</p>\n' +
                        '                    <div class="row col-md-12">\n' +
                        '                        <div class="row col-md-6 fieldAddProducts" >\n' +
                        '                            <p class="row col-md-12">{{Lang::get("front.menuSettings.productsIdRecipe")}}</p>\n' +
                        '                            <div class="row col-md-12">\n' +
                        '                            <button type="button" class="col-md-6 btn btn-default" onclick="openModalSelectProducts(\'' + dateMenu + '\',\'b\',\'1\')">\n' +
                        '  {{Lang::get("front.menuSettings.selecr")}}\n' +
                        '</button>' + '                            </div>\n' +
                        '                            <div class="row col-md-12">\n' +
                        '                                <i>Ориз</i>\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                        <div class="row col-md-6 fieldRemoveProducts">\n' +
                        '                            <p class="row col-md-12">{{Lang::get("front.menuSettings.productsIsntRecipe")}}</p>\n' +
                        '                            <div class="row col-md-12">\n' +
                        '                            <button type="button" class="col-md-6 btn btn-default" onclick="openModalSelectProducts(\'' + dateMenu + '\',\'b\',0 )"> \n' +
                        '  {{Lang::get("front.menuSettings.select")}}\n' +
                        '</button>' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                    </div>\n' +
                        '                </div>';
            }
            if (lunch === true) {
                lunchHtml = '<div class="row">\n' +
                        '                    <p class="row col-md-12">{{Lang::get("front.menuSettings.lunch")}}</p>\n' +
                        '                    <div class="row col-md-12">\n' +
                        '                        <div class="row col-md-6 fieldAddProducts" >\n' +
                        '                            <p class="row col-md-12">{{Lang::get("front.menuSettings.productsIdRecipe")}}</p>\n' +
                        '                            <div class="row col-md-12">\n' +
                        '                            <button type="button" class="col-md-6 btn btn-default" onclick="openModalSelectProducts(\'' + dateMenu + '\',\'l\',1)">\n' +
                        '  {{Lang::get("front.menuSettings.select")}}\n' +
                        '</button>' +
                        '                            </div>\n' +
                        '                            <div class="row col-md-12">\n' +
                        '                                <i>Ориз</i>\n' +
                        '                                <i>Картофи</i>\n' +
                        '                                <i>Пилешко</i>\n' +
                        '                                <i>Макарони</i>\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                        <div class="row col-md-6 fieldRemoveProducts" >\n' +
                        '                            <p class="row col-md-12">{{Lang::get("front.menuSettings.productsIsntRecipe")}}</p>\n' +
                        '                            <div class="row col-md-12">\n' +
                        '                            <button type="button" class="col-md-6 btn btn-default" onclick="openModalSelectProducts(\'' + dateMenu + '\',\'l\',0)">\n' +
                        '  {{Lang::get("front.menuSettings.select")}}\n' +
                        '</button>' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                    </div>\n' +
                        '                </div>'
            }
            if (dinner == true) {
                dinerHtml = '<div class="row">\n' +
                        '                    <p class="row col-md-12">{{Lang::get("front.menuSettings.dinner")}}</p>\n' +
                        '                    <div class="row col-md-12">\n' +
                        '                        <div class="row col-md-6 fieldAddProducts" >\n' +
                        '                            <p class="row col-md-12">{{Lang::get("front.menuSettings.productsIdRecipe")}}</p>\n' +
                        '                            <div class="row col-md-12">\n' +
                        '                            <button type="button" class="col-md-6 btn btn-default" onclick="openModalSelectProducts(\'' + dateMenu + '\',\'d\',1)">\n' +
                        '  Избери\n' +
                        '</button>' + '               ' +
                        '             </div>\n' +
                        '             </div>\n' +

                        '                        <div class="row col-md-6 fieldRemoveProducts" >\n' +
                        '                            <p class="row col-md-12">{{Lang::get("front.menuSettings.productsIsntRecipe")}}</p>\n' +
                        '                            <div class="row col-md-12">\n' +
                        '                            <button type="button" class="col-md-6 btn btn-default" onclick="openModalSelectProducts(\'' + dateMenu + '\',\'d\',0)">\n' +
                        '  {{Lang::get("front.menuSettings.select")}}\n' +
                        '</button>' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '                    </div>\n' +
                        '                </div>'
            }

            generatedHtml =
                    generatedHtml +

                    '<div class="col-md-12 white-block"  style="margin-top: 1%;" >\n' +
                    '            <div class="content-inner">\n' +
                    '                <h4 class="no-top-margin"> ' + day + '/' + month + '/' + year + '</h4>' +
                    breakfastHtml + lunchHtml + dinerHtml +
                    '</div>' +
                    '</div>';
        }
        generatedHtml = generatedHtml +'<div class="row col-md-12 " style="margin-right:  0px; padding-right:  0px;">'+
        '<button class="btn btn-success col-md-4" style="margin-top: 2%; float: right; margin-right: 0px; padding-right:  0px;" onclick="submitMenuSettings()">{{Lang::get("front.menuSettings.save")}}</button>'+
        '</div>'
        $('#fieldMenuSettings').append(generatedHtml);
        var title = firstDate + " - " + lastDate + ' ' + titleTypeMeals;
        $("#title_recipe_settings").val(title);

    }
    var currentDay = '';
    var typeChoiceG = '';
    var typeFoodG = '';
    function openModalSelectProducts(dateSelect, typeFood, typeChoice) {
        clearModal();

        typeFoodG = typeFood;
        $(".categorySelectButton").attr('style', 'background-color: #fff!important');
        $(".storeproductCheckbox").prop('checked', false);
        $(".categories_products_row").show();

        typeChoiceG = typeChoice;
        currentDay = dateSelect;
        $('#selectProductsModal').modal('show');

        var title = '';
        if (typeFood == 'b') {
            title = 'Закуска';
        } else if (typeFood == 'l') {
            title = 'Обяд';
        } else if (typeFood == 'd') {
            title = 'Вечеря';
        }
        $("#modalSelectProductTitle").text(title);
        $("#modalSelectProductTitleDate").text(dateSelect);
        if (typeChoice == 1) {
            $(".modal-content").css('box-shadow', '0 0 37px rgb(106, 247, 33)');

        } else if (typeChoice == 0) {
            $('.modal-content').css('box-shadow', 'rgb(247, 63, 33) 0px 0px 37px');
        }
        checkAlreadySelectedProducts();
    }
    function checkAlreadySelectedProducts() {
        var testNegative = [];
        if (typeChoiceG == 1) {
            $.each(objectSelectedProducts[currentDay][typeFoodG].typePositive.products, function (index, value) {
                $("#categories_products_" + value).prop('checked', true);

                $("#category_" + value).attr('style', 'background-color: #9cfd0847!important');
            });
            $.each(objectSelectedProducts[currentDay][typeFoodG].typeNegative.products, function (index, value) {
                $("#categories_products_label_" + value).attr('style', 'color: #fd0808!important');
//                selectedProductsArray.push(value);
            });
            $.each(objectSelectedProducts[currentDay][typeFoodG].typePositive.products, function (index, value) {
                $("#categories_products_label_" + value).attr('style', 'color: #44fd08!important');
                selectedProductsArray.push(value);
            });

            $.each(objectSelectedProducts[currentDay][typeFoodG].typePositive.categories, function (index, value) {
                $("#category_" + value).attr('style', 'background-color: #9cfd0847!important');
            });
            $.each(objectSelectedProducts[currentDay][typeFoodG].typeNegative.categories, function (index, value) {
                if ($("#categories_products_" + value)
                                .find("input[type='checkbox']").is(':checked') || $.inArray(value, testNegative) < 0) {
                    $("#category_" + value).attr('style', 'background-color: #fd04047d!important');
                } else {
                    $("#categories_products_row_" + value).hide()
                }

            });


        }
        if (typeChoiceG == 0) {
            $.each(objectSelectedProducts[currentDay][typeFoodG].typeNegative.products, function (index, value) {
                $("#categories_products_" + value).prop('checked', true);

                $("#category_" + value).attr('style', 'background-color: #9cfd0847!important');
            });
            $.each(objectSelectedProducts[currentDay][typeFoodG].typePositive.products, function (index, value) {
                $("#categories_products_label_" + value).attr('style', 'color: #44fd08!important');
//                selectedProductsArray.push(value);
            });
            $.each(objectSelectedProducts[currentDay][typeFoodG].typeNegative.products, function (index, value) {
                $("#categories_products_label_" + value).attr('style', 'color: #fd0808!important');
                selectedProductsArray.push(value);
            });

            $.each(objectSelectedProducts[currentDay][typeFoodG].typeNegative.categories, function (index, value) {
                $("#category_" + value).attr('style', 'background-color: #9cfd0847!important');
            });
            $.each(objectSelectedProducts[currentDay][typeFoodG].typePositive.categories, function (index, value) {
                if ($("#categories_products_" + value)
                                .find("input[type='checkbox']").is(':checked') || $.inArray(value, testNegative) < 0) {
                    $("#category_" + value).attr('style', 'background-color: #28d608!important');
                } else {
                    $("#categories_products_row_" + value).hide()
                }

            });

        }
    }
    function showProductsCategory(id) {

        if ($.inArray(id, selectedGroupArray) < 0) {
            $(".categories_products_section").hide();
            $("#categories_products__section_" + id).show();

        }
    }
    function clearModal() {
        $(".categorySelectButton").attr('style', 'background-color: #fff!important');
        $(".storeproductCheckbox").prop('checked', false);
        currentDay = '';
        typeChoiceG = '';
        selectedProductsArray = [];
        selectedGroupArray = [];
        $(".categories_products_section").hide();
    }
    var selectedGroupArray = [];
    function selectedCategory(id) {
        if ($.inArray(id, selectedGroupArray) > -1) {
            $("#category_" + id).attr('style', 'background-color: #fff!important');
            selectedGroupArray = jQuery.grep(selectedGroupArray, function (value) {
                return value != id;
            });
        } else {
            selectedGroupArray.push(id);
            $("#category_" + id).attr('style', 'background-color: #9cfd0847!important');

        }
    }
    var selectedProductsArray = [];
    function storeProducts(id, catid) {
        if ($.inArray(id, selectedProductsArray) > -1) {
            selectedProductsArray = jQuery.grep(selectedProductsArray, function (value) {
                return value != id;
            });
        } else {
            if ($.inArray(catid, selectedGroupArray) > -1) {
            } else {
                selectedGroupArray.push(catid);
                $("#category_" + catid).attr('style', 'background-color: #9cfd0847!important');
            }
            selectedProductsArray.push(id
            );

        }
    }
    var objectSelectedProducts = {};
    $('#finnishSelectedProducts').on('click', function () {
        if (typeof objectSelectedProducts[currentDay] !== 'undefined') {
            if (objectSelectedProducts[currentDay].day == currentDay) {
                if (typeChoiceG == 1) {
                    $.each(selectedGroupArray, function (index, value) {
                        if ($.inArray(objectSelectedProducts[currentDay][typeFoodG].typeNegative.categories, value) != -1) {
                            objectSelectedProducts[currentDay][typeFoodG].typeNegative.categories = jQuery.grep(objectSelectedProducts[currentDay][typeFoodG].typeNegative.categories, function (value2) {
                                return value2 != value;
                            });
                        }
                    });
                    $.each(selectedProductsArray, function (index, value) {
                        if ($.inArray(objectSelectedProducts[currentDay][typeFoodG].typeNegative.products, value) != -1) {
                            objectSelectedProducts[currentDay][typeFoodG].typeNegative.products = jQuery.grep(objectSelectedProducts[currentDay][typeFoodG].typeNegative.products, function (value2) {
                                return value2 != value;
                            });
                        }
                    });

                    objectSelectedProducts[currentDay][typeFoodG].typePositive.categories = selectedGroupArray;
                    objectSelectedProducts[currentDay][typeFoodG].typePositive.products = selectedProductsArray;
                    selectedProductsArray = [];
                    selectedGroupArray = [];
                } else {
                    $.each(selectedGroupArray, function (index, value) {
                        if ($.inArray(objectSelectedProducts[currentDay][typeFoodG].typePositive.categories, value) != -1) {
                            objectSelectedProducts[currentDay][typeFoodG].typePositive.categories = jQuery.grep(objectSelectedProducts[currentDay][typeFoodG].typePositive.categories, function (value2) {
                                return value2 != value;
                            });
                        }
                    });
                    $.each(selectedProductsArray, function (index, value) {

                        if ($.inArray(objectSelectedProducts[currentDay][typeFoodG].typePositive.products, value) != -1) {
                            objectSelectedProducts[currentDay][typeFoodG].typePositive.products = jQuery.grep(objectSelectedProducts[currentDay][typeFoodG].typeNegative.products, function (value2) {
                                return value2 != value;
                            });
                        }
                    });

                    objectSelectedProducts[currentDay][typeFoodG].typeNegative.categories = selectedGroupArray;
                    objectSelectedProducts[currentDay][typeFoodG].typeNegative.products = selectedProductsArray;
                    selectedProductsArray = [];
                    selectedGroupArray = [];

                }
            }
        }
        else {
            objectSelectedProducts[currentDay] = {
                day: currentDay,
                b: {
                    typePositive: {
                        categories: {},
                        products: {}
                    },
                    typeNegative: {
                        categories: {},
                        products: {}
                    }
                },
                l: {
                    typePositive: {
                        categories: {},
                        products: {}
                    },
                    typeNegative: {
                        categories: {},
                        products: {}
                    }
                },
                d: {
                    typePositive: {
                        categories: {},
                        products: {}
                    },
                    typeNegative: {
                        categories: {},
                        products: {}
                    }
                }
            };
            if (typeChoiceG == 1) {
                objectSelectedProducts[currentDay][typeFoodG].typePositive.categories = selectedGroupArray;
                objectSelectedProducts[currentDay][typeFoodG].typePositive.products = selectedProductsArray;
            } else {
                objectSelectedProducts[currentDay][typeFoodG].typeNegative.categories = selectedGroupArray;
                objectSelectedProducts[currentDay][typeFoodG].typeNegative.products = selectedProductsArray;
            }
        }
    })
    ;


    $('.storeproductCheckbox').change(function () {
        if (this.checked) {
            if (typeChoiceG == 1) {
                $("#categories_products_label_" + $(this).val()).attr('style', 'color: #44fd08!important');
            } else if (typeChoiceG == 0) {
                $("#categories_products_label_" + $(this).val()).attr('style', 'color: #fd0808!important');

            }
        } else {
            $("#categories_products_label_" + $(this).val()).attr('style', 'color: #000!important');

        }
    });
    function submitMenuSettings(){
                $.ajax({
                        method: 'POST', // Type of response and matches what we said in the route
                        url: '', // This is the url we gave in the route
                        data: {'ajax': 'ajax', 'type': 'productsMenuSElect',
                          params: objectSelectedProducts,
                            title: $("#title_recipe_settings").val(),
                            weeks: weeksG
                        },
                    // a JSON object to send back
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) { // What to do if we succeed
                            var errors = checkErrors(response);
                            if (errors == 1) {
                                if (response != 0) {
                                    $("#cutsId_" + id).remove();
                                }
                            } else {
                                alert(errors);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                        }
                    });
            clearModal();
        $("#fieldMenuSettings").empty();

    }

</script>