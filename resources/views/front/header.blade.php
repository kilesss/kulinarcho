<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon"
          href="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/favicon.png">


    <title>Kulinarcho</title>
    <!-- Speed of this site is optimised by WP Performance Score Booster plugin v1.9 - https://dipakgajjar.com/wp-performance-score-booster/ -->
    {{--<link rel='dns-prefetch' href='//fonts.googleapis.com' />--}}
    {{--<link rel='dns-prefetch' href='//s.w.org' />--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link rel='stylesheet' id='recipe-bootstrap-group-css' href='{{ asset('css/bootstrap.css') }}' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='recipe-style-inline-css' href='{{ asset('css/main.css') }}' type='text/css'/>
    <link rel='stylesheet' href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css' />

    <style id='recipe-style-inline-css' type='text/css'>
        /* HEADER */
        .top-bar {
            background: #333333;
        }

        .account-action .btn {
            color: #ffffff
        }

        .account-action .btn:hover {
            background: transparent;
            color: #ffffff
        }

        /* NAVIGATION */
        .nav.navbar-nav li a {
            font-family: "Lato", sans-serif;
            font-size: 14px;
        }

        .nav.navbar-nav > li li {
            border-color: #eeeeee;
        }

        .navigation-bar {
            background: #ffffff;
        }

        .nav.navbar-nav li a {
            color: #676767;
        }

        .navbar-toggle,
        #navigation .nav.navbar-nav li.open > a,
        #navigation .nav.navbar-nav li > a:hover,
        #navigation .nav.navbar-nav li > a:focus,
        #navigation .nav.navbar-nav li > a:active,
        #navigation .nav.navbar-nav li.current > a,
        #navigation .navbar-nav li.current-menu-parent > a,
        #navigation .navbar-nav li.current-menu-ancestor > a,
        #navigation .navbar-nav > li.current-menu-item > a {
            color: #6ba72b;
            background: #ffffff;
        }

        .nav.navbar-nav li li a {
            color: #676767;
            background: #ffffff;
        }

        #navigation .nav.navbar-nav li > a.main-search:hover,
        #navigation .nav.navbar-nav li > a.main-search:focus,
        #navigation .nav.navbar-nav li > a.main-search:active {
            background: transparent;
        }

        #navigation .nav.navbar-nav li li a:hover,
        #navigation .nav.navbar-nav li li a:active,
        #navigation .nav.navbar-nav li li a:focus,
        #navigation .nav.navbar-nav li.current > a,
        #navigation .navbar-nav li li.current-menu-parent > a,
        #navigation .navbar-nav li li.current-menu-ancestor > a,
        #navigation .navbar-nav li li.current-menu-item > a {
            color: #6ba72b;
            background: #ffffff;
        }

        /* BODY */
        body[class*=" "] {
            background-color: #f5f5f5;
            background-image: url();
            font-family: "Lato", sans-serif;
            font-size: 14px;
            line-height: 24px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "Ubuntu", sans-serif;
        }

        h1 {
            font-size: 38px;
            line-height: 1.25;
        }

        h2 {
            font-size: 32px;
            line-height: 1.25;
        }

        h3 {
            font-size: 28px;
            line-height: 1.25;
        }

        h4 {
            font-size: 22px;
            line-height: 1.25;
        }

        h5 {
            font-size: 18px;
            line-height: 1.25;
        }

        h6 {
            font-size: 13px;
            line-height: 1.25;
        }

        /* MAIN COLOR */
        a, a:hover, a:focus, a:active, a:visited,
        a.grey:hover,
        .section-title i,
        .blog-title:hover h4, .blog-title:hover h5,
        .next-prev a:hover .fa,
        .blog-read a, .blog-read a:hover, .blog-read a:active, .blog-read a:visited, .blog-read a:focus, .blog-read a:visited:hover,
        .fake-thumb-holder .post-format,
        .comment-reply-link:hover,
        .category-list .icon,
        .widget .category-list li:hover .icon,
        .my-menu li.active a, .my-menu li:hover a,
        .recipe-actions li:hover, .recipe-actions li:hover a {
            color: #6ba72b;
        }

        .user-block-overlay {
            background: rgba(107, 167, 43, 0.2);
        }

        .link-overlay {
            background: rgba(107, 167, 43, 0.8);
        }

        .my-menu li.active, .my-menu li:hover,
        .table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
            background: rgba(107, 167, 43, 0.06);
        }

        .single-recipe:not(.author) .nav-tabs li.active:before {
            border-color: #6ba72b transparent;
        }

        blockquote,
        .widget-title:after,
        .next-prev a:hover .fa {
            border-color: #6ba72b;
        }

        .my-menu li.active, .my-menu li:hover {
            border-left-color: #6ba72b;
        }

        table th,
        .tagcloud a, .btn, a.btn,
        .rslides_nav,
        .form-submit #submit,
        .nav-tabs > li > a:hover,
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus,
        .panel-default > .panel-heading a:not(.collapsed),
        .category-list li:hover .icon,
        .dashboard-item .badge,
        .ingredients-list li.checked .fake-checkbox,
        .sticky-wrap,
        .category-list a:hover .icon,
        .pagination a,
        .pagination a:visited,
        .pagination a:active,
        .pagination a:focus,
        .pagination > span {
            color: #ffffff;
            background-color: #6ba72b;
        }

        @media only screen and ( min-width: 768px ) {
            .ingredients-list li:hover .fake-checkbox {
                color: #ffffff;
                background-color: #6ba72b;
            }
        }

        .tagcloud a:hover, .tagcloud a:focus, .tagcloud a:active,
        .btn:hover, .btn:focus, .btn:active,
        .form-submit #submit:hover, .form-submit #submit:focus, .form-submit #submit:active,
        .pagination a:hover,
        .pagination > span {
            color: #ffffff;
            background-color: #333333;
        }

        .blog-read a, .blog-read a:hover, .blog-read a:active, .blog-read a:visited, .blog-read a:focus, .blog-read a:visited:hover {
            background: #ffffff;
            color: #6ba72b;
        }

        .copyrights {
            background: #333333;
            color: #ffffff;
        }

        .copyrights-share, .copyrights-share:visited {
            background: #ffffff;
            color: #333333;
        }
        .btn-default{
            background: #fff!important;
            color: #292929 !important;

        }
    </style>

    {{--<link href="{{ asset('css/main.css') }}" rel="stylesheet">--}}

    <link rel='stylesheet'  href='{{ asset('css/timepicker/bootstrap-datetimepicker.min.css') }}' type='text/css'/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

    <script src="{{ asset('/js/timepicker/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

  </head>
<body class="home page-template page-template-page-tpl_home page-template-page-tpl_home-php page page-id-479">
<!-- ==================================================================================================================================
TOP BAR
======================================================================================================================================= -->

<section class="top-bar">
    <div class="container">
        <div class="flex-wrap">
            <div class="flex-left">

                <a href="http://demo.djmimi.net/themes/recipe" class="logo">
                    <img class="img-responsve"
                         src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/recipe-logo.png" alt=""
                         height="50" width="167"/>
                </a>
            </div>
            @if (isset($isLogged) && $isLogged == 1)
                <div class="flex-right">
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample" style="    color: #fff;
    background-color: #333333;
    border-color: #333333;">
                        @lang('front.header.yourKulinarcho')
                    </button>
                </div>
            @else
                <div class="flex-right">
                    <p class="account-action text-right">
                        <a href="{!! url($globalVariable['frontRoutes']['register']) !!}" class="btn">
                            <i class="fa fa-user animation"></i>
                            @lang('front.header.register')                            </a>
                        <a href="{!! url($globalVariable['frontRoutes']['login']) !!}" class="btn">
                            <i class="fa fa-sign-in animation"></i>
                            @lang('front.header.login')                        </a>
                    </p>
                </div>
            @endif

        </div>
    </div>

    <div class="collapse" id="collapseExample">
        <div class="card card-body">
            @if (isset($isLogged) && $isLogged == 1)
                <div class="row">
                    <p class="account-action text-center">
                        <i>@lang('front.header.hi'), {{$username}}</i><br>
                        <a href=" {!! url($globalVariable['frontRoutes']['profile']) !!}" class="btn">
                            <i class="fa fa-sign-in animation"></i>
                            @lang('front.header.profile')                            </a>
                        <a href="{!! url($globalVariable['frontRoutes']['newRecipes']) !!}" class="btn">
                            <i class="fa fa-user animation"></i>
                            @lang('front.header.submitRecipes')                        </a>
                        <a href="http://demo.djmimi.net/themes/recipe/login-register/" class="btn">
                            <i class="fa fa-user animation"></i>
                            @lang('front.header.logout')                            </a>
                    </p>

                </div>
            @endif
            <div class="row">
                <p class="account-action text-center">
                    <a href=" {!! url($globalVariable['frontRoutes']['recipeSearch']) !!}" class="btn">
                        <i class="fa fa-sign-in animation"></i>
                        @lang('front.routing.recipeSearch')                            </a>
                    <a href="{!! url($globalVariable['frontRoutes']['members']) !!}" class="btn">
                        <i class="fa fa-user animation"></i>
                        @lang('front.routing.members')                        </a>
                    @if (isset($isLogged) && $isLogged == 1)
                        <a href="{!! url($globalVariable['frontRoutes']['menu']) !!}" class="btn">
                            <i class="fa fa-user animation"></i>
                            @lang('front.routing.menu')                        </a>
                        <a href="{!! url($globalVariable['frontRoutes']['shoppingList']) !!}" class="btn">
                            <i class="fa fa-user animation"></i>
                            @lang('front.routing.shoppingList')                        </a>
                    @endif

                </p>

            </div>

        </div>
    </div>
</section>
