<script>
    $(".page-link").click(function(){
        $("#frontNewsForm").closest('form').find("input[type=text], textarea").val("");
        $("#paginator-page").val($(this).text());
        $("#frontNewsForm").submit();
    })

    function deactivateNews(id){
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax':'ajax','type':'deactivateNews','id' : id, 'deactivate': 1}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response){ // What to do if we succeed
                if(response == 1){
                    $("#btn_"+id).html('<button class="btn btn-danger" onclick="activateNews('+id+')">@lang('admin.news.activate')</button>');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }
    function activateNews(id){
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax':'ajax','type':'activateNews','id' : id, 'activate': 1}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response){ // What to do if we succeed
                if(response == 1){
                    $("#btn_"+id).html('<button class="btn btn-success" onclick="deactivateNews('+id+')">@lang('admin.news.deactivate')</button>');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }
</script>