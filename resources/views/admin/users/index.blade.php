@include('admin.header')


<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        @lang('admin.users.title')</h4>
                </div>
                @if( count($errors)> 0)
                    @foreach($errors as $error)
                        <h2 class="alert alert-danger alert-dismissible"
                            style="border-radius: 20px; padding: 1%; margin-left: 2%; margin-right: 2%;}">{{Lang::get('admin.users.'.$error)}} </h2>
                    @endforeach
                @else
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">{{Lang::get('admin.users.id')}}</th>
                                <th scope="col">{{Lang::get('admin.users.name')}}</th>
                                <th scope="col">{{Lang::get('admin.users.email')}}</th>
                                <th scope="col">{{Lang::get('admin.users.type')}}</th>
                                <th scope="col">{{Lang::get('admin.users.showProfile')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $record)

                                <tr>
                                    <th scope="row">{{$record->id}}</th>
                                    <td>{{$record->name}}</td>
                                    <td>{{$record->email}}</td>
                                    <td>@if( $record->role == $vip)
                                            {{Lang::get('admin.users.vipUSer')}}
                                        @elseif($record->role == $master)
                                            {{Lang::get('admin.users.masterUSer')}}
                                        @elseif($record->role == $admin)
                                            {{Lang::get('admin.users.adminUser')}}
                                        @elseif($record->role == $regular)
                                            {{Lang::get('admin.users.regularUser')}}
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-success" href="{{ url('admin/home/users/show/'.$record->id) }}">{{Lang::get('admin.users.showProfileBtn')}} </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr class="pagination">
                                @for ($i = 1; $i <= $count; $i++)
                                    <td class="page-item"><a class="page-link" href="#">{{$i}}</a></td>
                                @endfor
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="row">

    </div>

</div>

@include('admin.footer')
@include('admin.news.js.javascripts')
