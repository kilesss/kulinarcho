@include('admin.header')

<style>
    .dashboard-item {
        padding: 2%;
    }

    .listBTN {
        padding: 3%;
        padding-left: 10%;
        border-bottom: 1px solid silver;
        color: #737373;
    }
</style>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                @if( count($errors)> 0)
                    @foreach($errors as $error)
                        <h2 class="alert alert-danger alert-dismissible"
                            style="border-radius: 20px; padding: 1%; margin-left: 2%; margin-right: 2%;}">{{Lang::get('admin.users.'.$error)}} </h2>
                    @endforeach
                @else
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="card">
                                        <div class="my-sidebar">
                                            <div class="my-avatar">
                                                <img style="    margin-left: 19%;border-radius: 50%;margin-top: 7%;"
                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2017/12/maxresdefault-1-150x150.jpg"
                                                     class="img-responsive" alt="author">
                                                <h4>Demo Demo</h4>

                                                <ul class="list-unstyled list-inline post-share">
                                                </ul>
                                            </div>
                                            <ul class="list-unstyled my-menu" style="margin-bottom: 0">
                                                <li class="listBTN" id="showDashboard">
                                                    {{Lang::get('admin.userShow.dashboard')}}
                                                </li>
                                                <li class="listBTN" id="showpublishedRecipes">
                                                    {{Lang::get('admin.userShow.publishedRecipes')}}
                                                </li>
                                                <li class="listBTN" id="showfavoriteRecipes">
                                                    {{Lang::get('admin.userShow.favoriteRecipes')}}
                                                </li>
                                                <li class="listBTN" id="showownRecipes">
                                                    {{Lang::get('admin.userShow.ownRecipes')}}
                                                </li>
                                                <li class="listBTN" id="showmenu">
                                                    {{Lang::get('admin.userShow.menu')}}
                                                </li>
                                                <li class="listBTN" id="showdiet">
                                                    {{Lang::get('admin.userShow.diet')}}
                                                </li>
                                                <li class="listBTN" id="showlistShoping">
                                                    {{Lang::get('admin.userShow.listShoping')}}
                                                </li>
                                                <li class="listBTN" id="showmakedShopping">
                                                    {{Lang::get('admin.userShow.makedShopping')}}
                                                </li>
                                                <li class="listBTN" id="showcookedRecipes">
                                                    {{Lang::get('admin.userShow.cookedRecipes')}}
                                                </li>
                                                <li class="listBTN">
                                                    {{Lang::get('admin.userShow.messeged')}}
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-9">
                                    <div class="card">
                                        <div class="content-inner fieldToHide" id="dashboardField">

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="dashboard-item clearfix">
                                                        <div class="pull-left">
                                                            <i class="fa fa-book"></i>
                                                            Published Recipes:
                                                        </div>

                                                        <div class="pull-right badge">
                                                            0
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="dashboard-item clearfix">
                                                        <div class="pull-left">
                                                        </div>

                                                        <div class="pull-right badge">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="dashboard-item clearfix">
                                                        <div class="pull-left">
                                                            <i class="fa fa-reply"></i>
                                                            Waiting For Approval Recipes:
                                                        </div>

                                                        <div class="pull-right badge">
                                                            2
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="dashboard-item clearfix">
                                                        <div class="pull-left">
                                                            <i class="fa fa-heart"></i>
                                                            Favourite Recipes:
                                                        </div>

                                                        <div class="pull-right badge">
                                                            4
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="dashboard-item clearfix">
                                                        <div class="pull-left">
                                                            <i class="fa fa-star"></i>
                                                            Ratings:
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="dashboard-item clearfix">
                                                        <div class="pull-left">
                                                            <i class="fa fa-cutlery"></i>
                                                            Cooking Level:
                                                        </div>

                                                        <div class="pull-right badge">
                                                            Rookie
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="dashboardField">
                                            <h1></h1>
                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="showpublishedRecipesField">
                                            <div class="white-block">
                                                <div class="content-inner">
                                                    <h4 class="no-top-margin">John Doe's recipes</h4>
                                                    <hr>
                                                    <p class="pretable-loading" style="display: none;">Loading...</p>
                                                    <div class="bt-table" style="display: block;">
                                                        <div class="bootstrap-table">
                                                            <div class="fixed-table-toolbar">
                                                                <div class="pull-left search"><input
                                                                            class="form-control" type="text"
                                                                            placeholder="Search for recipes..."></div>
                                                            </div>
                                                            <div class="fixed-table-container">
                                                                <div class="fixed-table-header">
                                                                    <table></table>
                                                                </div>
                                                                <div class="fixed-table-body">
                                                                    <div class="fixed-table-loading" style="top: 37px;">
                                                                        Loading, please wait...
                                                                    </div>
                                                                    <table data-toggle="table" data-search-align="left"
                                                                           data-search="true"
                                                                           data-classes="table table-striped"
                                                                           class="table table-striped">
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Image
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner sortable">
                                                                                    Name
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Ratings
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Difficulty
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr data-index="0">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/boilermaker-tailgate-chili/"
                                                                                   target="_blank">
                                                                                    Boilermaker Tailgate Chili </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 3.67 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 73.4%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr data-index="1">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/"
                                                                                   target="_blank">
                                                                                    Homemade Granola </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 4.5 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 90%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr data-index="2">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/sparkling-mint-juleps/"
                                                                                   target="_blank">
                                                                                    Sparkling mint juleps </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 3 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 60%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="fixed-table-pagination"
                                                                     style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="showfavoriteRecipesField">
                                            <div class="white-block">
                                                <div class="content-inner">
                                                    <h4 class="no-top-margin">John Doe's recipes</h4>
                                                    <hr>
                                                    <p class="pretable-loading" style="display: none;">Loading...</p>
                                                    <div class="bt-table" style="display: block;">
                                                        <div class="bootstrap-table">
                                                            <div class="fixed-table-toolbar">
                                                                <div class="pull-left search"><input
                                                                            class="form-control" type="text"
                                                                            placeholder="Search for recipes..."></div>
                                                            </div>
                                                            <div class="fixed-table-container">
                                                                <div class="fixed-table-header">
                                                                    <table></table>
                                                                </div>
                                                                <div class="fixed-table-body">
                                                                    <div class="fixed-table-loading" style="top: 37px;">
                                                                        Loading, please wait...
                                                                    </div>
                                                                    <table data-toggle="table" data-search-align="left"
                                                                           data-search="true"
                                                                           data-classes="table table-striped"
                                                                           class="table table-striped">
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Image
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner sortable">
                                                                                    Name
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Ratings
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Difficulty
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr data-index="0">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/boilermaker-tailgate-chili/"
                                                                                   target="_blank">
                                                                                    Boilermaker Tailgate Chili </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 3.67 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 73.4%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr data-index="1">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/"
                                                                                   target="_blank">
                                                                                    Homemade Granola </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 4.5 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 90%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr data-index="2">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/sparkling-mint-juleps/"
                                                                                   target="_blank">
                                                                                    Sparkling mint juleps </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 3 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 60%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="fixed-table-pagination"
                                                                     style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="dshowownRecipesField">
                                            <div class="white-block">
                                                <div class="content-inner">
                                                    <h4 class="no-top-margin">John Doe's recipes</h4>
                                                    <hr>
                                                    <p class="pretable-loading" style="display: none;">Loading...</p>
                                                    <div class="bt-table" style="display: block;">
                                                        <div class="bootstrap-table">
                                                            <div class="fixed-table-toolbar">
                                                                <div class="pull-left search"><input
                                                                            class="form-control" type="text"
                                                                            placeholder="Search for recipes..."></div>
                                                            </div>
                                                            <div class="fixed-table-container">
                                                                <div class="fixed-table-header">
                                                                    <table></table>
                                                                </div>
                                                                <div class="fixed-table-body">
                                                                    <div class="fixed-table-loading" style="top: 37px;">
                                                                        Loading, please wait...
                                                                    </div>
                                                                    <table data-toggle="table" data-search-align="left"
                                                                           data-search="true"
                                                                           data-classes="table table-striped"
                                                                           class="table table-striped">
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Image
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner sortable">
                                                                                    Name
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Ratings
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                            <th style="">
                                                                                <div class="th-inner ">
                                                                                    Difficulty
                                                                                </div>
                                                                                <div class="fht-cell"></div>
                                                                            </th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr data-index="0">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/soup-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/boilermaker-tailgate-chili/"
                                                                                   target="_blank">
                                                                                    Boilermaker Tailgate Chili </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 3.67 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 73.4%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr data-index="1">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/nougat-599518_1920-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/homemade-granola/"
                                                                                   target="_blank">
                                                                                    Homemade Granola </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 4.5 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 90%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr data-index="2">
                                                                            <td style="">
                                                                                <img width="150" height="150"
                                                                                     src="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-150x150.jpg"
                                                                                     class="attachment-thumbnail size-thumbnail wp-post-image"
                                                                                     alt=""
                                                                                     srcset="http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-150x150.jpg 150w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-50x50.jpg 50w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-25x25.jpg 25w, http://demo.djmimi.net/themes/recipe/wp-content/uploads/2015/02/bev_1-40x40.jpg 40w"
                                                                                     sizes="(max-width: 150px) 100vw, 150px">
                                                                            </td>
                                                                            <td style="">
                                                                                <a href="http://demo.djmimi.net/themes/recipe/recipe/sparkling-mint-juleps/"
                                                                                   target="_blank">
                                                                                    Sparkling mint juleps </a>
                                                                            </td>
                                                                            <td style="">
                                                                                <span class="bottom-ratings tip"
                                                                                      data-title="Average Rate: 3 / 5"><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="icon icon_rate"></span><span
                                                                                            class="top-ratings"
                                                                                            style="width: 60%"><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span><span
                                                                                                class="icon icon_rate"></span></span></span>
                                                                            </td>
                                                                            <td style="">
															<span class="tip level easy" data-title="Difficulty: Easy">
		<span class="level-bar-1"></span>
		<span class="level-bar-2"></span>
		<span class="level-bar-3"></span>
	</span>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="fixed-table-pagination"
                                                                     style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="showmenuField">
                                            <h1>showmenuField</h1>
                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="showdietField">
                                            <h1>showdiet</h1>
                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="showlistShopingField">
                                            <h1>showlistShopingField</h1>
                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="showmakedShoppingField">
                                            <h1>showmakedShoppingField</h1>
                                        </div>
                                        <div class="content-inner fieldToHide" style="display: none;"
                                             id="showcookedRecipesField">
                                            <h1>showcookedRecipesField</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                    </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">

        </div>

    </div>

@include('admin.footer')
@include('admin.usersShow.js.javascripts')
