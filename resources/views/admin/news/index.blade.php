@include('admin.header')


<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        @lang('admin.news.addNews')</h4>
                </div>
                <div class="card-body">

                    {!! Form::open(array('files'=>true, 'id'=>'frontNewsForm')) !!}


                    <div class="form-group">
                        <label for="titleNews">@lang('admin.news.title')</label>
                        {!! Form::input('text', 'titleNews' ,null, ['placeholder'=>Lang::get('admin.news.title'),
                                                                    'class'=> 'form-control',
                                                                    'id'=> 'titleNews']) !!}


                    </div>
                    <div class=" form-group">
                        <label for="contentNews">@lang('admin.news.content')</label>
                        {!! Form::textarea( 'contentNews' ,null, ['placeholder'=>Lang::get('admin.news.content'),
                                                                  'rows'=>'3',
                                                                  'class'=> 'form-control',
                                                                  'id'=> 'contentNews']) !!}
                    </div>
                    <div class="form-group">
                        <label for="textNews">@lang('admin.news.news')</label>
                        {!! Form::textarea('textNews' ,null, [ 'rows'=>'3',
                                                                'placeholder'=> Lang::get('admin.news.news'),
                                                                'class'=> 'form-control',
                                                                'id'=> 'textNews']) !!}
                    </div>
                    <div class="">
                        <label for="newsImages">@lang('admin.news.image')</label>
                        {{ Form::file('newsImages', ['class' => 'form-control-file', 'id'=>'exampleFormControlFile1']) }}

                    </div>
                    <div class="col-md-12" style=" margin-left:  0.5%; margin-top: 1%;">
                        {!! Form::checkbox('activateNews', '1', false,[ 'class'=> 'form-check-input','id'=> 'activateNews']) !!}
                        <label class="form-check-label" for="activateNews">
                            @lang('admin.news.activate')
                        </label>
                    </div>
                    <input type="hidden" value="{{ csrf_token() }}" name="csrf-token">
                    <input type="hidden" value="" name="paginator-page" id="paginator-page">
                    <br>
                    <input type="submit" class="btn btn-primary" placeholder="@lang('admin.news.submit')">
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
</div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @foreach ($records as $record)
                    <div class="card">
                        <div class="col-md-12">
                            <h3 class="post-title">{{$record->title}}</h3>
                            <hr>
                            <div class="post-content">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="{{ asset('img/frontPageNewsImages/'.$record->image) }}" />
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            {{$record->content}}
                                        </div>
                                        <p>{{$record->body}}</p>
                                        <div class="row">
                                            <b> Created at: {{$record->updated_at}}</b>
                                        </div>
                                        <div class="row">
                                            <b>Updated at : {{$record->created_at}}</b>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="row" id="btn_{{$record->id}}">
                                    @if($record->active == 1)
                                        <button class="btn btn-success" onclick="deactivateNews({{$record->id}})">@lang('admin.news.deactivate')</button>
                                        @else
                                        <button class="btn btn-danger" onclick="activateNews({{$record->id}})">@lang('admin.news.activate')</button>

                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                @endforeach


            </div>
        </div>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                @for ($i = 1; $i <= $count; $i++)
                    <li class="page-item"><a class="page-link" href="#">{{$i}}</a></li>
                @endfor
            </ul>
        </nav>
    </div>

@include('admin.footer')
@include('admin.news.js.javascripts')
