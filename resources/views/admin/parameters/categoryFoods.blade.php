<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading_8">
        <div class="panel-title">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_ZbMUOiTTwm"
               href="#coll_8_ZbMUOiTTwm" aria-expanded="true" aria-controls="coll_0_ZbMUOiTTwm">
                @lang('admin.params.foodCategory')
                <i class="fa fa-chevron-circle-down animation" style=" float: right;"> </i>
            </a>
        </div>
    </div>
    <div id="coll_8_ZbMUOiTTwm" class="panel-collapse collapse " role="tabpanel"
         aria-labelledby="heading_8" style="height: 0px;">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input('text', 'foodCategory' ,null, ['placeholder'=>Lang::get('admin.params.foodCategory'),
                                                                    'class'=> 'form-control',
                                                                    'id'=> 'foodCategoryInput']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btnAddArticle btn-default" style=" margin-top: 0;"
                         id="addFoodCategoryInputBtn">@lang('admin.params.addBtn')</div>
                </div>
                <div class="col-md-12" id="fieldFoodCategoryList">
                    @foreach ($foodCategoriesList as $category)
                        <div class="col-md-4 form-control" id="foodCategoryId_{{$category->id}}">
                            <p style="margin-bottom: 0;">{{$category->name}}
                                <input type="checkbox" name="onMenu" @if($category->showOnMenu == 1) checked @endif class="onMenu" id="onMenu_{{$category->id}}" onclick="FoodCategoryOnMenu({{$category->id}})">
                                <i style="float: right;" onclick="removeFoodCategory({{$category->id}})">X</i>
                            </p>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
