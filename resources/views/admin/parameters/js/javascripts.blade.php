<script>

    $("#addCategoryInputBtn").click(function () {
        var val = $("#categoryInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'addcategory', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                if (response != 0) {
                    $("#fieldCategoryList").append('<div id="categoryId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                            '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeCategory(' + response + ')" >X</i></p></div>')
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });
    $("#addFoodCategoryInputBtn").click(function () {
        var val = $("#foodCategoryInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'foodCategory', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                if (response != 0) {
                    $("#fieldFoodCategoryList").append('<div id="foodCategoryId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                        '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeFoodCategory(' + response + ')" >X</i></p></div>')
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });
    function removeCategory(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'removeCategory', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                if (response != 0) {
                    $("#categoryId_" + id).remove();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }
    function removeFoodCategory(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'removeFoodCategory', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                if (response != 0) {
                    $("#foodCategoryId_" + id).remove();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }


    $("#addTypeInputBtn").click(function () {
        var val = $("#TypeInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'addType', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#fieldTypeList").append('<div id="typeId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                                '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeType(' + response + ')" >X</i></p></div>')
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });
    function removeType(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'removeType', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#typeId_" + id).remove();
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }

    $("#addCutsInputBtn").click(function () {
        var val = $("#CutsInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'addCuts', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#fieldCutsList").append('<div id="cutsId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                                '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeCuts(' + response + ')" >X</i></p></div>')
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });
    $("#addProductsInputBtn").click(function () {
        var val = $("#ProductsInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'addProduct', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#fieldCutsList").append('<div id="cutsId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                                '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeCuts(' + response + ')" >X</i></p></div>')
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });
    //    $("#ProductsInput").on('change keyup paste', function () {
    //        var name = $("#ProductsInput").val();
    //        if(name.length() >= 3){
    //
    //        }
    //        console.log(name);
    //    });
    $.event.special.inputchange = {
        setup: function () {
            var self = this, val;
            $.data(this, 'timer', window.setInterval(function () {
                val = self.value;
                if ($.data(self, 'cache') != val) {
                    $.data(self, 'cache', val);
                    $(self).trigger('inputchange');
                }
            }, 20));
        },
        teardown: function () {
            window.clearInterval($.data(this, 'timer'));
        },
        add: function () {
            $.data(this, 'cache', this.value);
        }
    };

    $('#ProductsInput').on('inputchange', function () {
        var name = $("#ProductsInput").val();
        var suggestionText = '';
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'productInput', 'id': name}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                $('#autofillproducts').html('');

                response = JSON.parse(response);
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $.each(response, function (key, val) {

                            suggestionText = suggestionText + '<div id="suggestionProduct_' + val["id"] + '">' + val["name"] + '</div>';
                        });
                        $('#autofillproducts').html(suggestionText);
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });

    });

    function removeCuts(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'removeCuts', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#cutsId_" + id).remove();
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }


    function checkErrors(responce) {
        responce = String(responce);
        if (responce.toString() == '"requireLogin"') {
            return "Моля логнете се";
        }
        if (responce.toString() == '"noPermision"') {

            return "Нямата права да извършвате тази функция";
        }
        return 1;

    }


    function editProductShow(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'editProductShow', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                response = JSON.parse(response);
                $("#editProductID").val(response['id']);
                $("#editChangeName").val(response['name']);
                $("#editChangeHint").val(response['hint']);
                $("#editChangeCalories").val(response['calories']);
                $("#editChangeProteins").val(response['proteins']);
                $("#editChangeFats").val(response['fat']);
                if (response['showOnMenu'] == '1'){
                    $('#showOnMenu').prop('checked', true);
                }else{
                    $('#showOnMenu').prop('checked', false);
                }
                $("#foodcategoryid").val(response['foodCategoryId']);
                $("#editChangeCarbohydrates").val(response['carbohydrates']);
                $('#editProductModal').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }

    $("#editProductCLoseBtn2").click(function(){
        $("#editProductID").val('');
        $("#editChangeName").val('');
        $("#editChangeHint").val('');
        $("#editChangeCalories").val('');
        $('#showOnMenu').prop('checked', false);
        $("#foodcategoryid").val(0);
        $("#editChangeProteins").val('');
        $("#editChangeFats").val('');
        $("#editChangeCarbohydrates").val('');
    });
    $("#editProductCLoseBtn").click(function(){
        $("#editProductID").val('');
        $("#editChangeName").val('');
        $("#editChangeHint").val('');
        $("#editChangeCalories").val('');
        $("#editChangeProteins").val('');
        $('#showOnMenu').prop('checked', false);
        $("#foodcategoryid").val(0);
        $("#editChangeFats").val('');
        $("#editChangeCarbohydrates").val('');
    });

    $("#editProductSubmitBtn").click(function(){
        var id= $("#editProductID").val();
        if (id > 0){
            $.ajax({
                method: 'POST', // Type of response and matches what we said in the route
                url: '', // This is the url we gave in the route
                data: {'ajax': 'ajax',
                    'type': 'editProductSubmit',
                    'id': id,
                    'name': $("#editChangeName").val(),
                    'hint': $("#editChangeHint").val(),
                    'calories': $("#editChangeCalories").val(),
                    'proteins': $("#editChangeProteins").val(),
                    'fats': $("#editChangeFats").val(),
                    'foodcategoryid': $("#foodcategoryid").val(),
                    'showOnMenu': $(".showOnMenu").is(":checked"),
                    'carbo': $("#editChangeCarbohydrates").val(),
                }, // a JSON object to send back
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) { // What to do if we succeed
                    if(response === '1'){
                        showSuccessMain("{{Lang::get('admin.params.editProductSuccessUpdate')}}");
                        $('#editProductModal').modal('hide');
                    }else{
                        showErrorsMain("{{Lang::get('admin.params.editProductNotupdated')}}");
                    }
                },
            });
        }else{
            showErrorsMain("{{Lang::get('admin.params.editWrongProduct')}}");
        }
        // var name= $("#editChangeName").val();
        // var hint= $("#editChangeHint").val();
        // var calories= $("#editChangeCalories").val();
        // var prote= $("#editChangeProteins").val();
        // var = $("#editChangeFats").val();
        // var = $("#editChangeCarbohydrates").val();
    });
    function FoodCategoryOnMenu(id){

        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax',
                'type': 'foodCategoryOnMenu',
                'id': id,
                'checked': $('#onMenu_' + id).is(":checked")
                }, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                //TODO: check the popups
                {{--if(response === '1'){--}}
                    {{--showSuccessMain("{{Lang::get('admin.params.editProductSuccessUpdate')}}");--}}
                    {{--$('#editProductModal').modal('hide');--}}
                {{--}else{--}}
                    {{--showErrorsMain("{{Lang::get('admin.params.editProductNotupdated')}}");--}}
                {{--}--}}
            }
        });
        console.log(
        )
    }
</script>
