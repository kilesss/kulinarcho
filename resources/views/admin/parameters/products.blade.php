<style>
    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
    }
    </style>
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading_2">
        <div class="panel-title">
            <a class="" data-toggle="collapse" data-parent="#accordion_ZbMUOiTTwm"
               href="#coll_2_ZbMUOiTTwm" aria-expanded="true" aria-controls="coll_2_ZbMUOiTTwm">
                @lang('admin.params.products')
                <i class="fa fa-chevron-circle-down animation" style=" float: right;"></i>
            </a>
        </div>
    </div>
    <div id="coll_2_ZbMUOiTTwm" class="panel-collapse @if($productPage> 0) collapsed @else collapse in @endif" role="tabpanel"
         aria-labelledby="heading_2" style="">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input('text', 'titleNews' ,null, ['placeholder'=>Lang::get('admin.params.productsReceipt'),
                                                                    'class'=> 'form-control',
                                                                    'id'=> 'ProductsInput']) !!}
                        <div id="autofillproducts" style=" padding-left: 6%;"></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btnAddArticle btn-default" style=" margin-top: 0;"
                         id="addProductsInputBtn">@lang('admin.params.addBtn')</div>
                </div>
            </div>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>{{Lang::get('admin.params.tableNameTitle')}}</th>
                    <th>{{Lang::get('admin.params.tableEditTitle')}}</th>
                    <th>{{Lang::get('admin.params.tableEditShow')}}</th>
                    <th>{{Lang::get('admin.params.tableEditDelete')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($recordsProduct as $record)
                    <tr>
                        <td>{{$record->name}}</td>
                        <td><button class="btn btn-edit" onclick="editProductShow({{$record->id}})" id="editProductBtn_{{$record->id}}">{{Lang::get('admin.params.btnEditProduct')}}</button></td>
                        <td><button class="btn btn-success" id="showProductBtn_{{$record->id}}">{{Lang::get('admin.params.btnShowProducts')}}</button></td>
                        <td><button class="btn btn-danger"  onclick="removeProduct({{$record->id}})">{{Lang::get('admin.params.btnDeleteProduct')}}</button></td>
                    </tr>

                @endforeach

                </tbody>
            </table>


            <!-- Modal -->
            <div class="modal fade" id="editProductModal" tabindex="-1" role="dialog" aria-labelledby="editProductModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{Lang::get('admin.params.editProductTitle')}}</h5>
                            <button type="button" class="close" id="editProductCLoseBtn2" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row col-md-12">
                                <div class="form-group col-md-12">
                                    <label for="editChangeName">{{Lang::get('admin.params.editChangeName')}}</label>
                                    {!! Form::input('text', 'editChangeName' ,null, ['placeholder'=>Lang::get('admin.params.editChangeName'),
                                                                    'class'=> 'form-control col-md-12',
                                                                    'id'=> 'editChangeName']) !!}
                                    <small id="emailHelp" class="form-text text-muted">{{Lang::get('admin.params.editHintChangeName')}}</small>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="form-group col-md-12">
                                    <label for="exampleInputEmail1">{{Lang::get('admin.params.editChangeHint')}}</label>
                                    {!! Form::input('text', 'editChangeHint' ,null, ['placeholder'=>Lang::get('admin.params.editChangeHint'),
                                                                                                                      'class'=> 'form-control col-md-12',
                                                                                                                      'id'=> 'editChangeHint']) !!}
                                    <small id="emailHelp" class="form-text text-muted">{{Lang::get('admin.params.editHintChangeHint')}}</small>
                                </div>
                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-12">
                                    <label for="foodcategoryid">{{Lang::get('admin.params.foodCategory')}}</label>
                                    <select class="form-control" id="foodcategoryid">
                                        <option value="0"  selected>{{Lang::get('admin.params.foodCategorySelect')}}</option>
                                        @foreach ($foodCategoriesList as $category)

                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>

                                    <small id="FoodCategory" class="form-text text-muted">{{Lang::get('admin.params.editFoodCategoryHint')}}</small>
                                </div>

                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-12">
                                    <label for="foodcategoryid">{{Lang::get('admin.params.foodCategory')}}</label>
                                    <input type="checkbox" name="showOnMenu" id="showOnMenu" class="showOnMenu" style=" width: 20px;  height:  20px;">

                                    <small id="showOnMenu" class="form-text text-muted">{{Lang::get('admin.params.editshowOnMenuHint')}}</small>
                                </div>

                            </div>


                            <div class="row col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">{{Lang::get('admin.params.editChangeCalories')}}</label>
                                        {!! Form::input('text', 'editChangeCalories' ,null, ['placeholder'=>Lang::get('admin.params.editChangeCalories'),
                                                                                                                          'class'=> 'form-control col-md-12',
                                                                                                                          'id'=> 'editChangeCalories']) !!}
                                        <small id="emailHelp" class="form-text text-muted">{{Lang::get('admin.params.editHintChangeCalories')}}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="editChangeProteins">{{Lang::get('admin.params.editChangeProteins')}}</label>
                                        {!! Form::input('text', 'editChangeName' ,null, ['placeholder'=>Lang::get('admin.params.editChangeProteins'),
                                                                                                                          'class'=> 'form-control col-md-12',
                                                                                                                          'id'=> 'editChangeProteins']) !!}
                                        <small id="emailHelp" class="form-text text-muted">{{Lang::get('admin.params.editHintChangeProteins')}}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="editChangeFats">{{Lang::get('admin.params.editChangeFats')}}</label>
                                        {!! Form::input('text', 'editChangeName' ,null, ['placeholder'=>Lang::get('admin.params.editChangeFats'),
                                                                                                                          'class'=> 'form-control col-md-12',
                                                                                                                          'id'=> 'editChangeFats']) !!}
                                        <small id="emailHelp" class="form-text text-muted">{{Lang::get('admin.params.editHintChangeFats')}}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="editChangeCarbohydrates">{{Lang::get('admin.params.editChangeCarbohydrates')}}</label>
                                        {!! Form::input('text', 'editChangeName' ,null, ['placeholder'=>Lang::get('admin.params.editChangeCarbohydrates'),
                                                                                                                          'class'=> 'form-control col-md-12',
                                                                                                                          'id'=> 'editChangeCarbohydrates']) !!}
                                        <small id="emailHelp" class="form-text text-muted">{{Lang::get('admin.params.editHintChangeCarbohydrates')}}</small>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="editProductID" value="" name="editProductID">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" id="editProductCLoseBtn" data-dismiss="modal">Close</button>
                            <button type="button" id="editProductSubmitBtn" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
            <table>
                <tr class="pagination">
                    @php
                        $r = 1;
                    @endphp
                    @for ($i = 1; $i <= $countProduct; $i++)
                        <td class="page-item"><a class="page-link" href="{!! url('admin/home/parameters/products/'.$i) !!}">{{$i}}</a></td>
                        @if($r == 15) @php $r = 1; @endphp</tr><tr class="pagination">@endif
                    @php $r = $r+1; @endphp
                    @endfor
                </tr>
            </table>
        </div>
    </div>
</div>