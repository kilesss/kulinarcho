
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading_1">
        <div class="panel-title">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_ZbMUOiTTwm"
               href="#coll_1_ZbMUOiTTwm" aria-expanded="true" aria-controls="coll_1_ZbMUOiTTwm">
                @lang('admin.params.typeReceipt')
                <i class="fa fa-chevron-circle-down animation" style=" float: right;"></i>
            </a>
        </div>
    </div>
    <div id="coll_1_ZbMUOiTTwm" class="panel-collapse collapse" role="tabpanel"
         aria-labelledby="heading_1" style="height: 0px;">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input('text', 'titleNews' ,null, ['placeholder'=>Lang::get('admin.params.typeReceipt'),
                                                                    'class'=> 'form-control',
                                                                    'id'=> 'TypeInput']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btnAddArticle btn-default" style=" margin-top: 0;"
                         id="addTypeInputBtn">@lang('admin.params.addBtn')</div>
                </div>
                <div class="col-md-6" id="fieldTypeList">
                    @foreach ($typesList as $type)
                        <div class="col-md-4 form-control" id="typeId_{{$type->id}}">
                            <p style="margin-bottom: 0;">{{$type->name}}<i style="float: right;"
                                                                           onclick="removeType({{$type->id}})">X</i></p></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>