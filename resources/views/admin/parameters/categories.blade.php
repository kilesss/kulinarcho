
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading_0">
        <div class="panel-title">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_ZbMUOiTTwm"
               href="#coll_0_ZbMUOiTTwm" aria-expanded="true" aria-controls="coll_0_ZbMUOiTTwm">
                @lang('admin.params.category')
                <i class="fa fa-chevron-circle-down animation" style=" float: right;"> </i>
            </a>
        </div>
    </div>
    <div id="coll_0_ZbMUOiTTwm" class="panel-collapse collapse" role="tabpanel"
         aria-labelledby="heading_0" style="height: 0px;">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input('text', 'titleNews' ,null, ['placeholder'=>Lang::get('admin.params.category'),
                                                                    'class'=> 'form-control',
                                                                    'id'=> 'categoryInput']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btnAddArticle btn-default" style=" margin-top: 0;"
                         id="addCategoryInputBtn">@lang('admin.params.addBtn')</div>
                </div>
                <div class="col-md-6" id="fieldCategoryList">
                    @foreach ($categoriesList as $category)
                        <div class="col-md-4 form-control" id="categoryId_{{$category->id}}">
                            <p style="margin-bottom: 0;">{{$category->name}}<i style="float: right;"
                                                                               onclick="removeCategory({{$category->id}})">X</i></p></div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
