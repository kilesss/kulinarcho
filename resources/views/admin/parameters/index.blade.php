@include('admin.header')
<style>
    .panel-title {
        margin-bottom: 1%;
        border: 1px solid silver;
        border-radius: 5px;
        padding: 1%;
    }
    .btnAddArticle{
    border: 1px solid silver;
    border-radius:  23px;
    /* padding-bottom:  0; */
    /* margin-bottom: 0; */
    padding-bottom: 4%;
    padding-left:  14%;
    padding-top: 4%;
    }
</style>

<div class="content">
    <div class="card">

        <div class="card-header post-content">

            <div class="panel-group" id="accordion_ZbMUOiTTwm" role="tablist" aria-multiselectable="true">
                @include('admin.parameters.categories')
                @include('admin.parameters.categoryFoods')
                @include('admin.parameters.typeRecipe')
                @include('admin.parameters.products')
                @include('admin.parameters.cuts')
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


@include('admin.footer')
@include('admin.parameters.js.javascripts')
