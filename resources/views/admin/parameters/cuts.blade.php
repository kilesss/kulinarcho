<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading_3">
        <div class="panel-title">
            <a class="" data-toggle="collapse" data-parent="#accordion_ZbMUOiTTwm"
               href="#coll_3_ZbMUOiTTwm" aria-expanded="true" aria-controls="coll_2_ZbMUOiTTwm">
                @lang('admin.params.volumes')
                <i class="fa fa-chevron-circle-down animation" style=" float: right;"></i>
            </a>
        </div>
    </div>
    <div id="coll_3_ZbMUOiTTwm" class="panel-collapse collapse in" role="tabpanel"
         aria-labelledby="heading_3" style="">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input('text', 'titleNews' ,null, ['placeholder'=>Lang::get('admin.params.cutReceipt'),
                                                                    'class'=> 'form-control',
                                                                    'id'=> 'CutsInput']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btnAddArticle btn-default" style=" margin-top: 0;"
                         id="addCutsInputBtn">@lang('admin.params.addBtn')</div>
                </div>
                <div class="col-md-6" id="fieldCutsList">
                    @foreach ($cutsList as $type)
                        <div class="col-md-4 form-control" id="cutsId_{{$type->id}}">
                            <p style="margin-bottom: 0;">{{$type->name}}<i style="float: right;"
                                                                           onclick="removeCuts({{$type->id}})">X</i></p></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>