<script>
    $("#receiptActive").change(function(){
        var id = $( "#receiptActive option:selected" ).val();
       sendAjaxSort('sortActive', id)
    });


    $("#receiptType").change(function(){
        var id = $( "#receiptType option:selected" ).val();
        sendAjaxSort('sortType', id)
    });
    $("#receiptCategory").change(function(){
        var id = $( "#receiptCategory option:selected" ).val();
        sendAjaxSort('sortCategory', id)
    });

    function sendAjaxSort(subtype, id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'recipesListSort','subType': subtype, 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#cutsId_" + id).remove();
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }

</script>