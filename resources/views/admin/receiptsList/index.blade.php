@include('admin.header')
{{--<style>--}}
{{--.panel-title {--}}
{{--margin-bottom: 1%;--}}
{{--border: 1px solid silver;--}}
{{--border-radius: 5px;--}}
{{--padding: 1%;--}}
{{--}--}}
{{--.btnAddArticle{--}}
{{--border: 1px solid silver;--}}
{{--border-radius:  23px;--}}
{{--/* padding-bottom:  0; */--}}
{{--/* margin-bottom: 0; */--}}
{{--padding-bottom: 4%;--}}
{{--padding-left:  14%;--}}
{{--padding-top: 4%;--}}
{{--}--}}
{{--</style>--}}
<div class="panel-header panel-header-sm" xmlns="http://www.w3.org/1999/html">
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h4 class="card-title">
                            @lang('admin.receiptList.titlePage')</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="receiptProductdiv">{{Lang::get('admin.receiptList.searchName')}}</label>

                                    <div class="form-group receiptProductdiv" id="ProductSuggestion_1">
                                        {!! Form::input('text', 'searchNameInput' ,null, ['placeholder'=>Lang::get('admin.receiptList.searchName'),
                                                                                    'class'=> 'form-control', 'id'=>'searchNameInput']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">

                                    <div class="form-group">
                                        <label for="receiptCategory">{{Lang::get('admin.receiptList.searchCategory')}}</label>
                                        <select class="form-control" name="searchCategoryInput" id="receiptCategory">
                                            <option value="0">{{Lang::get('admin.receipt.choose')}}</option>
                                            @foreach($categories as $cat)
                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="receiptType">{{Lang::get('admin.receiptList.searchType')}}</label>
                                        <select class="form-control" name="searchTypeInput" id="receiptType">
                                            <option value="0">{{Lang::get('admin.receipt.choose')}}</option>
                                            @foreach($types as $typ)

                                                <option value="{{$typ->id}}">{{$typ->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="receiptActive">{{Lang::get('admin.receiptList.activeUnactive')}}</label>
                                        <select class="form-control" name="searchTypeInput" id="receiptActive">
                                            <option value="">{{Lang::get('admin.receipt.choose')}}</option>
                                            <option value="1">{{Lang::get('admin.receiptList.titleUnactive')}}</option>
                                            <option value="0">{{Lang::get('admin.receiptList.titleActive')}}</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                              
                                <th>{{Lang::get('admin.receiptList.tableName')}}</th>
                                <th>{{Lang::get('admin.receiptList.tableCategory')}}</th>
                                <th>{{Lang::get('admin.receiptList.tableType')}}</th>
                                <th>{{Lang::get('admin.receiptList.activeUnactive')}}</th>
                                <th>{{Lang::get('admin.receiptList.tableShowEdit')}}</th>
                                <th>{{Lang::get('admin.receiptList.tableDelete')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($list['records'] as $rec)
                                <tr>

                                    <td>{{$rec->title}}</td>
                                    <td> @foreach($categories as $cat)
                                            @if($cat->id == $rec->category)
                                                {{$cat->name}}
                                            @endif
                                        @endforeach</td>
                                    <td> @foreach($types as $typ)
                                            @if($typ->id == $rec->type)
                                                {{$typ->name}}
                                            @endif
                                        @endforeach</td>
                                    <td>@if($rec->active == 1)
                                            {{Lang::get('admin.receiptList.titleActive')}}
                                        @else
                                            {{Lang::get('admin.receiptList.titleUnactive')}}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{!! url($global['routes']['receipt']) !!}/{{$rec->id}}" class="edit btn btn-warning" >{{Lang::get('admin.receiptList.edit')}}</a>
                                    </td>
                                    <td>
                                        <a href="#editEmployeeModal" class="edit btn btn-danger" data-toggle="modal">{{Lang::get('admin.receiptList.delete')}}</a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                        <table>
                            <tr class="pagination">
                                @php
                                    $r = 1;
                                @endphp
                                @for ($i = 1; $i <= $list['count']; $i++)
                                    <td class="page-item"><a class="page-link" href="{!! url('admin/home/receiptList/'.$i) !!}">{{$i}}</a></td>
                                    @if($r == 15) @php $r = 1; @endphp</tr><tr class="pagination">@endif
                                @php $r = $r+1; @endphp
                                @endfor
                            </tr>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@include('admin.footer')
@include('admin.receiptsList.js.javascripts')
