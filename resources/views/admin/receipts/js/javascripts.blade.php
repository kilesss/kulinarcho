<script>
    var plaintext = 0;
    var productRowId = 1;
    var stepCookingRow = 1;

    $(document).ready(function () {
                @if(isset($countPro))
        var t = {{$countPro}};
                @else
        var t = 1;
                @endif
                @if(isset($count))
        var r = {{$count}};

                @else
        var r = 1;

        @endif
        if (t >= 1) {
            productRowId = t;
        }
        if (r >= 1) {
            stepCookingRow = r;
        }
    });

    $("#addReceiptPlainTextBtn").click(function () {
        if (plaintext == 0) {
            $("#addReceiptPlainTextTextarea").show();
            plaintext = 1;
        } else {
            $("#addReceiptPlainTextTextarea").hide();
            plaintext = 0;
        }
    });
    $(".page-link").click(function () {
        $("#frontNewsForm").closest('form').find("input[type=text], textarea").val("");
        $("#paginator-page").val($(this).text());
        $("#frontNewsForm").submit();
    });
    $("#addProductBtn").click(function () {
        productRowId = productRowId + 1
        $("#addProductField").prepend(getProdyctRow(productRowId));
    });
    $("#stepCookingBtn").click(function () {
        stepCookingRow = stepCookingRow + 1
        $("#stepCookingField").append('' +
            '<textarea class="form-control" rows="5" name="stepCooking_' + stepCookingRow + '" id="stepCooking_' + stepCookingRow + '"></textarea>');
    });


    $.event.special.inputchange = {
        setup: function () {
            var self = this, val;
            $.data(this, 'timer', window.setInterval(function () {
                val = self.value;
                if ($.data(self, 'cache') != val) {
                    $.data(self, 'cache', val);
                    $(self).trigger('inputchange');
                }
            }, 20));
        },
        teardown: function () {
            window.clearInterval($.data(this, 'timer'));
        },
        add: function () {
            $.data(this, 'cache', this.value);
        }
    };
    var receiptId;
    $(document).on('change keyup paste', '.receiptProduct', function () {
        var parent = $(this).parent().attr('id');
        receiptId = $(this).attr('id');
        var name = $(this).val();
        var suggestionText = '';
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '{!! url("admin/home/parameters") !!}', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'productInput', 'id': name}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                $('.receiptProductSuggestion').html('');

                response = JSON.parse(response);
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $.each(response, function (key, val) {

                            suggestionText = suggestionText + '<div class="selectSuggestion" id="tProduct_' + productRowId + '">' + val["name"] + '</div>';
                        });

                        $('#receipt' + parent).html(suggestionText);

                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });
    $(document).on('click', '.selectSuggestion', function () {

        $("#"+receiptId).val($(this).text());
        console.log();
        console.log($(this).text());
        $('.receiptProductSuggestion').delay(30000).html('');
    });

    function getProdyctRow(id) {
        var categoryProduct = "receiptProduct_" + id;
        var productVol = 'productVol_' + id;
        return ' <div class="row addProductRowClass" id="addProductRow_' + id + '" >' +
            '<div class="col-md-4">' +
            '<div class="form-groupreceiptProductdiv" id="ProductSuggestion_' + id + '">' +
            '<input type="text" name="' + categoryProduct + '" class="form-control receiptProduct" id="' + categoryProduct + '" placeholder="{{Lang::get("admin.receipt.product")}}"  >' +
            '<div id="receiptProductSuggestion_' + id + '" class="receiptProductSuggestion" style=" padding-left: 6%;"></div>' +
            '</div>' +

            ' <label>{{Lang::get('admin.receipt.hintProduct')}}</label>' +
            '<div class="form-group hintProductdiv">' +
            '<input type="text" name="hintProduct_' + id + '" class="form-control" id="hintProduct_' + id + '" placeholder="{{Lang::get("admin.receipt.hintInput")}}"  >' +
            '</div>' + '</div>' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<select  class="form-control receiptProductCategory" name="receiptProductCategory_' + id + '"> ' +
            '<option value="0">{{Lang::get('admin.receipt.cut')}}</option>' +
            '@foreach($cuts as $cut)' +
            '    <option value="{{$cut->id}}">{{$cut->name}}</option>' +
            '@endforeach' +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-2">' +
            '<div class="form-group">' +
            '<input type="text" name="' + productVol + '" class="form-control receiptProduct" id="' + productVol + '" placeholder="{{Lang::get("admin.receipt.vol")}}"  >' +

            '<div id="productVolSuggestion" style=" padding-left: 6%;"></div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div><hr>'
    }


    ////////////////////////////////
    $("#addCategoryInputBtn").click(function () {
        var val = $("#categoryInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'addcategory', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                if (response != 0) {
                    $("#fieldCategoryList").append('<div id="categoryId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                        '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeCategory(' + response + ')" >X</i></p></div>')
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });

    function removeCategory(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'removeCategory', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                if (response != 0) {
                    $("#categoryId_" + id).remove();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }


    $("#addTypeInputBtn").click(function () {
        var val = $("#TypeInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'addType', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#fieldTypeList").append('<div id="typeId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                            '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeType(' + response + ')" >X</i></p></div>')
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });

    function removeType(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'removeType', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#typeId_" + id).remove();
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }

    function removeProduct(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'removeProduct', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#typeId_" + id).remove();
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }

    $("#addCutsInputBtn").click(function () {
        var val = $("#CutsInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'addCuts', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#fieldCutsList").append('<div id="cutsId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                            '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeCuts(' + response + ')" >X</i></p></div>')
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });
    $("#addProductsInputBtn").click(function () {
        var val = $("#ProductsInput").val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'addProduct', 'id': val}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#fieldCutsList").append('<div id="cutsId_' + response + '"  class=\"col-md-2 form-control\">\n' +
                            '<p style=\"margin-bottom: 0;\">' + val + '<i style=\"float: right;\" onclick="removeCuts(' + response + ')" >X</i></p></div>')
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    });
    //    $("#ProductsInput").on('change keyup paste', function () {
    //        var name = $("#ProductsInput").val();
    //        if(name.length() >= 3){
    //
    //        }
    //        console.log(name);
    //    });
    $.event.special.inputchange = {
        setup: function () {
            var self = this, val;
            $.data(this, 'timer', window.setInterval(function () {
                val = self.value;
                if ($.data(self, 'cache') != val) {
                    $.data(self, 'cache', val);
                    $(self).trigger('inputchange');
                }
            }, 20));
        },
        teardown: function () {
            window.clearInterval($.data(this, 'timer'));
        },
        add: function () {
            $.data(this, 'cache', this.value);
        }
    };

    $('#ProductsInput').on('inputchange', function () {
        var name = $("#ProductsInput").val();
        var suggestionText = '';
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '{!! url("admin/home/parameters") !!}', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'productInput', 'id': name}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                $('#receiptProductSuggestion').html('');

                response = JSON.parse(response);
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $.each(response, function (key, val) {

                            suggestionText = suggestionText + '<div id="suggestionProduct_' + val["id"] + '">' + val["name"] + '</div>';
                        });
                        $('#receiptProductSuggestion').html(suggestionText);
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });

    });

    function removeCuts(id) {
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '', // This is the url we gave in the route
            data: {'ajax': 'ajax', 'type': 'removeCuts', 'id': id}, // a JSON object to send back
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) { // What to do if we succeed
                var errors = checkErrors(response);
                if (errors == 1) {
                    if (response != 0) {
                        $("#cutsId_" + id).remove();
                    }
                } else {
                    alert(errors);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
            }
        });
    }


    function checkErrors(responce) {
        responce = String(responce);
        if (responce.toString() == '"requireLogin"') {
            return "Моля логнете се";
        }
        if (responce.toString() == '"noPermision"') {

            return "Нямата права да извършвате тази функция";
        }
        return 1;

    }
</script>
