@include('admin.header')
{{--<style>--}}
{{--.panel-title {--}}
{{--margin-bottom: 1%;--}}
{{--border: 1px solid silver;--}}
{{--border-radius: 5px;--}}
{{--padding: 1%;--}}
{{--}--}}
{{--.btnAddArticle{--}}
{{--border: 1px solid silver;--}}
{{--border-radius:  23px;--}}
{{--/* padding-bottom:  0; */--}}
{{--/* margin-bottom: 0; */--}}
{{--padding-bottom: 4%;--}}
{{--padding-left:  14%;--}}
{{--padding-top: 4%;--}}
{{--}--}}
{{--</style>--}}

<div class="content">
    <div class="row">
        <div class="@if(isset($plaintext) && $plaintext != ""  && $active != 1) col-md-8 @else col-md-12 @endif">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-6">
                        <h4 class="card-title">
                            @lang('admin.receipt.titlePage')</h4>
                    </div>

                    <div class="col-md-6">
                        <button class="btn btn-dark"
                                id="addReceiptPlainTextBtn">{{Lang::get('admin.receipt.addReceiptPlainText')}}</button>
                    </div>
                </div>
                <div class="row" id="addReceiptPlainTextTextarea" style="display: none;">
                    <div class="col-md-12">
                        <div class=" form-group">
                            <div class="container">@if(isset($plaintext)){!! $plaintext!!}@endif</div>
                            <textarea class="form-control" rows="25"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button class="col-md-12 btn btn-success"
                                    style=" max-height: 312px!important;">{{Lang::get('admin.receipt.generateReceipt')}}</button>
                        </div>
                    </div>
                </div>

                {!! Form::open(array('files'=>true, 'id'=>'frontNewsForm')) !!}

                <div class="card-body" id="cardBordReceipt">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                @if(!isset($title))

                                    @php
                                        $title  = Lang::get('admin.receipt.title')
                                    @endphp

                                    {!! Form::input('text', 'receiptTitle' ,null, ['placeholder'=>$title,
                                                                                'class'=> 'form-control  ',
                                                                               'id'=> 'receiptTitle']) !!}
                                @else
                                    {!! Form::input('text', 'receiptTitle' ,$title, ['placeholder'=>$title,
                                                                                'class'=> 'form-control',
                                                                               'id'=> 'receiptTitle',
                                                                               ]) !!}

                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="receiptCategory">{{Lang::get('admin.receipt.category')}}</label>
                                <select class="form-control" name="receiptCategory" id="receiptCategory">
                                    <option value="0">{{Lang::get('admin.receipt.choose')}}</option>
                                    @foreach($categories as $cat)
                                        @if(isset($category) && $cat->id == $category)
                                            <option selected value="{{$cat->id}}">{{$cat->name}}</option>
                                        @else
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="receiptType">{{Lang::get('admin.receipt.typeReceipt')}}</label>
                                    <select class="form-control" name="receiptType" id="receiptType">
                                        <option value="0">{{Lang::get('admin.receipt.choose')}}</option>
                                        @foreach($types as $typ)
                                            @if(isset($type) &&  $typ->id == $type)
                                                <option selected value="{{$typ->id}}">{{$typ->name}}</option>
                                            @else
                                                <option value="{{$typ->id}}">{{$typ->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                        </div>
                    </div>
                    @if(isset($products) && count($products)>0)
                        @foreach($products as $prok => $pro)
                            @php
                                $countPro= $prok+1;

                            @endphp
                            <div id="addProductField">
                                <div style="border-bottom: 1px solid silver; margin-bottom: 1%"
                                     class="row addProductRowClass" id="addProductRow_{{$countPro}}">
                                    <div class="col-md-4">
                                        <div class="form-group receiptProductdiv" id="ProductSuggestion_{{$countPro}}">
                                            @if(isset($pro['name']))
                                                {!! Form::input('text', "receiptProduct_$countPro" ,$pro['name'], ['placeholder'=>$pro['name'],
                                                                                      'class'=> 'form-control receiptProduct',
                                                                                       'id'=>"receiptProduct_$countPro",
                                                                                      ]) !!}

                                            @else
                                                {!! Form::input('text', "receiptProduct_$countPro" ,null, ['placeholder'=>Lang::get('admin.receipt.product'),
                                                                                      'class'=> 'form-control receiptProduct',
                                                                                       'id'=>"receiptProduct_$countPro"]) !!}
                                            @endif

                                            <div id="receiptProductSuggestion_{{$countPro}}"
                                                 class="receiptProductSuggestion" style=" padding-left: 6%;"></div>
                                        </div>
                                        <label>{{Lang::get('admin.receipt.hintProduct')}}</label>
                                        <div class="form-group hintProductdiv">
                                            @if(isset($pro['hint']))
                                                {!! Form::input('text', "hintProduct_$countPro" ,$pro['hint'], ['placeholder'=>$pro['hint'],
                                                                                      'class'=> 'form-control',
                                                                                       'id'=>"hintProduct_$countPro",
                                                                                       ]) !!}

                                            @else
                                                {!! Form::input('text', "hintProduct_$countPro" ,null, ['placeholder'=>Lang::get('admin.receipt.hintInput'),
                                                                                      'class'=> 'form-control',
                                                                                       'id'=>"hintProduct_$countPro"]) !!}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control" id="receiptProductCategory_{{$countPro}}"
                                                    name="receiptProductCategory_{{$countPro}}">
                                                <option value="0">{{Lang::get('admin.receipt.cut')}}</option>
                                                @foreach($cuts as $cut)
                                                    @if(isset($pro['cuts']) && $pro['cuts'] == $cut->id)
                                                        <option selected value="{{$cut->id}}">{{$cut->name}}</option>
                                                    @else
                                                        <option value="{{$cut->id}}">{{$cut->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            @if(isset($pro['vol']))
                                                {!! Form::input('text', "productVol_$countPro" ,$pro['vol'], ['placeholder'=>$pro['vol'],
                                                                                         'class'=> 'form-control',
                                                                                         'id'=> 'productVol']) !!}

                                            @else
                                                {!! Form::input('text', "productVol_$countPro" ,null, ['placeholder'=>Lang::get('admin.receipt.vol'),
                                                                                       'class'=> 'form-control',
                                                                                       'id'=> 'productVol']) !!}
                                            @endif
                                        </div>

                                    </div>
                                    @if($countPro == 1)
                                        <div class="col-md-2">
                                            <p class="btn btn-block" id="addProductBtn"
                                               style="margin-top: 0;border-radius: 18px;"> {{Lang::get('admin.receipt.addProductBtn')}}</p>
                                        </div>

                                    @endif
                                </div>
                            </div>

                        @endforeach
                    @else
                        @php
                            $countPro= 1
                        @endphp

                        <div id="addProductField">
                            <div class="row addProductRowClass"
                                 style="border-bottom: 1px solid silver; margin-bottom: 1%" id="addProductRow_1">
                                <div class="col-md-4">
                                    <div class="form-group receiptProductdiv" id="ProductSuggestion_1">
                                        {!! Form::input('text', 'receiptProduct_1' ,null, ['placeholder'=>Lang::get('admin.receipt.product'),
                                                                                    'class'=> 'form-control receiptProduct', 'id'=>'receiptProduct_1']) !!}
                                        <div id="receiptProductSuggestion_1" class="receiptProductSuggestion"
                                             style=" padding-left: 6%;"></div>
                                    </div>
                                    <label>{{Lang::get('admin.receipt.hintProduct')}}</label>
                                    <div class="form-group hintProductdiv">
                                        {!! Form::input('text', "hintProduct_1" ,null, ['placeholder'=>Lang::get('admin.receipt.hintInput'),
                                                                              'class'=> 'form-control receiptProduct',
                                                                               'id'=>"hintProduct_1"]) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="receiptProductCategory_1"
                                                name="receiptProductCategory_1">
                                            <option value="0">{{Lang::get('admin.receipt.cut')}}</option>
                                            @foreach($cuts as $cut)
                                                <option value="{{$cut->id}}">{{$cut->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::input('text', 'productVol_1' ,null, ['placeholder'=>Lang::get('admin.receipt.vol'),
                                                                                    'class'=> 'form-control',
                                                                                    'id'=> 'productVol']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <p class="btn btn-block" id="addProductBtn"
                                       style="margin-top: 0;border-radius: 18px;"> {{Lang::get('admin.receipt.addProductBtn')}}</p>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                @if(!isset($cookingTime))
                                    @php
                                        $cookingTime  = Lang::get('admin.receipt.cookingTime')
                                    @endphp
                                    {!! Form::input('text', 'ProductsCookingTime' ,null, ['placeholder'=>$cookingTime,
                                                                                 'class'=> 'form-control',
                                                                                 'id'=> 'ProductsCookingTime']) !!}

                                @else
                                    {!! Form::input('text', 'ProductsCookingTime' ,$cookingTime, ['placeholder'=>$cookingTime,
                                                                                'class'=> 'form-control',
                                                                                'id'=> 'ProductsCookingTime']) !!}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                @if(!isset($portion))
                                    @php
                                        $portion  = Lang::get('admin.receipt.portions')
                                    @endphp
                                    {!! Form::input('text', 'ProductsPortions' ,null, ['placeholder'=>$portion,
                                                                                'class'=> 'form-control',
                                                                                'id'=> 'ProductsPortions']) !!}
                                @else
                                    {!! Form::input('text', 'ProductsPortions' ,$portion, ['placeholder'=>$portion,
                                            'class'=> 'form-control',
                                            'id'=> 'ProductsPortions']) !!}

                                @endif

                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="newsImages">@lang('admin.news.image')</label>
                            {!!  Form::file('imagePortions', ['placeholder'=>Lang::get('admin.receipt.image'),
                                                                                                     'class'=> 'form-control',
                                                                                                     'id'=> 'imagePortions']) !!}

                            {{--<div class="form-group">--}}
                            {{----}}
                            {{--{!! Form::input('text', 'imagePortions', null ,['placeholder'=>Lang::get('admin.receipt.image'),--}}
                            {{--'class'=> 'form-control',--}}
                            {{--'id'=> 'imagePortions']) !!}--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    {{--<div class="row">--}}
                    {{--<div class="col-md-3"><p style="height: 200px; border: 1px solid silver"></p></div>--}}
                    {{--<div class="col-md-3"><p style="height: 200px; border: 1px solid silver"></p></div>--}}
                    {{--<div class="col-md-3"><p style="height: 200px; border: 1px solid silver"></p></div>--}}
                    {{--<div class="col-md-3"><p style="height: 200px; border: 1px solid silver"></p></div>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<div class="form-group">--}}
                    {{--{!! Form::input('text', 'videoPortions' ,null, ['placeholder'=>Lang::get('admin.receipt.video'),--}}
                    {{--'class'=> 'form-control',--}}
                    {{--'id'=> 'videoPortions']) !!}--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<iframe width="560" height="315" src="https://www.youtube.com/embed/BmpKqC9vbu4"--}}
                    {{--frameborder="0"--}}
                    {{--allow="autoplay; encrypted-media" allowfullscreen></iframe>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="row">
                        <div class="col-md-3"><h5>{{Lang::get('admin.receipt.stepCooking')}}</h5></div>
                        <div class="col-md-2">
                            <p class="btn btn-group" id="stepCookingBtn"
                               style="margin-top: 0">{{Lang::get('admin.receipt.stepCookingBtn')}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class=" form-group" id="stepCookingField">
                                @if(isset($steps))
                                    @foreach($steps as $k=>$step)
                                        <textarea class="form-control" rows="5" name="stepCooking_{{$k+1}}"
                                                  id="stepCooking_{{$k+1}}">{{$step}}</textarea>
                                        @php
                                            $count = $k+1;
                                        @endphp
                                    @endforeach
                                @else
                                    @php
                                        $count = 1;
                                    @endphp
                                    <textarea class="form-control" rows="5" name="stepCooking_1"
                                              id="stepCooking_1"></textarea>

                                @endif
                            </div>
                        </div>
                    </div>
                    @if(isset($active))
                        <div class="row">
                            <div class="form-check" style=" margin-left: 2%;">
                                <input type="checkbox" name="activeReceipt" @if($active == 1) checked @endif class="form-check-input" id="activeReceipt"
                                       style="visibility:  visible;opacity: inherit;width:  27px;height:  21px;padding-right: 2%;margin-right:  1%;">
                                <label class="form-check-label" for="exampleCheck1"
                                       style=" margin-left: 0; padding-left: 9px;">{{Lang::get('admin.receipt.activeCheckbox')}}</label>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-2">
                            @if(isset($updateReceipt))
                                <input type="hidden" name="updateReceipt" value="{{$updateReceipt}}">
                            @endif
                            <input type="submit" value="{{Lang::get('admin.receipt.titlePage')}}"
                                   class="btn btn-success">
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @if(isset($plaintext) && $plaintext != "" && $active != 1)
            <div class="col-md-4">
                {!! $plaintext !!}
            </div>
        @endif
    </div>
</div>


@include('admin.footer')
@include('admin.receipts.js.javascripts')
