<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'register'=>'Регистрация',
    'name'=>'Потребител',
    'repassword'=>'Пожторете имейла',
    ''=>'',
    'email'=>"Имейл",
    'password'=>'Парола',
    'remember'=>'Запомни ме',
    'login'=>'Вход',
    'forgot'=>'Забравена парола',
    'failed' => 'Имейла или паролата са грешни.',
    'throttle' => 'Твърде много опити за вход. Моля опитайте след :seconds seconds.',

];
