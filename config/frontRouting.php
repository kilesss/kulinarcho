<?php
/**
 * Created by PhpStorm.
 * User: kilesss
 * Date: 5/9/2018
 * Time: 12:29 PM
 */

return[
    'routing'=>[
        'home'=>'/home',
        'login'=>'/login',
        'register'=>'/register',
        'profile'=>'/profile',
        'newRecipes'=>'/newRecipes',
        'news'=>'/news',
        'recipeSearch'=>'/recipeSearch',
        'members'=>'/members',
        'menu'=>'/menu',
        'shoppingList'=>'/shoppingList',
    ]
];